<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
	<div class="row">
		<div class="col-md-12">
			@if ($horarios)
			<h6>Días y Horarios</h6>
			<div class="table-responsive-md">
				<table class="table table-bordered table-condensed table-striped m-b-0">
					<thead>
						<tr>
							<th>Días de la semana</th>
							<th>Hora de inicio</th>
							<th>Hora de fin</th>
						</tr>
					</thead>
					<tbody>
						@foreach($horarios as $horario)
						@php 
							$hora_inicio = explode(' ', $horario->hora_inicio);
							$hora_fin = explode(' ', $horario->hora_fin);
						@endphp
						<tr>
							<td>{{ $dias[$horario->dia] }}</td>
							<td>{{ substr($hora_inicio[1], 0, -3) }}</td>
							<td>{{ substr($hora_fin[1], 0, -3) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@else
			<div class="alert alert-danger" role="alert">
				<i class="la la-exclamation-circle la-2x float-left mt-n1 mr-2"></i> No se han especificado horarios de visita!
			</div>
			@endif
			@if ($parametros)
			<h6>Parametros especiales</h6>
			<div class="table-responsive-md">
				<table class="table table-bordered table-condensed table-striped m-b-0">
					<thead>
						<tr>
							<th>Clave</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						@foreach($parametros as $parametro)
						<tr>
							<td>{{ $parametro->clave }}</td>
							<td>{{ $parametro->valor }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
</div>
<div class="clearfix"></div>