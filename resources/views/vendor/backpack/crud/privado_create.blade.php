@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    "Ingresar PPL" => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;


  $centro = backpack_auth()->user()->centro_id;
  if ($centro){
    $centros = \App\Models\Centro::where('id', $centro)->orderBy('nombre')->with('pabellones')->get();
    
    $pabellones = \App\Models\CentroPabellon::where('centro_id', $centro)->get();

  } else {
      $centros = \App\Models\Centro::orderBy('nombre')->with('pabellones')->get();
      $pabellones = \App\Models\CentroPabellon::all();
  }
  $nacionalidades = \App\Models\Pais::all();
  $generos = \App\Models\Genero::all();
  $identidades_tipos = \App\Models\IdentidadTipo::all();
  
  //dd($centros);

  share(['pabellones' => $pabellones, 'centros' => $centros, 'centro' => $centro, 'token' => csrf_token(), 'now' => date('Y-m-d')]);

@endphp

@section('header')
    <section class="container-fluid">
	  <h2>
        <span class="text-capitalize">Ingresar PPL</span>
	  </h2>
	</section>
@endsection

@section('content')

        <div class="row">
            <div class="col-md-12 bold-labels">
                <div class="card">
                    <div class="card-body row">
                        <div class="col-12">
                            <p>Antes de comenzar a fichar a una persona por favor busque si la misma ya cuenta con antecedente de ficha</p>
                        </div>
                        <div class="col-12">
                            <form class="row" id="search">
                                <div class="form-group col-sm-4" element="div">
                                    <label>Nombres</label>
                                    <input type="text" name="nombres" value="" class="form-control" >
                                </div>
                                <div class="form-group col-sm-4" element="div">
                                    <label>Apellidos</label>
                                    <input type="text" name="apellidos" value="" class="form-control" >
                                </div>
                                <div class="form-group col-sm-4" element="div">
                                    <label>Documento de Identidad</label>
                                    <input type="text" name="documento" value="" class="form-control" >
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-search"></i> Buscar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 bold-labels">
                <div class="card">
                    <div class="card-body">
                        <h3>Resultados posibles</h3>
                        <div class="form-group mt-4 animated fadeIn d-none" element="div" id="info-add-new">
                            <label>
                                Si ninguno de los resultados corresponde a la persona, por favor
                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#fichar"><i class="la la-id-card la-lg"></i> Registre nuevo ingreso</a>
                            </label>
                        </div>
                        <div class="justify-content-center animated fadeIn row m-5" id="info-search">
                            <div class="col-4 text-center">
                                <i class="las la-info-circle la-6x text-muted"></i>
                                <h3 class="text-muted">Utilice el formulario <br>para obtener resultados</h3>
                            </div>
                        </div>
                        <div class="animated fadeIn d-none" id="results"></div>
                    </div>
                </div>
            </div>
        </div>
        
@endsection


@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/select2/dist/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
    <style>
       #results .card.bg-danger {
           background-color: #E38484 !important;
       }
       .card-to-modal {
            position: relative;
            width: auto;
            max-width: 800px;
            margin: 1.75rem auto;
            display: flex;
            transition: transform .3s ease-out;
            transform: translateY(-50px);
            margin-top: 25%;
       }
    </style>
@endsection

@section('after_scripts')
    <!-- Modal -->
    <div class="modal fade" id="fichar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ficharLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ficharLabel">Registrar nuevo ingreso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="{{ url('api/fichar') }}">
                @csrf
                <div class="modal-body">

                    <div class="row form bold-labels">
                        <div class="form-group col-sm-6" element="div">
                            <label>Nombres</label>
                            <input type="text" name="nombres" id="nombres" value="" class="form-control" >
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <label>Apellidos</label>
                            <input type="text" name="apellidos" id="apellidos" value="" class="form-control" >
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <label>Género</label>
                            <select name="genero_id" id="genero_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                @foreach($generos as $genero)
                                    <option value="{{ $genero->id }}">{{ $genero->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <input type="hidden" name="fecha_nacimiento" value="">
                            <label>Fecha Nacimiento</label>
                            <div class="input-group date">
                                <input id="fecha_nacimiento" data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;endDate&quot;:&quot;new date()&quot;,&quot;startView&quot;:&quot;3&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="la la-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <label>Nacionalidad</label>
                            <select name="nacionalidad_id" id="nacionalidad_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                @foreach($nacionalidades as $nacionalidad)
                                    <option value="{{ $nacionalidad->id }}" @if($nacionalidad->id == '181') selected @endif>{{ $nacionalidad->gentilicio }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <label>Tipo de Identidad</label>
                            <select name="identidad_tipo_id" id="identidad_tipo_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                @foreach($identidades_tipos as $tipo)
                                    <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <label>Documento de Identidad</label>
                            <input type="text" name="documento" id="documento" value="" class="form-control" >
                        </div>
                        <div class="col-sm-12">
                        <hr>
                        </div>
                        <div class="form-group col-sm-6" element="div">
                            <input type="hidden" name="fecha_ingreso" id="fecha_ingreso" value="{{ date("Y-m-d") }}">
                            <label>Fecha Ingreso</label>
                            <div class="input-group date">
                                <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;endDate&quot;:&quot;new date()&quot;,&quot;setDate&quot;:&quot;new Date()&quot;,&quot;startView&quot;:&quot;2&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="la la-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if(!$centro)
                        <div class="form-group col-sm-6" element="div">
                            <label>Centro de Reclusión</label>
                            <select name="centro_id" id="centro_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                <option value="">-</option>
                                @foreach($centros as $lugar)
                                    <option value="{{ $lugar->id }}">{{ $lugar->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="form-group col-sm-6" element="div">
                            <label>Pabellón</label>
                            <select name="pabellon_id" id="pabellon_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                <option value="">-</option>
                            </select>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>

	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('packages/select2/dist/js/i18n/es.js') }}"></script>
    <script>
        function bpFieldInitSelect2Element(element) {
            // element will be a jQuery wrapped DOM node
            if (!element.hasClass("select2-hidden-accessible")) {
                element.select2({
                    theme: "bootstrap"
                });
            }
        }

    </script>
    <script src="{{ asset('packages/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script charset="UTF-8" src="{{ asset('packages/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    <script>
        if (jQuery.ui) {
            var datepicker = $.fn.datepicker.noConflict();
            $.fn.bootstrapDP = datepicker;
        } else {
            $.fn.bootstrapDP = $.fn.datepicker;
        }

        var dateFormat=function(){var a=/d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,b=/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,c=/[^-+\dA-Z]/g,d=function(a,b){for(a=String(a),b=b||2;a.length<b;)a="0"+a;return a};return function(e,f,g){var h=dateFormat;if(1!=arguments.length||"[object String]"!=Object.prototype.toString.call(e)||/\d/.test(e)||(f=e,e=void 0),e=e?new Date(e):new Date,isNaN(e))throw SyntaxError("invalid date");f=String(h.masks[f]||f||h.masks.default),"UTC:"==f.slice(0,4)&&(f=f.slice(4),g=!0);var i=g?"getUTC":"get",j=e[i+"Date"](),k=e[i+"Day"](),l=e[i+"Month"](),m=e[i+"FullYear"](),n=e[i+"Hours"](),o=e[i+"Minutes"](),p=e[i+"Seconds"](),q=e[i+"Milliseconds"](),r=g?0:e.getTimezoneOffset(),s={d:j,dd:d(j),ddd:h.i18n.dayNames[k],dddd:h.i18n.dayNames[k+7],m:l+1,mm:d(l+1),mmm:h.i18n.monthNames[l],mmmm:h.i18n.monthNames[l+12],yy:String(m).slice(2),yyyy:m,h:n%12||12,hh:d(n%12||12),H:n,HH:d(n),M:o,MM:d(o),s:p,ss:d(p),l:d(q,3),L:d(q>99?Math.round(q/10):q),t:n<12?"a":"p",tt:n<12?"am":"pm",T:n<12?"A":"P",TT:n<12?"AM":"PM",Z:g?"UTC":(String(e).match(b)||[""]).pop().replace(c,""),o:(r>0?"-":"+")+d(100*Math.floor(Math.abs(r)/60)+Math.abs(r)%60,4),S:["th","st","nd","rd"][j%10>3?0:(j%100-j%10!=10)*j%10]};return f.replace(a,function(a){return a in s?s[a]:a.slice(1,a.length-1)})}}();dateFormat.masks={default:"ddd mmm dd yyyy HH:MM:ss",shortDate:"m/d/yy",mediumDate:"mmm d, yyyy",longDate:"mmmm d, yyyy",fullDate:"dddd, mmmm d, yyyy",shortTime:"h:MM TT",mediumTime:"h:MM:ss TT",longTime:"h:MM:ss TT Z",isoDate:"yyyy-mm-dd",isoTime:"HH:MM:ss",isoDateTime:"yyyy-mm-dd'T'HH:MM:ss",isoUtcDateTime:"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"},dateFormat.i18n={dayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],monthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","January","February","March","April","May","June","July","August","September","October","November","December"]},Date.prototype.format=function(a,b){return dateFormat(this,a,b)};

        function bpFieldInitDatePickerElement(element) {
            var $fake = element,
            $field = $fake.closest('.form-group').find('input[type="hidden"]'),
            $customConfig = $.extend({
                format: 'dd/mm/yyyy'
            }, $fake.data('bs-datepicker'));
            $picker = $fake.bootstrapDP($customConfig);

            var $existingVal = $field.val();

                if( $existingVal.length ){
                    // Passing an ISO-8601 date string (YYYY-MM-DD) to the Date constructor results in
                    // varying behavior across browsers. Splitting and passing in parts of the date
                    // manually gives us more defined behavior.
                    // See https://stackoverflow.com/questions/2587345/why-does-date-parse-give-incorrect-results
                    var parts = $existingVal.split('-');
                    var year = parts[0];
                    var month = parts[1] - 1; // Date constructor expects a zero-indexed month
                    var day = parts[2];
                    preparedDate = new Date(year, month, day).format($customConfig.format);
                    $fake.val(preparedDate);
                    $picker.bootstrapDP('update', preparedDate);
                }

                // prevent users from typing their own date
                // since the js plugin does not support it
                // $fake.on('keydown', function(e){
                //     e.preventDefault();
                //     return false;
                // });

            $picker.on('show hide change', function(e){
                if( e.date ){
                    var sqlDate = e.format('yyyy-mm-dd');
                } else {
                    try {
                        var sqlDate = $fake.val();

                        if( $customConfig.format === 'dd/mm/yyyy' ){
                            sqlDate = new Date(sqlDate.split('/')[2], sqlDate.split('/')[1] - 1, sqlDate.split('/')[0]).format('yyyy-mm-dd');
                        }
                    } catch(e){
                        if( $fake.val() ){
                                new Noty({
                                    type: "error",
                                    text: "<strong>Whoops!</strong><br>Sorry we did not recognise that date format, please make sure it uses a yyyy mm dd combination"
                                  }).show();
                            }
                        }
                    }

                    $field.val(sqlDate);
                });
            }
    </script>
    <script>
        $(document).ready(function(){
            initializeFieldsWithJavascript('form');

            $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

            var $infoAddNew = $('#info-add-new');
            var $infoSearch = $('#info-search');
            var $results = $('#results');
            var sharedData = window.sharedData;
            var token = sharedData.token;
            var now = sharedData.now;

            const formOptions = ({ id, nombre }) => `<option value="${id}">${nombre}</option>`;
            const novalue = '<option value="">-</option>';
            const pabellonesForm = (sharedData.centro) ? sharedData.centros[0].pabellones.map(formOptions).join('') : '';

            
            if (!sharedData.centro) {
                
                var centrosForm = sharedData.centros.map(formOptions).join('');
                
                $('#results').on('change', 'select.centro', function(){
                    var value = $(this).val();
                    var $selectPabellon = $(this).parent().next().next().find('.select2_field');
                    const index = sharedData.centros.findIndex(centro => centro.id === parseInt(value));

                    if (parseInt(value) > 0){
                        const pabellonesForm = sharedData.centros[index].pabellones.map(formOptions).join('');
                        $selectPabellon.html(novalue + pabellonesForm);
                    } else {
                        $selectPabellon.html(novalue);
                    }
                });
            }

            const btnCard = {
                transferir: `<button class="btn btn-warning btn-sm abrir-form"><i class="la la-exchange-alt"></i> Solicitar traslado</button>`,
                ingresar: `<button class="btn btn-primary btn-sm abrir-form"><i class="la la-user-lock la-lg"></i> Realizar ingreso</button>`,
                fichar: `<button class="btn btn-primary btn-sm abrir-form"><i class="la la-address-card la-lg"></i> Generar ficha</button>`,
                ver: `<a class="btn btn-primary btn-sm ver-ficha"><i class="la la-address-card la-lg"></i> Ver Ficha</a>`
                };

            const formElementCentro = (!sharedData.centro) ? `
                                <div class="form-group col-sm-12" element="div">
                                    <label>Centro</label>
                                    <select name="centro_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field centro" >
                                        <option value="">-</option>
                                        ${centrosForm}
                                    </select>
                                </div>
                    ` : ``;

            const btnAccion = {
                transferir: `<button class="btn btn-primary btn-sm procesar">Solicitar</button>`,
                ingresar: `<button class="btn btn-primary btn-sm procesar">Realizar ingreso</button>`,
                fichar: `<button class="btn btn-primary btn-sm procesar">Realizar ingreso</button>`
                };
            const formElements = {
                transferir: `<div class="form-group col-sm-12" element="div">
                                <label>Observación (opcional)</label>
                                <textarea name="mensaje" class="form-control"></textarea>
                            </div>`,
                ingresar: ` ${formElementCentro}
                            <div class="form-group col-sm-6" element="div">
                                <input type="hidden" name="fecha_ingreso" value="${now}">
                                <label>Fecha Ingreso</label>
                                <div class="input-group date">
                                    <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;endDate&quot;:&quot;new date()&quot;,&quot;setDate&quot;:&quot;new Date()&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <span class="la la-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6" element="div">
                                <label>Pabellón</label>
                                <select name="pabellon_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                    <option value="">-</option>
                                    ${pabellonesForm}
                                </select>
                            </div>`,
                fichar:    `${formElementCentro}
                            <div class="form-group col-sm-6" element="div">
                                <input type="hidden" name="fecha_ingreso" value="${now}">
                                <label>Fecha Ingreso</label>
                                <div class="input-group date">
                                    <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;endDate&quot;:&quot;new date()&quot;,&quot;setDate&quot;:&quot;new Date()&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <span class="la la-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6" element="div">
                                <label>Pabellón</label>
                                <select name="pabellon_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                                    <option value="">-</option>
                                    ${pabellonesForm}
                                </select>
                            </div>`,
                ver: ``,

            };

            const Item = ({ id, accion, tipo, nombres, apellidos, foto, documento, estado, centro, color }) => `
                        <div class="col-12 col-md-6 col-lg-4 mb-4">
                            <div class="result">
                                <div class="card h-100 bg-${color}" data-id="${id}">
                                    <div class="card-body">
                                        <div class="media">
                                            <img src="${foto}" width="64" height="64" alt="${nombres} ${apellidos}" class="mr-3 rounded-circle" >
                                            <div class="media-body">
                                                <h6 class="mt-2 card-subtitle">${nombres} ${apellidos}</h6>
                                                <p class="card-text">${documento}</p>
                                                <h6 class="my-3">
                                                    <i class="las la-exclamation-circle la-lg"></i> ${estado}
                                                </h6>
                                                <form class="row d-none form">
                                                    <input type="hidden" name="_token" value="${token}">
                                                    <input type="hidden" name="accion" value="${accion}">
                                                    <input type="hidden" name="${tipo}" value="${id}">
                                                    <input type="hidden" name="centro" value="${centro}">
                                                    ${formElements[accion]}
                                                    <div class="col-sm-6">
                                                        ${btnAccion[accion]}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <button class="btn btn-default btn-sm cancelar">Cancelar</button>
                                                    </div>
                                                </form>
                                                ${btnCard[accion]}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;

            $('#search button').on('click', function(e){
                e.preventDefault();
                var $form = $(this).closest('form');
                var $data = $form.serialize();
                var $dataSource = '/api/buscar-personas';

                $.ajax({
                    url: $dataSource,
                    data: $data,
                    type: 'POST',
                    success: function (result) {
                        $infoAddNew.addClass('show').removeClass('d-none');
                        $infoSearch.removeClass('show').addClass('d-none');
                        $results.addClass('show').removeClass('d-none');
                        if (result.success)
                            $('#results').html('<div class="row">' + result.message.map(Item).join('') + '</div>');
                        else
                            $('#results').html(result.message);
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            });

            $('#results').on('click', '.abrir-form', function(){
                $(this).closest('div.result').addClass('modal d-block');
                $(this).closest('div.card').removeClass('h-100').addClass('card-to-modal');
                $(this).prev('.row').removeClass('d-none');
                $(this).addClass('d-none');
                $('body').addClass('modal-open');
                initializeFieldsWithJavascript('form');
            });

            $('#results').on('click', '.ver-ficha', function(){
                var $id = $(this).closest('div.card').attr('data-id');
                window.location.href= $id + '/show';
            }); 

            $('#results').on('click', '.cancelar', function(e){
                e.preventDefault();
                $(this).closest('div.result').removeClass('modal d-block');
                $(this).closest('div.card').addClass('h-100').removeClass('card-to-modal');
                $(this).closest('form.row').addClass('d-none');
                $(this).closest('form.row').next().removeClass('d-none');
                $('body').removeClass('modal-open');
            });

            $('#results').on('click', '.procesar', function(e){
                e.preventDefault();
                var $dataSource = '/api/ingresar-persona';
                var $form = $(this).closest('form');
                var $data = $form.serialize();

                $.ajax({
                    url: $dataSource,
                    data: $data,
                    type: 'POST',
                    success: function (result) {

                        if (result.perfil_privado_id){
                            window.location.href= result.perfil_privado_id + '/show';
                        } else {
                            new Noty({
                                    type: "error",
                                    text: "<strong>Ocurrió un error</strong><br>Por favor comuniquese con el administrador"
                                }).show();
                        }
                    },
                    error: function (result) {
                        var errors = result.responseJSON.errors;
                        $.each(errors, function( key, value ) {
                            new Noty({
                                type: "error",
                                text: "<strong>Por favor corrija el error</strong><br>" + value
                            }).show();
                        });
                        
                    }
                });

            });
            if (!sharedData.centro) {
                $('#centro_id').on('change', function(){
                    const id = $(this).val();
                    setPabellones(id);
                });
                if (parseInt($('#centro_id').val()))
                    setPabellones(parseInt($('#centro_id').val()));

            } else {
                const pabellonesActuales = sharedData.centros[0].pabellones.map(formOptions).join('');
                $('#pabellon_id').html(pabellonesActuales);
                
            }

            function setPabellones(id){
                var pabellonesActuales = '';
                    if (parseInt(id)){
                        const index = sharedData.centros.findIndex(centro => centro.id === parseInt(id));
                        pabellonesActuales = sharedData.centros[index].pabellones.map(formOptions).join('');
                    }
                    $('#pabellon_id').html(novalue + pabellonesActuales);
            }

            $('#fichar form').on('submit', function(e){
                e.preventDefault();
                var $formData = $(this).serialize();
                var $dataSource = $(this).attr('action');

                $.ajax({
                    url: $dataSource,
                    data: $formData,
                    type: 'POST',
                    success: function (result) {

                        if (result.id){
                            window.location.href= result.id + '/show';
                        } else {
                            new Noty({
                                    type: "error",
                                    text: "<strong>Ocurrió un error</strong><br>Por favor comuniquese con el administrador"
                                }).show();
                        }
                    },
                    error: function (data) {

                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#fichar #'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                        
                    }
                });

            });

        });

        function initializeFieldsWithJavascript(container) {
            var selector;
            if (container instanceof jQuery) {
                selector = container;
            } else {
                selector = $(container);
            }
            selector.find("[data-init-function]").not("[data-initialized=true]").each(function () {
                var element = $(this);
                var functionName = element.data('init-function');

                if (typeof window[functionName] === "function") {
                window[functionName](element);

                // mark the element as initialized, so that its function is never called again
                element.attr('data-initialized', 'true');
                }
            });
        }

        $('#fichar').on('shown.bs.modal', function () {
            bpFieldInitDatePickerElement($('#fichar .date input'));
        });
    </script>
@endsection
