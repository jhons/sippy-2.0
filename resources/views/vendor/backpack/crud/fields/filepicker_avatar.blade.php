{{-- ########################################## --}}
{{-- Como usar el módulo 
        // $this->crud->addField([
        //     'name'   => 'nombre', 
        //     'type'   => 'filepicker_avatar', 
        //     'label'  => 'Avatar',
        //     'origin' => 'fotos/',       // Folder destination
        //     'disk'   => 'public',       // Storage destination
        //     'prefix' => 'miniatura/',   // Avatar folder
        //     'avatar' => [               // Avatar size
        //         'width' => '350',       // Default: 300
        //         'height' => '350'       // Default: 300
        //     ],
        //     // 'keep_original' => false     //Default: true
        // ]);

--}}
{{-- ########################################## --}}

    @php
        $width = isset($field['width']) ? $field['width'] : '200';
        $origin = isset($field['origin']) ? $field['origin'] : '';
        $prefix = isset($field['prefix']) ? $field['prefix'] : '';
        $value = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';
        $avatar = (isset($value) && $value) ? Storage::disk($field['disk'])->url($origin.$prefix.$value) : null;
        $original = isset($value) ? Storage::disk($field['disk'])->url($origin . $value) : null;
    @endphp

    @include('crud::fields.inc.wrapper_start')
        <p>
            <!-- Display the avatar -->
            <img src="{{ $avatar ? $avatar : 'https://www.gravatar.com/avatar/?d=mm&s=300' }}" class="img-fluid rounded d-block file-avatar" width="{{ $width }}">
            <input type="hidden" data-handle="hiddenImage" name="{{ $field['name'] }}" id="{{ $field['name'] }}_avatar" value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}">
        </p>
        <div id="filepicker">
            <!-- Button Bar -->
            <div class="button-bar">
                <div>
                    <div class="btn btn-success fileinput">
                        <i class="la la-arrow-circle-o-up"></i> Subir
                        <input type="file" name="files[]">
                    </div>

                    <button type="button" class="btn btn-primary camera-show">
                        <i class="la la-camera"></i> Camara
                    </button>
                </div>

                <div>
                    <!-- Here we set the `data-fileurl` attribute with the original image source
                            that it will be used when cropping the image. -->
                    <button type="button" class="btn btn-info crop-btn {{ !$avatar ? 'd-none' : '' }}"
                        data-fileurl="{{ $original }}">
                        <i class="la la-crop"></i> Cortar
                    </button>

                    <!-- Here we set the `data-fileurl` attribute with the original image source
                            that will be used when deleting the file. -->
                    <button type="button" class="btn btn-danger delete-btn {{ !$avatar ? 'd-none' : '' }}"
                        data-fileurl="{{ $original }}">
                        <i class="la la-trash-o"></i> Borrar
                    </button>
                </div>
            </div>
            <div class="drop-window modal fade">
                <div class="drop-window-content">
                    <h3><i class="la la-upload"></i> Soltar archivos para cargar</h3>
                </div>
            </div>
        </div><!-- end of #filepicker -->
    @include('crud::fields.inc.wrapper_end')

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    <link rel="stylesheet" href="{{ asset('packages/filepicker/assets/css/cropper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/filepicker/assets/css/filepicker.css') }}">
    <style>
        .crop-rotate {
            text-align: center;
        }
        .drop-window {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1050;
            display: none;
            width: 100%;
            height: 100%;
            overflow: hidden;
            outline: 0;
        }
    </style>

    @endpush

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
    <!-- Drop Window -->

    <!-- Crop Modal -->
    <div id="crop-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Has una elección</h5>
                    <span class="close" data-dismiss="modal">&times;</span>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning crop-loading">Cargando imagen...</div>
                    <div class="crop-preview"></div>
                </div>
                <div class="modal-footer">
                    <div class="crop-rotate">
                        <button type="button" class="btn btn-default crop-rotate-left" title="Girar a la izquierda">
                            <i class="la la-undo"></i>
                        </button>
                        <button type="button" class="btn btn-default crop-flip-horizontal" title="Voltear horizontal">
                            <i class="la la-arrows-h"></i>
                        </button>
                        <button type="button" class="btn btn-default crop-flip-vertical" title="Voltear vertical">
                            <i class="la la-arrows-v"></i>
                        </button>
                        <button type="button" class="btn btn-default crop-rotate-right" title="Girar a la derecha">
                            <i class="la la-repeat"></i>
                        </button>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-success crop-save" data-loading-text="Guardando...">Guardar</button>
                </div>
            </div>
        </div>
    </div><!-- end of #crop-modal -->

    <!-- Camera Modal -->
    <div id="camera-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Toma una foto</h5>
                    <span class="close" data-dismiss="modal">&times;</span>
                </div>
                <div class="modal-body">
                    <div class="camera-preview"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left camera-hide" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-success camera-capture">Tomar foto</button>
                </div>
            </div>
        </div>
    </div><!-- end of #camera-modal -->

    <!-- Scripts -->
    <script src="{{ asset('packages/filepicker/assets/js/cropper.min.js') }}"></script>

    <script src="{{ asset('packages/filepicker/assets/js/filepicker.js') }}"></script>
    <script src="{{ asset('packages/filepicker/assets/js/filepicker-drop.js') }}"></script>
    <script src="{{ asset('packages/filepicker/assets/js/filepicker-crop.js') }}"></script>
    <script src="{{ asset('packages/filepicker/assets/js/filepicker-camera.js') }}"></script>

    <script>
        /*global $*/

        var cropBtn = $('.crop-btn');
        var deleteBtn = $('.delete-btn');

        $('#filepicker').filePicker({
            url: '{{ backpack_url('filepicker-avatar') }}',
            data: {
                _token: "{{ csrf_token() }}",
                disk: "{{ $field['disk'] }}",
                origin: "{{ $origin }}",
                @if(isset($field['keep_original'])) 
                keep_original: "{{ $field['keep_original'] }}", 
                @endif
                @if(isset($field['avatar']['width'])) 
                width: "{{ $field['avatar']['width'] }}", 
                @endif
                @if(isset($field['avatar']['height'])) 
                height: "{{ $field['avatar']['height'] }}", 
                @endif
                
            },
            acceptedFiles: /(\.|\/)(gif|jpe?g|png)$/i,
            plugins: ['drop', 'camera', 'crop'],
            crop: {
                aspectRatio: 1, // Square
                showBtn: cropBtn
            }
        })
        .on('add.filepicker', function (e, data) {
            var file = data.files[0];

            if (file.error) {
                e.preventDefault();
                alert(file.error);
            }
        })
        .on('done.filepicker', function (e, data) {
            // Here the file has been uploaded.
            var file = data.files[0];

            if (file.error) {
                alert(file.error);
            } else {
                // Show the crop modal.
                $(this).filePicker().plugins.crop.show(file.url);
            }
        })
        .on('cropsave.filepicker', function (e, file) {
            // Here the image has been cropped.
            console.log(file);
            // Update the avatar image.
            $('.file-avatar').attr('src', file.versions.avatar.url +'?'+ new Date().getTime());
            $('#{{ $field['name'] }}_avatar').val(file.name);

            // Update the button fileurl.
            cropBtn.data('fileurl', file.url).removeClass('d-none');
            deleteBtn.data('file', file.name).removeClass('d-none');
        })
        .on('fail.filepicker', function () {
            alert('Oops! Something went wrong.');
        });

        // When clicking on the delete button delete the file.
        deleteBtn.on('click', function () {
            // Delete the file.
            $('#filepicker').filePicker().delete($(this).data('file'));

            // Reset default avatar.
            $('.file-avatar').attr('src', 'https://www.gravatar.com/avatar/?d=mm&s=300');
            $('#{{ $field['name'] }}_avatar').val('');
            // Hide crop and delete buttons.
            cropBtn.hide();
            deleteBtn.hide();
        });
    </script>

@endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}