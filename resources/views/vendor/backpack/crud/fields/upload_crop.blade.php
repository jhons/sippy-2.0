@php
    $prefix = isset($field['prefix']) ? $field['prefix'] : '';
    $value = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';
    $value = $value
        ? preg_match('/^data\:image\//', $value)
            ? $value
            : (isset($field['disk'])
                ? Storage::disk($field['disk'])->url($prefix.$value)
                : url($prefix.$value))
        :''; // if validation failed, tha value will be base64, so no need to create a URL for it
@endphp
@include('crud::fields.inc.wrapper_start')
    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::fields.inc.translatable_icon')
    </div>

    <div id="uploaded_image">
        @if(isset($value) && $value)
            <img src="{{ $value }}" alt="Avatar" class="img-fluid rounded d-block" id="profileImage">
        @endif
    </div>
    <input type="file" accept="image/*" name="image" id="upload_image" class="d-none" />
    <input type="hidden" data-handle="hiddenImage" name="{{ $field['name'] }}" id="{{ $field['name'] }}_croppie" value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}">
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif



@include('crud::fields.inc.wrapper_end')

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    <link href="{{ asset('packages/croppie/croppie.css') }}" rel="stylesheet" type="text/css" />
    @endpush

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')

    <div id="uploadimageModal" tabindex="-1" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload & Crop Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div id="image_crop" style="width:450px; margin-top:30px"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary crop_image">Crop & Upload Image</button>
                </div>
            </div>
        </div>
    </div>

<script src="{{ asset('packages/croppie/croppie.js') }}"></script>
<script>  
    $(document).ready(function(){

        $image_crop = $('#image_crop').croppie({
                enableExif: true,
                viewport: {
                width:240,
                height:240,
                type:'square' //circle
            },
            boundary:{
                width:350,
                height:350
            }
        });

        $("#profileImage").click(function(e) {
            $("#upload_image").click();
        });

        $('#upload_image').on('change', function(){
            var reader = new FileReader();
            reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event){
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            })
            .then(function(response){
                $.ajax({
                    url: "{{ backpack_url('upload-crop')}}",
                    type: "POST",
                    data: {image: response, destination: '{{ $field['destination'] }}' },
                    success: function(data)
                    {
                        if (data.status) {
                            $('#uploadimageModal').modal('hide');
                            $('#{{ $field['name'] }}_croppie').val(data.image);
                            $('#uploaded_image').html('<img src="{{ asset('storage/users') }}/' + data.image + '"  alt="Avatar" class="img-fluid rounded d-block" id="profileImage" />');
                        } else {
                            new Noty({
                                type: "error",
                                text: data.message
                            }).show();
                        }
                    }
                });
            })
        });

    });  
</script>

@endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
