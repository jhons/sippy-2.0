@if ($entry->estado_solicitud_registro_id < 4)

	<a href="javascript:void(0)" onclick="notifyEntry(this)" data-route="{{ url($crud->route.'/'.$entry->getKey()).'/notify' }}" class="btn btn-sm btn-link" data-button-type="notify"><i class="la la-sms"></i> {{ trans('mensajes_notificaciones.notify') }}</a>


	{{-- Button Javascript --}}
	{{-- - used right away in AJAX operations (ex: List) --}}
	{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
	@push('after_scripts') @if (request()->ajax()) @endpush @endif
	<script>

		if (typeof notifyEntry != 'function') {
		$("[data-button-type=notify]").unbind('click');

		function notifyEntry(button) {
			// ask for confirmation after notify an item
			// e.preventDefault();

			var button = $(button);
			var route = button.attr('data-route');
			var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

			// location.href = 'whatsapp://send?phone=595' + button.attr('data-number') + '&text=' + encodeURIComponent(button.attr('data-message'));

			swal({
			title: "{!! trans('mensajes_notificaciones.warning') !!}",
			text: "{!! trans('mensajes_notificaciones.notify_confirm') !!}",
			icon: "warning",
			buttons: {
				cancel: {
				text: "{!! trans('mensajes_notificaciones.not_notified') !!}",
				value: null,
				visible: true,
				className: "bg-secondary",
				closeModal: true,
				},
				delete: {
				text: "{!! trans('mensajes_notificaciones.notified') !!}",
				value: true,
				visible: true,
				className: "bg-success",
				}
			},
			}).then((value) => {
				if (value) {
					$.ajax({
					url: route,
					type: 'POST',
					success: function(result) {
						if (result == 1) {
							// Show a success notification bubble
							new Noty({
								type: "success",
								text: "{!! '<strong>'.trans('mensajes_notificaciones.notification_confirmation_title').'</strong><br>'.trans('mensajes_notificaciones.notification_confirmation_message') !!}"
							}).show();

							// Hide the modal, if any
							$('.modal').modal('hide');

							// Change the state
							row.find('.estado').html('Enviado');
						} else {
							// if the result is an array, it means 
							// we have notification bubbles to show
							if (result instanceof Object) {
								// trigger one or more bubble notifications 
								Object.entries(result).forEach(function(entry, index) {
								var type = entry[0];
								entry[1].forEach(function(message, i) {
									new Noty({
										type: type,
										text: message
									}).show();
								});
								});
							} else {// Show an error alert
								swal({
									title: "{!! trans('mensajes_notificaciones.notification_confirmation_not_title') !!}",
									text: "{!! trans('mensajes_notificaciones.notification_confirmation_not_message') !!}",
									icon: "error",
									timer: 4000,
									buttons: false,
								});
							}			          	  
						}
					},
					error: function(result) {
						// Show an alert with the result
						swal({
							title: "{!! trans('mensajes_notificaciones.notification_confirmation_not_title') !!}",
							text: "{!! trans('mensajes_notificaciones.notification_confirmation_not_message') !!}",
							icon: "error",
							timer: 4000,
							buttons: false,
						});
					}
				});
				}
			});

		}
		}

		// make it so that the function above is run after each DataTable draw event
		// crud.addFunctionToDataTablesDrawEventQueue('notifyEntry');
	</script>
	@if (!request()->ajax()) @endpush @endif
@endif