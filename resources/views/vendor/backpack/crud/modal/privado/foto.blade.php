<div id="uploadimageModal" tabindex="-1" class="modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Subir y Cortar Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <div id="image_crop" style="width:450px; margin-top:30px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary crop_image">Cortar y Subir Foto</button>
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('packages/croppie/croppie.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('packages/croppie/croppie.js') }}"></script>
<script>  
    $(document).ready(function(){

        $image_crop = $('#image_crop').croppie({
                enableExif: true,
                viewport: {
                width:500,
                height:500,
                type:'square' //circle
            },
            boundary:{
                width:460,
                height:460
            }
        });
        $("#subirFotoFicha").click(function(e) {
            $("#upload_image").click();
        });
        $('#upload_image').on('change', function(){
            var reader = new FileReader();
            reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event){
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            })
            .then(function(response){
                $.ajax({
                    url: "/upload-crop",
                    type: "POST",
                    data: {image: response, destination: 'personas/fotos' },
                    success: function(data)
                    {
                        if (data.status) {
                            var perfil_persona_id = $("#perfil_persona_id").val();
                            $.ajax({
                                url: "/api/perfil-foto",
                                type: "POST",
                                data: {foto: data.image, perfil_persona_id: perfil_persona_id, principal: true},
                                success: function(data)
                                {
                                    $('#uploadimageModal').modal('hide');
                                    $('#fotoPrincipal').html('<img src="{{ asset('storage/personas/fotos') }}/' + data.foto + '"  alt="Avatar" class="shadow-sm rounded-lg mx-auto d-block img-fluid" style="margin-top: -6.5rem; width:180px"" id="fotoFicha" />');
                                }
                            })
                        } else {
                            new Noty({
                                type: "error",
                                text: data.message
                            }).show();
                        }
                    }
                });
            })
        });

    });  
</script>