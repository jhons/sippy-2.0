<div class="modal fade" id="perfilProfesion" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilProfesionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilProfesionLabel"><span>Acción</span> Profesión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="">
            @csrf
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Profesión</label>
                        <select name="profesion_id" id="profesion_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($listaprofesiones as $profesion)
                                <option value="{{ $profesion->id }}">{{ $profesion->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <input type="hidden" name="fecha" id="fecha" value="">
                        <label>Fecha</label>
                        <div class="input-group date">
                            <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;startView&quot;:&quot;2&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-sm-12" element="div">
                        <label>Otra</label>
                        <input type="text" name="otro" id="otro" class="form-control" value="">
                    </div>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){

            $('#perfilProfesion form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var dataSource = $(this).attr('action');
                var type = ($(this).attr('method')) ?? 'POST'; 

                $.ajax({
                    url:  dataSource,
                    data: $formData,
                    type: type,
                    success: function (result) {
                        console.log(result.profesion_id);
                        var fecha = (result.fecha) ? moment(result.fecha).format('DD/MM/YYYY') : '-';
                        var profesion = (result.profesion_id == 'Otra') ? result.otro : result.profesion.nombre;
                        var route = (result.response_type == 'create') ? dataSource + '/' + result.id : dataSource;
                        var nuevoDocumento = `<td>${profesion}</td><td>${fecha}</td>
                        <td>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="${route}" class="btn btn-sm btn-link" data-button-type="delete" data-id="${result.id}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                            <a href="javascript:void(0)" data-route="${route}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="${result.id}" title="Editar" data-toggle="modal" data-target="#perfilProfesion"><i class="la la-edit la-lg"></i></a>
                        </td>`;
                        if (result.response_type == 'create') {
                            $('#tabla-profesiones .sin-datos').remove();
                            $('#tabla-profesiones tbody').append('<tr role="row">' + nuevoDocumento + '</tr>');
                        } else if (result.response_type == 'edit') {
                            var row = $("#tabla-profesiones a[data-route='"+dataSource+"']").closest('tr');
                            row.html(nuevoDocumento);
                        }

                        $('#perfilProfesion form').trigger("reset");
                        $('#perfilProfesion').modal('hide');
                    }, 
                    error: function(data){
                        // console.log(data);
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#perfilProfesion #'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
                // console.log($formData);
            });
        });
    </script>