<div class="modal fade" id="perfilDireccion" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilDireccionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilDireccionLabel"><span>Acción</span> Dirección</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="">
            @csrf
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Dirección</label>
                        <input type="text" name="direccion" id="direccion" class="form-control" value="">
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Barrio</label>
                        <input type="text" name="barrio" id="barrio" class="form-control" value="">
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <label>País</label>
                        <select name="pais_id" id="pais_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($paises as $pais)
                                <option value="{{ $pais->id }}" @if ($pais->id == 181) selected @endif>{{ $pais->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">    
                        <label>Departamento</label>
                        <select
                            name="departamento_id" 
                            id="departamento_id"
                            style="width: 100%"
                            data-init-function="bpFieldInitSelect2FromAjaxElement"
                            data-column-nullable="true"
                            data-dependencies="[&quot;pais_id&quot;]"
                            data-placeholder="Seleccione un Departamento"
                            data-minimum-input-length="0"
                            data-data-source="{{ url('api/departamento') }}"
                            data-method="GET"
                            data-field-attribute="nombre"
                            data-connected-entity-key-name="id"
                            data-include-all-form-fields="true"
                            data-ajax-delay="500"
                            class="form-control"
                        >
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">    
                        <label>Ciudad</label>
                        <select
                            name="ciudad_id"
                            id="ciudad_id"
                            style="width: 100%"
                            data-init-function="bpFieldInitSelect2FromAjaxElement"
                            data-column-nullable="true"
                            data-dependencies="[&quot;departamento_id&quot;]"
                            data-placeholder="Seleccione una Ciudad"
                            data-minimum-input-length="0"
                            data-data-source="{{ url('api/ciudad') }}"
                            data-method="GET"
                            data-field-attribute="nombre"
                            data-connected-entity-key-name="id"
                            data-include-all-form-fields="true"
                            data-ajax-delay="500"
                            class="form-control"
                        >
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Otra Ciudad</label>
                        <input type="text" name="ciudad" id="ciudad" class="form-control" value="">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){

            $('#perfilDireccion form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var dataSource = $(this).attr('action');
                var type = ($(this).attr('method')) ?? 'POST'; 

                $.ajax({
                    url:  dataSource,
                    data: $formData,
                    type: type,
                    success: function (result) {

                        var fecha = (result.updated_at) ? moment(result.updated_at).format('DD/MM/YYYY') : '-';
                        var barrio = (result.barrio) ? result.barrio : '-';
                        var route = (result.response_type == 'create') ? dataSource + '/' + result.id : dataSource;
                        var nuevoDocumento = `<td>${result.direccion}</td><td>${barrio}</td><td>${result.ciudad_id}</td><td>${result.departamento_id}</td><td>${result.pais_id}</td><td>${fecha}</td>
                        <td>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="${route}" class="btn btn-sm btn-link" data-button-type="delete" data-id="${result.id}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                            <a href="javascript:void(0)" data-route="${route}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="${result.id}" title="Editar" data-toggle="modal" data-target="#perfilDireccion"><i class="la la-edit la-lg"></i></a>
                        </td>`;
                        if (result.response_type == 'create') {
                            $('#tabla-direcciones .sin-datos').remove();
                            $('#tabla-direcciones tbody').append('<tr role="row">' + nuevoDocumento + '</tr>');
                        } else if (result.response_type == 'edit') {
                            var row = $("#tabla-direcciones a[data-route='"+dataSource+"']").closest('tr');
                            row.html(nuevoDocumento);
                        }

                        $('#perfilDireccion form').trigger("reset");
                        $('#perfilDireccion').modal('hide');
                    }, 
                    error: function(data){

                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
            });
        });
    </script>