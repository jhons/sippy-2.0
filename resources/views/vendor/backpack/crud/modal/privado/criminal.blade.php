<div class="modal fade" id="perfilCriminal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilCriminalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilCriminalLabel">Editar Datos Varios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="{{ url('api/perfil-criminal') }}">
            @csrf
            
            <div class="modal-body">

                <div class="row form bold-labels">
                     

                    <div class="form-group col-sm-6" element="div">
                        <label>Nivel Peligrosidad</label>
                        <select name="nivel_peligrosidad_id" id="nivel_peligrosidad_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($peligrosidades as $peligrosidad)
                                <option value="{{ $peligrosidad->id }}">{{ $peligrosidad->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Grupo Criminal</label>
                        <select name="grupo_criminal_id" id="grupo_criminal_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            <option value="">-</option>
                            @foreach($grupos_criminales as $grupo)
                                <option value="{{ $grupo->id }}">{{ $grupo->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <label>Grupos Vulnerables</label>
                        <select name="grupo_vulnerable_id" id="grupo_vulnerable_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            <option value="">-</option>
                            @foreach($vulnerables as $vulnerable)
                                <option value="{{ $vulnerable->id }}">{{ $vulnerable->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <label>Observación</label>
                        <input type="text" name="observaciones" id="observaciones" class="form-control" value="">
                    </div>
                    

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){
            $('#perfilCriminal').on('shown.bs.modal', function () {
                var origen = $('a[data-target="#perfilCriminal"]');
                if (origen.attr('data-peligrosidad'))    
                    $('#nivel_peligrosidad_id').val(origen.attr('data-peligrosidad')).trigger('change');

                $('#grupo_criminal_id').val(origen.attr('data-criminal')).trigger('change');
                $('#grupo_vulnerable_id').val(origen.attr('data-vulnerable')).trigger('change');
                $('#observaciones').val(origen.attr('data-observacion'));
            });

            $('#perfilCriminal form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var $dataSource = $(this).attr('action');

                $.ajax({
                    url: $dataSource,
                    data: $formData,
                    type: 'POST',
                    success: function (result) {
                        console.log(result);

                        $('a[data-target="#perfilCriminal"]').attr({'data-vulnerable': ((result.grupo_vulnerable_id)??''), 'data-peligrosidad': ((result.nivel_peligrosidad_id)??''), 'data-criminal': ((result.grupo_criminal_id)??''), 'data-observacion': result.observaciones});
                        
                        console.log($('a[data-target="#perfilCriminal"]'));

                        var vulnerable = (result.grupo_vulnerable_id) ? result.grupo_vulnerable : '-';
                        $('#grupo-vulnerable').html(vulnerable);

                        var peligrosidad = (result.nivel_peligrosidad) ? '<i class="las la-square" style="color:'+result.nivel_peligrosidad.color+'"></i> ' + result.nivel_peligrosidad.nombre : '-';
                        $('#nivel-peligrosidad').html(peligrosidad);

                        var criminal = (result.grupo_criminal) ? result.grupo_criminal.nombre : '-';
                        $('#grupo-criminal').html(criminal);

                        $('#perfilCriminal').modal('hide');
                    }, 
                    error: function(data){
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
            });
        });
    </script>