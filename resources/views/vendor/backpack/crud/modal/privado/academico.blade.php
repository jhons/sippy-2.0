<div class="modal fade" id="perfilAcademico" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilAcademicoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilAcademicoLabel"><span>Acción</span> Formación Académica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="">
            @csrf
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Institución</label>
                        <select name="institucion_id" id="institucion_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            <option value="">-</option>
                            @foreach($instituciones as $institucion)
                                <option value="{{ $institucion->id }}">{{ $institucion->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Otra</label>
                        <input type="text" name="otro" id="otro" class="form-control" value="">
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Nivel Académico</label>
                        <select name="nivel_academico_id" id="nivel_academico_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($niveles as $nivel)
                                <option value="{{ $nivel->id }}">{{ $nivel->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">    
                        <label>Grado Académico</label>
                        <select
                            name="grado_academico_id" 
                            id="grado_academico_id"
                            style="width: 100%"
                            data-init-function="bpFieldInitSelect2FromAjaxElement"
                            data-column-nullable="true"
                            data-dependencies="[&quot;nivel_academico_id&quot;]"
                            data-placeholder="Seleccione un Grado Académico"
                            data-minimum-input-length="0"
                            data-data-source="{{ url('api/grado_academico') }}"
                            data-method="GET"
                            data-field-attribute="nombre"
                            data-connected-entity-key-name="id"
                            data-include-all-form-fields="true"
                            data-ajax-delay="500"
                            class="form-control"
                        >
                        </select>
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <label>Año</label>
                        <input type="text" name="anio" id="anio" class="form-control" value="">
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <input type="hidden" name="fecha_promocion" id="fecha_promocion" value="">
                        <label>Promoción</label>
                        <div class="input-group date">
                            <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;startView&quot;:&quot;2&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){

            $('#perfilAcademico form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var dataSource = $(this).attr('action');
                var type = ($(this).attr('method')) ?? 'POST'; 

                $.ajax({
                    url:  dataSource,
                    data: $formData,
                    type: type,
                    success: function (result) {

                        // var validez = (result.validez) ? moment(result.validez).format('DD/MM/YYYY') : '-';
                        var route = (result.response_type == 'create') ? dataSource + '/' + result.id : dataSource;
                        var institucion = (result.institucion_id) ? result.institucion.nombre : result.otro;
                        var nivel_academico = (result.nivel_academico_id) ? result.nivel_academico.nombre : '-';
                        var grado_academico = (result.grado_academico_id) ? result.grado_academico.nombre : '-';
                        var anio = (result.anio) ? result.anio : '-';
                        var nuevoAcademico = `<td>${institucion}</td><td>${nivel_academico}</td><td>${grado_academico}</td><td>${anio}</td>
                        <td>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="${route}" class="btn btn-sm btn-link" data-button-type="delete" data-id="${result.id}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                            <a href="javascript:void(0)" data-route="${route}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="${result.id}" title="Editar" data-toggle="modal" data-target="#perfilAcademico"><i class="la la-edit la-lg"></i></a>
                        </td>`;
                        if (result.response_type == 'create') {
                            $('#tabla-academicos .sin-datos').remove();
                            $('#tabla-academicos tbody').append('<tr role="row">' + nuevoAcademico + '</tr>');
                        } else if (result.response_type == 'edit') {
                            var row = $("#tabla-academicos a[data-route='"+dataSource+"']").closest('tr');
                            row.html(nuevoAcademico);
                        }

                        $('#perfilAcademico form').trigger("reset");
                        $('#perfilAcademico').modal('hide');
                    }, 
                    error: function(data){
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#perfilAcademico #'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
            });
        });
    </script>