<div id="uploadHuellaModal" tabindex="-1" class="modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Subir y Cortar Huella</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <div id="huella_crop" style="width:450px; margin-top:30px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary crop_huella">Cortar y Subir Huella</button>
                <input type="hidden" id="huella_tipo_id">
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('packages/croppie/croppie.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('packages/croppie/croppie.js') }}"></script>
<script>  
    $(document).ready(function(){
        var huella_tipo_id = null;
        $image_crop = $('#huella_crop').croppie({
                enableExif: true,
                viewport: {
                width:500,
                height:500,
                type:'square' //circle
            },
            boundary:{
                width:460,
                height:460
            }
        });
        $(".subirHuellaDactilar").on('click', function(e) {
            e.preventDefault();
            $("#upload_huella").click();
            var huella_tipo_id = $(this).data('tipo');
            $('#huella_tipo_id').val(huella_tipo_id);
            console.log('huella_tipo_id ' + huella_tipo_id);
        });
        $('#upload_huella').on('change', function(){
            console.log('upload_huella');
            var reader = new FileReader();
            reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadHuellaModal').modal('show');
        });

        $('.crop_huella').click(function(event){
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            })
            .then(function(response){
                $.ajax({
                    url: "/upload-crop",
                    type: "POST",
                    data: {image: response, destination: 'personas/huellas' },
                    success: function(data)
                    {
                        if (data.status) {
                            var perfil_persona_id = $("#perfil_persona_id").val();
                            var huella_tipo_id = $("#huella_tipo_id").val();

                            $.ajax({
                                url: "/api/perfil-huella",
                                type: "POST",
                                data: {captura: data.image, perfil_persona_id: perfil_persona_id, huella_tipo_id: huella_tipo_id},
                                success: function(data)
                                {
                                    var tipo = (huella_tipo_id == 6 || huella_tipo_id == 12) ? 'palma' : 'dedo';
                                    $('#huella-' + huella_tipo_id).html('<img src="{{ asset('assets/img/') }}/huella-'+tipo+'-ok.png" alt="Huella" class="img-thumbnail img-fluid" />');
                                    $("#huella_tipo_id").val('');
                                    $('#uploadHuellaModal').modal('hide');
                                }
                            })
                        } else {
                            new Noty({
                                type: "error",
                                text: data.message
                            }).show();
                        }
                    }
                });
            })
        });

    });  
</script>