<div class="modal fade" id="perfilDocumento" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilDocumentoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilDocumentoLabel"><span>Acción</span> Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="">
            @csrf
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Tipo Documento</label>
                        <select name="identidad_tipo_id" id="identidad_tipo_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($documentos_tipos as $tipo)
                                <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>País Emisor</label>
                        <select name="pais_emisor_id" id="pais_emisor_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($paises as $pais)
                                <option value="{{ $pais->id }}" @if ($pais->id == 181) selected @endif>{{ $pais->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Documento</label>
                        <input type="text" name="documento" id="documento" class="form-control" value="">
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <input type="hidden" name="validez" id="validez" value="">
                        <label>Validéz</label>
                        <div class="input-group date">
                            <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;startView&quot;:&quot;2&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){

            $('#perfilDocumento form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var dataSource = $(this).attr('action');
                var type = ($(this).attr('method')) ?? 'POST'; 

                $.ajax({
                    url:  dataSource,
                    data: $formData,
                    type: type,
                    success: function (result) {

                        var validez = (result.validez) ? moment(result.validez).format('DD/MM/YYYY') : '-';
                        var route = (result.response_type == 'create') ? dataSource + '/' + result.id : dataSource;
                        var nuevoDocumento = `<td>${result.identidad_tipo.nombre}</td><td>${result.documento}</td><td>${result.pais_emisor.nombre}</td><td>${validez}</td>
                        <td>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="${route}" class="btn btn-sm btn-link" data-button-type="delete" data-id="${result.id}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                            <a href="javascript:void(0)" data-route="${route}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="${result.id}" title="Editar" data-toggle="modal" data-target="#perfilDocumento"><i class="la la-edit la-lg"></i></a>
                        </td>`;
                        if (result.response_type == 'create') {
                            $('#tabla-documentos .sin-datos').remove();
                            $('#tabla-documentos tbody').append('<tr role="row">' + nuevoDocumento + '</tr>');
                        } else if (result.response_type == 'edit') {
                            var row = $("#tabla-documentos a[data-route='"+dataSource+"']").closest('tr');
                            row.html(nuevoDocumento);
                        }

                        $('#perfilDocumento form').trigger("reset");
                        $('#perfilDocumento').modal('hide');
                    }, 
                    error: function(data){
                        // console.log(data);
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
                // console.log($formData);
            });
        });
    </script>