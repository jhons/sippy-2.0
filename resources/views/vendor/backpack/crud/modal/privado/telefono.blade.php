<div class="modal fade" id="perfilTelefono" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilTelefonoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilTelefonoLabel"><span>Acción</span> Teléfono</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="">
            @csrf
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Tipo Teléfono</label>
                        <select name="telefono_tipo_id" id="telefono_tipo_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($telefonos_tipos as $tipo)
                                <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-6" element="div">
                        <label>Número</label>
                        <input type="text" name="telefono" id="telefono" class="form-control" value="">
                    </div>

                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_persona_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){

            $('#perfilTelefono form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var dataSource = $(this).attr('action');
                var type = ($(this).attr('method')) ?? 'POST'; 

                $.ajax({
                    url:  dataSource,
                    data: $formData,
                    type: type,
                    success: function (result) {

                        var validez = (result.validez) ? moment(result.validez).format('DD/MM/YYYY') : '-';
                        var route = (result.response_type == 'create') ? dataSource + '/' + result.id : dataSource;
                        var nuevoDocumento = `<td>${result.telefono_tipo.nombre}</td><td>${result.telefono}</td>
                        <td>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="${route}" class="btn btn-sm btn-link" data-button-type="delete" data-id="${result.id}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                            <a href="javascript:void(0)" data-route="${route}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="${result.id}" title="Editar" data-toggle="modal" data-target="#perfilTelefono"><i class="la la-edit la-lg"></i></a>
                        </td>`;
                        if (result.response_type == 'create') {
                            $('#tabla-telefonos .sin-datos').remove();
                            $('#tabla-telefonos tbody').append('<tr role="row">' + nuevoDocumento + '</tr>');
                        } else if (result.response_type == 'edit') {
                            var row = $("#tabla-telefonos a[data-route='"+dataSource+"']").closest('tr');
                            row.html(nuevoDocumento);
                        }

                        $('#perfilTelefono form').trigger("reset");
                        $('#perfilTelefono').modal('hide');
                    }, 
                    error: function(data){
                        // console.log(data);
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
                // console.log($formData);
            });
        });
    </script>