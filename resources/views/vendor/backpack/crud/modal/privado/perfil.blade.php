<div class="modal fade" id="perfilBasico" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="perfilBasicoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="perfilBasicoLabel">Editar Datos Personales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form bold-labels" action="{{ url('api/persona') }}">
            @csrf
            
            <div class="modal-body">

                <div class="row form bold-labels">
                    <div class="form-group col-sm-6" element="div">
                        <label>Nombres</label>
                        <input type="text" name="nombres" id="nombres" value="{{ $perfil->nombres }}" class="form-control" >
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Apellidos</label>
                        <input type="text" name="apellidos" id="apellidos" value="{{ $perfil->apellidos ?? '' }}" class="form-control" >
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Apodo</label>
                        <input type="text" name="apodo" value="{{ $perfil->apodo ?? '' }}" class="form-control" >
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Alias</label>
                        <input type="text" name="alias" value="{{ $perfil->alias ?? '' }}" class="form-control" >
                    </div>         
                    <div class="form-group col-sm-6" element="div">
                        <label>Estado Civil</label>
                        <select name="estado_civil_id" id="estado_civil_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            <option value="">-</option>
                            @foreach($estados_civiles as $estado_civil)
                                <option value="{{ $estado_civil->id }}" @if($perfil->estado_civil_id == $estado_civil->id) selected @endif>{{ $estado_civil->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Género</label>
                        <select name="genero_id" id="genero_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            @foreach($generos as $genero)
                                <option value="{{ $genero->id }}" @if($perfil->genero_id == $genero->id) selected @endif>{{ $genero->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <input type="hidden" name="fecha_nacimiento" id="fecha_nacimiento" value="{{ $perfil->fecha_nacimiento->format('Y-m-d') ?? '' }}">
                        <label>Fecha Nacimiento</label>
                        <div class="input-group date">
                            <input data-bs-datepicker="{&quot;todayBtn&quot;:&quot;linked&quot;,&quot;format&quot;:&quot;dd\/mm\/yyyy&quot;,&quot;language&quot;:&quot;es&quot;,&quot;endDate&quot;:&quot;new date()&quot;}" data-init-function="bpFieldInitDatePickerElement" type="text" style="background-color: white!important;" readonly="readonly" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span class="la la-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-6" element="div">
                        <label>Etnia</label>
                        <select name="etnia_id" id="etnia_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field">
                            <option value="">-</option>
                            @foreach($etnias as $etnia)
                                <option value="{{ $etnia->id }}" @if($perfil->etnia_id == $etnia->id) selected @endif>{{ $etnia->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <input type="hidden" name="perfil_id" value="{{ $perfil->id }}">
            </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){
            $('#perfilBasico').on('shown.bs.modal', function () {
                $('#estado_civil_id').val($('#perfil-estado_civil').data('estado-civil-id')).trigger('change');
                $('#genero_id').val($('#perfil-genero').data('genero-id')).trigger('change');
                $('#etnia_id').val( $('#perfil-etnia').data('etnia-id')).trigger('change');
                $('#fecha_nacimiento').val($('#perfil-fecha_nacimiento').data('fecha-nacimiento'));
                bpFieldInitDatePickerElement($('#perfilBasico .date input'));
                $('#nombres').val($('#perfil-nombres').text());
                $('#apellidos').val($('#perfil-apellidos').text());
                document.getElementsByName('apodo')[0].value = ($('#perfil-apodo').text() !== '-') ? $('#perfil-apodo').text() : '';
                document.getElementsByName('alias')[0].value = ($('#perfil-alias').text() !== '-') ? $('#perfil-alias').text() : '';
                
            });

            $('#perfilBasico form').on('submit', function(e) {
                e.preventDefault();
                var $formData = $(this).serialize();
                var $dataSource = $(this).attr('action');

                $.ajax({
                    url: $dataSource,
                    data: $formData,
                    type: 'POST',
                    success: function (result) {
                        $('#perfil-nombres').html(result.nombres).data();
                        $('#perfil-apellidos').html(result.apellidos);
                        $('#perfil-apodo').html(result.apodo);
                        $('#perfil-alias').html(result.alias);
                        $('#perfil-estado_civil').html(result.estado_civil_id).data('estado-civil-id', (result.estado_civil) ? result.estado_civil.id : '');
                        $('#perfil-genero').html(result.genero_id).data('genero-id', (result.genero) ? result.genero.id : '');
                        $('#perfil-fecha_nacimiento').html(result.fecha_nacimiento_texto).data('fecha-nacimiento', result.fecha_nacimiento);
                        $('#perfil-etnia').html(result.etnia_id).data('etnia-id', (result.etnia) ? result.etnia.id : '');
                        $('#perfilBasico').modal('hide');
                    }, 
                    error: function(data){
                        if (data.status == 422){
                            $.each(data.responseJSON.errors, function(index, value){
                                $('#'+index).addClass('is-invalid').parent().addClass('required text-danger').append('<div class="invalid-feedback">'+value+'</div>');
                            });
                        }
                    }
                });
            });
        });
    </script>