@extends(backpack_view('blank'))

@php
  /* $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];*/

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  //$breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;

  $perfil = $crud->entry->persona;
  $documentos = $crud->entry->persona->identidades;
  $direcciones = $crud->entry->persona->direcciones;
  $telefonos = $crud->entry->persona->telefonos;
  $profesiones = $crud->entry->persona->profesiones;
  $academicos = $crud->entry->persona->academicos;
  $criminal = $crud->entry->persona->criminal;
  $grupo_criminal = ($criminal->count() > 0 && isset($criminal[0]->grupo_criminal_id)) ? $criminal[0]->grupo_criminal : false;
  $nivel_peligrosidad = ($criminal->count() > 0 && isset($criminal[0]->nivel_peligrosidad_id)) ? $criminal[0]->nivel_peligrosidad : false;
  $huellas = collect($crud->entry->persona->huellas_tipos)->pluck('huella_tipo_id')->all();

//   dd(in_array(1, $huellas));
  
  $estados_civiles = \App\Models\EstadoCivil::all();
  $generos = \App\Models\Genero::all();
  $etnias = \App\Models\Etnia::all();
  $documentos_tipos = \App\Models\IdentidadTipo::all();
  $paises = \App\Models\Pais::all();
  $telefonos_tipos = \App\Models\TelefonoTipo::all();
  $listaprofesiones = \App\Models\Profesion::all();
  $instituciones = \App\Models\Institucion::all();
  $niveles = \App\Models\NivelAcademico::all();
  $peligrosidades = \App\Models\NivelPeligrosidad::all();
  $grupos_criminales = \App\Models\GrupoCriminal::all();
  $vulnerables = \App\Models\GrupoVulnerable::all();

@endphp

@section('header')
	<!-- <section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section> -->
@endsection

@section('content')
  <div class="d-flex row mt-4" id="perfil-persona">
    <div class="col-12 d-lg-none">
        <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mb-3 text-right" role="group" aria-label="Basic example">
                <a href="#" role="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Imprimir"><i class="la la-print la-lg"></i></a>
                <a href="#" role="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Descargar"><i class="la la-download la-lg"></i></a>
            </div>
        </div>
        <h3>{{ $perfil->nombres }} {{ $perfil->apellidos }}</h3>
        <p class="card-subtitle">{{ $crud->entry->getCentroActual() }} | {{ $crud->entry->getPabellonActual() }}</p>
    </div>
    <div class="col-sm-4 col-lg-3 col-xl-2">
        <ul class="list-group" style="margin-top: 6rem">
            <li class="list-group-item justify-content-between align-items-center">
                <div>
                    <div id="fotoPrincipal">
                        <img src="{{ $perfil->foto_principal ? asset('storage/personas/fotos/' . $perfil->foto_principal->foto)  : asset('assets/img/avatar.png')}}" alt="" class="shadow-sm rounded-lg mx-auto d-block img-fluid" style="margin-top: -6.5rem; width:180px" id="fotoFicha">
                    </div>
                    <div class="foto-perfil">
                        <a href="#" class="btn btn-link" id="subirFotoFicha"><i class="la la-photo"></i></a>
                    </div>
                    <input id="upload_image" type="file" accept="image/*" name="foto" placeholder="Foto" required="" capture class="d-none">
                    <input type="hidden" id="perfil_persona_id" value="{{ $perfil->id }}">
                </div>
                <p class="d-block mt-2 mb-0 text-center">N° <span class="card-text">{{ $crud->entry->getKey() }}</span></p>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Causas
                <span class="badge badge-primary badge-pill">14</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Traslados
                <span class="badge badge-primary badge-pill">5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Visitas
                <span class="badge badge-success badge-pill">10</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Incidentes
                <span class="badge badge-danger badge-pill">19</span>
            </li>
        </ul>
        <div class="list-group mt-4">
            <button type="button" class="list-group-item list-group-item-warning list-group-item-action">
            <i class="la la-exchange-alt la-lg"></i> Trasladar
            </button>
            <button type="button" class="list-group-item list-group-item-warning list-group-item-action"><i class="la la-walking la-lg"></i> Salida transitoria</button>
            <button type="button" class="list-group-item list-group-item-warning list-group-item-action"><i class="la la-lock-open la-lg"></i> Libertad</button>
            <button type="button" class="list-group-item list-group-item-warning list-group-item-action"><i class="la la-user-times la-lg"></i> Baja</button>
            <button type="button" class="list-group-item list-group-item-warning list-group-item-action"><i class="la la-thumbs-down la-lg"></i> Incidente</button>
        </div>
    </div>
    <div class="col-sm-8 col-lg-3 col-xl-2 order-lg-8">
        <div class="btn-toolbar d-none d-lg-inline-block text-right w-100" role="toolbar" aria-label="Barra de herramientas">
            <div class="btn-group mb-4" role="group" aria-label="Basic example">
                <a href="#" role="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Imprimir"><i class="la la-print la-lg"></i></a>
                <a href="#" role="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Descargar"><i class="la la-download la-lg"></i></a>
            </div>
        </div>
        <div class="card mt-4 mt-sm-0">
            <div class="card-body">
                <a href="#" class="float-right" title="Editar" data-toggle="modal" data-target="#perfilCriminal" data-vulnerable="{{ $perfil->grupo_vulnerable_id }}" data-criminal="{{ ($grupo_criminal) ? $grupo_criminal->id : '' }}" data-peligrosidad="{{ ($nivel_peligrosidad) ? $nivel_peligrosidad->id : '' }}" data-observacion="{{ ($criminal->count() > 0)  ? $criminal[0]->observaciones : '' }}"><i class="la la-edit la-lg"></i></a>
                <h6 class="card-subtitle text-muted">Grupo Vulnerable</h6>
                <p class="card-text" id="grupo-vulnerable">{{ ($perfil->grupo_vulnerable_id) ? $perfil->grupo_vulnerable->nombre : '-' }}</p>
                <h6 class="card-subtitle text-muted">Nivel Peligrosidad</h6>
                <p class="card-text" id="nivel-peligrosidad">
                    @if($nivel_peligrosidad)
                    <i class="las la-square" style="color:{{ $nivel_peligrosidad->color }}"></i> {{ $nivel_peligrosidad->nombre }}
                    @else
                    -
                    @endif
                </p>
                <h6 class="card-subtitle text-muted">Grupo Criminal</h6>
                <p class="card-text" id="grupo-criminal">{{ ($grupo_criminal) ? $grupo_criminal->nombre : '-' }}</p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <a href="#" class="float-right" data-toggle="tooltip" data-placement="bottom" title="Agregar"><i class="la la-plus-square la-lg"></i></a>
                <h6 class="card-subtitle text-muted">Relacionados</h6>
                <div class="row">
                    <div class="col-6 col-sm-4 col-lg-6">
                        <a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Luis Enrique Urrieta" data-content="And here's some amazing content. It's very engaging. Right?">
                            <img src="{{ asset('storage/personas/fotos/foto.png') }}" alt="" class="rounded-circle img-fluid mt-3" >
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 col-lg-6">
                        <a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Luis Enrique Urrieta" data-content="And here's some amazing content. It's very engaging. Right?">
                            <img src="{{ asset('storage/personas/fotos/foto.png') }}" alt="" class="rounded-circle img-fluid mt-3" >
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 col-lg-6">
                        <a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Luis Enrique Urrieta" data-content="And here's some amazing content. It's very engaging. Right?">
                            <img src="{{ asset('storage/personas/fotos/foto.png') }}" alt="" class="rounded-circle img-fluid mt-3" >
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 col-lg-6">
                        <a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Luis Enrique Urrieta" data-content="And here's some amazing content. It's very engaging. Right?">
                            <img src="{{ asset('storage/personas/fotos/foto.png') }}" alt="" class="rounded-circle img-fluid mt-3" >
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 col-lg-6">
                        <a tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Luis Enrique Urrieta" data-content="And here's some amazing content. It's very engaging. Right?">
                            <img src="{{ asset('storage/personas/fotos/foto.png') }}" alt="" class="rounded-circle img-fluid mt-3" >
                        </a>
                    </div>
                    <div class="col-6 col-sm-4 col-lg-6 pt-3">
                        <a href="#" class="btn btn-link rounded-circle w-100 h-100 d-flex align-items-center justify-content-center">+6</a>
                    </div>
                    <div class="col text-center mt-3">
                        <a href="#" class="btn btn-light"><i class="la la-edit la-lg"></i> Editar</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-12 col-lg-6 col-xl-8 order-lg-4">
        <h3 class="d-none d-lg-block mb-0">{{ $perfil->nombres }} {{ $perfil->apellidos }}</h3>
        <p class="card-subtitle d-none d-lg-block">{{ $crud->entry->getCentroActual() }} | {{ $crud->entry->getPabellonActual() }}</p>
        <div class="nav-tabs-custom" id="form_tabs">
            <ul class="nav nav-tabs " role="tablist">
                <li role="presentation" class="nav-item">
                    <a href="#tab_datos" 
                        aria-controls="tab_datos" 
                        role="tab" 
                        tab_name="datos" 
                        data-toggle="tab" 
                        class="nav-link active"
                        >Datos Personales</a>
                </li>
                <li role="presentation" class="nav-item">
                    <a href="#tab_relaciones" 
                        aria-controls="tab_relaciones" 
                        role="tab" 
                        tab_name="relaciones" 
                        data-toggle="tab" 
                        class="nav-link "
                        >Datos Relacionales</a>
                </li>
                <li role="presentation" class="nav-item">
                    <a href="#tab_penales" 
                        aria-controls="tab_penales" 
                        role="tab" 
                        tab_name="penales" 
                        data-toggle="tab" 
                        class="nav-link "
                        >Datos Penales</a>
                </li>
                <li role="presentation" class="nav-item">
                    <a href="#tab_huellas" 
                        aria-controls="tab_huellas" 
                        role="tab" 
                        tab_name="huellas" 
                        data-toggle="tab" 
                        class="nav-link "
                        >Huellas Dactilares</a>
                </li>
                <li role="presentation" class="nav-item">
                    <a href="#tab_documentos" 
                        aria-controls="tab_documentos" 
                        role="tab" 
                        tab_name="documentos" 
                        data-toggle="tab" 
                        class="nav-link "
                        >Documentos Varios</a>
                </li>
            </ul>
            <div class="tab-content p-0 ">
                <div role="tabpanel" class="tab-pane fade show active p-4" id="tab_datos">
                    <a href="#" class="float-right" title="Editar" data-toggle="modal" data-target="#perfilBasico"><i class="la la-edit la-lg"></i></a>
                    <div class="row no-gutters perfil">
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Nombres</h6>
                            <p class="card-text" id="perfil-nombres">{{ $perfil->nombres }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Apellidos</h6>
                            <p class="card-text" id="perfil-apellidos">{{ $perfil->apellidos ?? '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Apodo</h6>
                            <p class="card-text" id="perfil-apodo">{{ $perfil->apodo ?? '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Alias</h6>
                            <p class="card-text" id="perfil-alias">{{ $perfil->alias ?? '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Estado Civil</h6>
                            <p class="card-text" id="perfil-estado_civil" data-estado-civil-id="{{ $perfil->estado_civil_id ?? '' }}">{{ $perfil->estado_civil->nombre ?? '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Género</h6>
                            <p class="card-text" id="perfil-genero" data-genero-id="{{ $perfil->genero_id ?? '' }}">{{ $perfil->genero->nombre ?? '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Fecha de Nacimiento</h6>
                            <p class="card-text" id="perfil-fecha_nacimiento" data-fecha-nacimiento="{{ $perfil->fecha_nacimiento->format('Y-m-d') ?? '' }}">{{ $perfil->fecha_nacimiento ? \Carbon\Carbon::parse($perfil->fecha_nacimiento)
                                ->locale(App::getLocale())
                                ->isoFormat($column['format'] ?? config('backpack.base.default_date_format')) : '-' }}</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="card-subtitle">Étnia</h6>
                            <p class="card-text" id="perfil-etnia" data-etnia-id="{{ $perfil->etnia_id?? '' }}">{{ $perfil->etnia->nombre ?? '-' }}</p>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="row documentos">
                        <div class="col-12">
                            <a href="#" class="float-right btn btn-sm btn-add btn-link" title="Agregar" data-toggle="modal" data-target="#perfilDocumento" data-route="{{ url('api/perfil-documento') }}"><span>Agregar</span> <i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Documentos</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless perfilTable" id="tabla-documentos">
                                    <tbody>
                                    @if ($documentos->count() > 0)
                                        @foreach($documentos as $documento)
                                        <tr role="row">
                                            <td>{{ $documento->identidad_tipo->nombre }}</td>
                                            <td>{{ $documento->documento }}</td>
                                            <td>{{ $documento->pais_emisor->nombre }}</td>
                                            <td>{{ ($documento->validez) ? $documento->validez->format("d/m/Y") : '-' }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url('api/perfil-documento/' . $documento->id) }}" class="btn btn-sm btn-link" data-button-type="delete" data-id="{{ $documento->id }}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                                                <a href="javascript:void(0)" data-route="{{ url('api/perfil-documento/' . $documento->id) }}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="{{ $documento->id }}" title="Editar" data-toggle="modal" data-target="#perfilDocumento"><i class="la la-edit la-lg"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="sin-datos">
                                            <td colspan="5">
                                            No hay documentos, agregar
                                            <a href="#" class="btn btn-link btn-sm btn-add" title="Agregar" data-toggle="modal" data-target="#perfilDocumento" data-route="{{ url('api/perfil-documento') }}"><i class="la la-plus-square la-lg"></i> nuevo</a>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="row direcciones">
                        <div class="col">
                            <a href="#" class="float-right btn btn-sm btn-add btn-link" title="Agregar" data-toggle="modal" data-target="#perfilDireccion" data-route="{{ url('api/perfil-direccion') }}"><span>Agregar</span> <i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Direcciones Conocidas</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless perfilTable" id="tabla-direcciones">
                                    <tbody>
                                    @if ($direcciones->count() > 0)
                                        @foreach($direcciones as $direccion)
                                        <tr role="row">
                                            <td>{{ $direccion->direccion}}</td>
                                            <td>{{ ($direccion->barrio) ? $direccion->barrio : '-' }}</td>
                                            <td>{{ ($direccion->ciudad_id) ? $direccion->laciudad->nombre : '-' }}</td>
                                            <td>{{ ($direccion->departamento_id) ? $direccion->departamento->nombre : '-' }}</td>
                                            <td>{{ $direccion->pais->nombre }}</td>
                                            <td>{{ $direccion->updated_at->format("d/m/Y") }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url('api/perfil-direccion/' . $direccion->id) }}" class="btn btn-sm btn-link" data-button-type="delete" data-id="{{ $direccion->id }}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                                                <a href="javascript:void(0)" data-route="{{ url('api/perfil-direccion/' . $direccion->id) }}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="{{ $direccion->id }}" title="Editar" data-toggle="modal" data-target="#perfilDireccion"><i class="la la-edit la-lg"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="sin-datos">
                                            <td colspan="7">
                                            No hay direcciones, agregar
                                            <a href="#" class="btn btn-link btn-sm btn-add" title="Agregar" data-toggle="modal" data-target="#perfilDireccion" data-route="{{ url('api/perfil-direccion') }}"><i class="la la-plus-square la-lg"></i> nueva</a>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="row">
                        <div class="col-md-6 telefonos">
                            <a href="#" class="float-right btn btn-sm btn-add btn-link" title="Agregar" data-toggle="modal" data-target="#perfilTelefono" data-route="{{ url('api/perfil-telefono') }}"><span>Agregar</span> <i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Teléfonos Registrados</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless perfilTable" id="tabla-telefonos">
                                    <tbody>
                                        @if ($telefonos->count() > 0)
                                        @foreach($telefonos as $telefono)
                                        <tr role="row">
                                            <td>{{ $telefono->telefono_tipo->nombre}}</td>
                                            <td>{{ $telefono->telefono }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url('api/perfil-telefono/' . $telefono->id) }}" class="btn btn-sm btn-link" data-button-type="delete" data-id="{{ $telefono->id }}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                                                <a href="javascript:void(0)" data-route="{{ url('api/perfil-telefono/' . $telefono->id) }}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="{{ $telefono->id }}" title="Editar" data-toggle="modal" data-target="#perfilTelefono"><i class="la la-edit la-lg"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr class="sin-datos">
                                            <td colspan="3">
                                            No hay teléfonos, agregar
                                            <a href="#" class="btn btn-link btn-sm btn-add" title="Agregar" data-toggle="modal" data-target="#perfilTelefono" data-route="{{ url('api/perfil-telefono') }}"><i class="la la-plus-square la-lg"></i> nuevo</a>
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 laboral">
                            <a href="#" class="float-right btn btn-sm btn-add btn-link" title="Agregar" data-toggle="modal" data-target="#perfilProfesion" data-route="{{ url('api/perfil-profesion') }}"><span>Agregar</span> <i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Información Laboral</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless perfilTable" id="tabla-profesiones">
                                    <tbody>
                                    @if ($profesiones->count() > 0)
                                        @foreach($profesiones as $profesion)
                                        <tr role="row">
                                            <td>{{ ($profesion->profesion_id != 501) ? $profesion->profesion->nombre : $profesion->otro }}</td>
                                            <td>{{ ($profesion->fecha) ? $profesion->fecha->format("d/m/Y") : '-' }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url('api/perfil-profesion/' . $profesion->id) }}" class="btn btn-sm btn-link" data-button-type="delete" data-id="{{ $profesion->id }}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                                                <a href="javascript:void(0)" data-route="{{ url('api/perfil-profesion/' . $profesion->id) }}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="{{ $profesion->id }}" title="Editar" data-toggle="modal" data-target="#perfilProfesion"><i class="la la-edit la-lg"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="sin-datos">
                                            <td colspan="3">
                                            No hay información laboral, agregar
                                            <a href="#" class="btn btn-link btn-sm btn-add" title="Agregar" data-toggle="modal" data-target="#perfilProfesion" data-route="{{ url('api/perfil-profesion') }}"><i class="la la-plus-square la-lg"></i> nueva</a>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row academico">
                        <div class="col">
                            <a href="#" class="float-right btn btn-sm btn-add btn-link" title="Agregar" data-toggle="modal" data-target="#perfilAcademico" data-route="{{ url('api/perfil-academico') }}"><span>Agregar</span> <i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Formación Académica</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless perfilTable" id="tabla-academicos">
                                    <tbody>
                                    @if ($academicos->count() > 0)
                                        @foreach($academicos as $academico)
                                        <tr>
                                            <td>{{ ($academico->institucion_id) ? $academico->institucion->nombre : $academico->otro }}</td>
                                            <td>{{ ($academico->nivel_academico_id) ? $academico->nivel_academico->nombre : '-' }}</td>
                                            <td>{{ ($academico->grado_academico_id) ? $academico->grado_academico->nombre : '-' }}</td>
                                            <td>{{ ($academico->anio) ? $academico->anio : '-' }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url('api/perfil-academico/' . $academico->id) }}" class="btn btn-sm btn-link" data-button-type="delete" data-id="{{ $academico->id }}" title="Borrar"><i class="la la-trash la-lg"></i></a>
                                                <a href="javascript:void(0)" data-route="{{ url('api/perfil-academico/' . $academico->id) }}" class="btn btn-sm btn-link btn-edit" data-button-type="edit" data-id="{{ $academico->id }}" title="Editar" data-toggle="modal" data-target="#perfilAcademico"><i class="la la-edit la-lg"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="sin-datos">
                                            <td colspan="3">
                                            No hay formación académica, agregar
                                            <a href="#" class="btn btn-link btn-sm btn-add" title="Agregar" data-toggle="modal" data-target="#perfilAcademico" data-route="{{ url('api/perfil-academico') }}"><i class="la la-plus-square la-lg"></i> nueva</a>
                                            </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade px-4" id="tab_relaciones">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="media mt-4">
                                <img src="{{ asset('storage/personas/fotos/foto.png') }}" width="64" height="64" alt="" class="mr-3 rounded-circle" >
                                <div class="media-body">
                                    <h6 class="mt-2 card-subtitle">Padre</h6>
                                    <p class="card-text">José Luis Gonzalez</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media mt-4">
                                <img src="{{ asset('storage/personas/fotos/foto.png') }}" width="64" height="64" alt="" class="mr-3 rounded-circle" >
                                <div class="media-body">
                                    <h6 class="mt-2 card-subtitle">Madre</h6>
                                    <p class="card-text">José Luis Gonzalez</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media mt-4">
                                <img src="{{ asset('storage/personas/fotos/foto.png') }}" width="64" height="64" alt="" class="mr-3 rounded-circle" >
                                <div class="media-body">
                                    <h6 class="mt-2 card-subtitle">Hermano</h6>
                                    <p class="card-text">José Luis Gonzalez</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media mt-4">
                                <img src="{{ asset('storage/personas/fotos/foto.png') }}" width="64" height="64" alt="" class="mr-3 rounded-circle" >
                                <div class="media-body">
                                    <h6 class="mt-2 card-subtitle">Concubina</h6>
                                    <p class="card-text">José Luis Gonzalez</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade p-4" id="tab_penales">
                    <div class="row">
                        <div class="col">
                            <a href="#" class="float-right" data-toggle="tooltip" data-placement="bottom" title="Agregar"><i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Causas</h6>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>CARATULA</th>
                                            <th>ULTIMA ACTUACION</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1234/2019</td>
                                            <td>Miguel Ojeda sobre robo agravado</td>
                                            <td>12/03/2020</td>
                                            <td>
                                                <a href="#" class="btn btn-sm imprimir-causa" data-toggle="tooltip" data-placement="left" title="Imprimir causa"><i class="la la-print la-lg"></i></a>
                                                <a href="#" data-exp="1234" class="btn btn-sm abrir-expediente" data-toggle="tooltip" data-placement="left" title="Abrir expediente"><i class="la la-folder-plus la-lg"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>1041/2019</td>
                                            <td>Magallanes y otros sobre robo agravado</td>
                                            <td>30/11/2019</td>
                                            <td>
                                                <a href="#" class="btn btn-sm imprimir-causa" data-toggle="tooltip" data-placement="left" title="Imprimir causa"><i class="la la-print la-lg"></i></a>
                                                <a href="#" data-exp="1041" class="btn btn-sm abrir-expediente" data-toggle="tooltip" data-placement="left" title="Abrir expediente"><i class="la la-folder-plus la-lg"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>948/2019</td>
                                            <td>Miguel Ojeda sobre estafa</td>
                                            <td>10/02/2019</td>
                                            <td>
                                                <a href="#" class="btn btn-sm imprimir-causa" data-toggle="tooltip" data-placement="left" title="Imprimir causa"><i class="la la-print la-lg"></i></a>
                                                <a href="#" data-exp="948" class="btn btn-sm abrir-expediente" data-toggle="tooltip" data-placement="left" title="Abrir expediente"><i class="la la-folder-plus la-lg"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col d-none animated fadeIn" id="expediente">
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade p-4" id="tab_huellas">
                    <h6 class="card-subtitle">Mano Derecha</h6>
                    <div class="row">
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Pulgar</small></p>
                            <div id="huella-1">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(1, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="1"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                            
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Índice</small></p>
                            <div id="huella-2">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(2, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="2"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Medio</small></p>
                            <div id="huella-3">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(3, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="3"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Anular</small></p>
                            <div id="huella-4">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(4, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="4"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Meñique</small></p>
                            <div id="huella-5">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(5, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="5"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Palma</small></p>
                            <div id="huella-6">
                                <img src="{{ asset('assets/img/huella-palma') }}@if(in_array(6, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="6"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <h6 class="card-subtitle mt-5">Mano Izquierda</h6>
                    <div class="row">
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Pulgar</small></p>
                            <div id="huella-7">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(7, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="7"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                            
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Índice</small></p>
                            <div id="huella-8">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(8, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="8"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Medio</small></p>
                            <div id="huella-9">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(9, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="9"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Anular</small></p>
                            <div id="huella-10">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(10, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="10"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Meñique</small></p>
                            <div id="huella-11">
                                <img src="{{ asset('assets/img/huella-dedo') }}@if(in_array(11, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="11"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <p class="card-subtitle text-muted"><small>Palma</small></p>
                            <div id="huella-12">
                                <img src="{{ asset('assets/img/huella-palma') }}@if(in_array(12, $huellas)){{'-ok'}}@endif{{ '.png' }}" alt="Huella" class="img-thumbnail img-fluid" >
                            </div>
                            <div class="huella-dactilar">
                                <a href="#" class="btn btn-link subirHuellaDactilar" data-tipo="12"><i class="la la-fingerprint la-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <input id="upload_huella" type="file" accept="image/*" name="huella" placeholder="Huella" required="" capture class="d-none">

                </div>
                <div role="tabpanel" class="tab-pane fade p-4" id="tab_documentos">
                    <div class="row">
                        <div class="col">4</div>
                    </div>
                </div>
            </div>
        </div>


    </div>
  </div>
  
@endsection


@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/select2/dist/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
    <style>
        #perfil-persona .foto-perfil, .huella-dactilar
        {
            position: absolute;           
            margin: auto;
            margin-top: -30px;
            text-align: center;
            right: 5%;
        }

        #perfil-persona .foto-perfil .btn, .huella-dactilar .btn {
            border-radius: 160px;
            background-color: #e4e6eb;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }
        .huella-dactilar .btn {
            padding-top: 0.6rem;
            padding-bottom: 0.6rem;
        }
        #perfil-persona .foto-perfil .btn:hover,  .huella-dactilar .btn:hover 
        {
            background-color: #e4e6eb;
        }

        #perfil-persona .card-subtitle
        {
            font-weight: 300 !important;
            font-size: 0.8rem;
            margin-bottom: 0rem;
            margin-top: 0.50rem;
            text-transform: uppercase;
        }

        #perfil-persona .card-text
        {
            font-weight: 800;
        }
        #perfil-persona .nav-tabs {
            padding-top: 10px;
            background-color: #F5F5F5;
            padding-right: 5px;
            padding-left: 10px;
        }
        #perfil-persona .table {
            margin-bottom: 0rem;
        }
        #perfil-persona .table-borderless td {
            border: none;
            font-weight: 800;
            white-space: nowrap !important;
        }
        #perfil-persona .table thead th {
            font-weight: 300;
            text-transform: uppercase;
        }
        #perfil-persona .titulo-causa strong {
            font-size: 130%;
        }
        #perfil-persona .titulo-causa span {
            font-size: 130%;
        }
        #perfil-persona .relacion {
            color:#B7B7B7;
            margin-left: 10px;
            margin-right: 10px;
        }
        #perfil-persona .relacion small {
            text-decoration: underline;
        }

        .btn-add span {
            display: none;
            /* font-size: 0.8rem; */
            font-weight: 300;
            text-transform: uppercase;
            /* text-decoration: none; */
            transition: all 5s ease-out;
        }

        .btn-add:hover span {
            display:inline-block;
            transition: all 5s ease-out;
            color:#B7B7B7;
        }

        .table td, .table th {
            vertical-align: middle !important;
        }

        .direcciones a, .documentos a,
        .telefonos a,
        .laboral a, .academico a {
            visibility: hidden;
        }
        .direcciones:hover a, .documentos:hover a,
        .telefonos:hover a,
        .laboral:hover a, .academico:hover a {
            visibility:visible;
        }
        .direcciones:hover, .documentos:hover   ,
        .telefonos:hover,
        .laboral:hover, .academico:hover     {
            background-color: rgba(255, 255, 25, .25);
            color: #fddfff;
        }
        
    </style>
@endsection

@section('after_scripts')

	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('packages/select2/dist/js/i18n/es.js') }}"></script>
    <script src="{{ asset('packages/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script charset="UTF-8" src="{{ asset('packages/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    <script charset="UTF-8" src="{{ asset('packages/moment/min/moment.min.js') }}"></script>
    
    @include('vendor.backpack.crud.modal.privado.foto')
    @include('vendor.backpack.crud.modal.privado.perfil')
    @include('vendor.backpack.crud.modal.privado.documento')
    @include('vendor.backpack.crud.modal.privado.direccion')
    @include('vendor.backpack.crud.modal.privado.telefono')
    @include('vendor.backpack.crud.modal.privado.profesion')
    @include('vendor.backpack.crud.modal.privado.academico')
    @include('vendor.backpack.crud.modal.privado.criminal')
    @include('vendor.backpack.crud.modal.privado.huellas')
    
    <script>
        function bpFieldInitSelect2Element(element) {
            // element will be a jQuery wrapped DOM node
            if (!element.hasClass("select2-hidden-accessible")) {
                element.select2({
                    theme: "bootstrap"
                });
            }
        }

        if (jQuery.ui) {
            var datepicker = $.fn.datepicker.noConflict();
            $.fn.bootstrapDP = datepicker;
        } else {
            $.fn.bootstrapDP = $.fn.datepicker;
        }

        var dateFormat=function(){var a=/d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,b=/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,c=/[^-+\dA-Z]/g,d=function(a,b){for(a=String(a),b=b||2;a.length<b;)a="0"+a;return a};return function(e,f,g){var h=dateFormat;if(1!=arguments.length||"[object String]"!=Object.prototype.toString.call(e)||/\d/.test(e)||(f=e,e=void 0),e=e?new Date(e):new Date,isNaN(e))throw SyntaxError("invalid date");f=String(h.masks[f]||f||h.masks.default),"UTC:"==f.slice(0,4)&&(f=f.slice(4),g=!0);var i=g?"getUTC":"get",j=e[i+"Date"](),k=e[i+"Day"](),l=e[i+"Month"](),m=e[i+"FullYear"](),n=e[i+"Hours"](),o=e[i+"Minutes"](),p=e[i+"Seconds"](),q=e[i+"Milliseconds"](),r=g?0:e.getTimezoneOffset(),s={d:j,dd:d(j),ddd:h.i18n.dayNames[k],dddd:h.i18n.dayNames[k+7],m:l+1,mm:d(l+1),mmm:h.i18n.monthNames[l],mmmm:h.i18n.monthNames[l+12],yy:String(m).slice(2),yyyy:m,h:n%12||12,hh:d(n%12||12),H:n,HH:d(n),M:o,MM:d(o),s:p,ss:d(p),l:d(q,3),L:d(q>99?Math.round(q/10):q),t:n<12?"a":"p",tt:n<12?"am":"pm",T:n<12?"A":"P",TT:n<12?"AM":"PM",Z:g?"UTC":(String(e).match(b)||[""]).pop().replace(c,""),o:(r>0?"-":"+")+d(100*Math.floor(Math.abs(r)/60)+Math.abs(r)%60,4),S:["th","st","nd","rd"][j%10>3?0:(j%100-j%10!=10)*j%10]};return f.replace(a,function(a){return a in s?s[a]:a.slice(1,a.length-1)})}}();dateFormat.masks={default:"ddd mmm dd yyyy HH:MM:ss",shortDate:"m/d/yy",mediumDate:"mmm d, yyyy",longDate:"mmmm d, yyyy",fullDate:"dddd, mmmm d, yyyy",shortTime:"h:MM TT",mediumTime:"h:MM:ss TT",longTime:"h:MM:ss TT Z",isoDate:"yyyy-mm-dd",isoTime:"HH:MM:ss",isoDateTime:"yyyy-mm-dd'T'HH:MM:ss",isoUtcDateTime:"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"},dateFormat.i18n={dayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],monthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","January","February","March","April","May","June","July","August","September","October","November","December"]},Date.prototype.format=function(a,b){return dateFormat(this,a,b)};

        function bpFieldInitDatePickerElement(element) {
            var $fake = element,
            $field = $fake.closest('.form-group').find('input[type="hidden"]'),
            $customConfig = $.extend({
                format: 'dd/mm/yyyy'
            }, $fake.data('bs-datepicker'));
            $picker = $fake.bootstrapDP($customConfig);

            var $existingVal = $field.val();

                if( $existingVal.length ){
                    // Passing an ISO-8601 date string (YYYY-MM-DD) to the Date constructor results in
                    // varying behavior across browsers. Splitting and passing in parts of the date
                    // manually gives us more defined behavior.
                    // See https://stackoverflow.com/questions/2587345/why-does-date-parse-give-incorrect-results
                    var parts = $existingVal.split('-');
                    var year = parts[0];
                    var month = parts[1] - 1; // Date constructor expects a zero-indexed month
                    var day = parts[2];
                    preparedDate = new Date(year, month, day).format($customConfig.format);
                    $fake.val(preparedDate);
                    $picker.bootstrapDP('update', preparedDate);
                }

                // prevent users from typing their own date
                // since the js plugin does not support it
                // $fake.on('keydown', function(e){
                //     e.preventDefault();
                //     return false;
                // });

            $picker.on('show hide change', function(e){
                if( e.date ){
                    var sqlDate = e.format('yyyy-mm-dd');
                } else {
                    try {
                        var sqlDate = $fake.val();

                        if( $customConfig.format === 'dd/mm/yyyy' ){
                            sqlDate = new Date(sqlDate.split('/')[2], sqlDate.split('/')[1] - 1, sqlDate.split('/')[0]).format('yyyy-mm-dd');
                        }
                    } catch(e){
                        if( $fake.val() ){
                                new Noty({
                                    type: "error",
                                    text: "<strong>Whoops!</strong><br>Sorry we did not recognise that date format, please make sure it uses a yyyy mm dd combination"
                                  }).show();
                        }
                    }
                }

                $field.val(sqlDate);
            });
        }

        function bpFieldInitSelect2FromAjaxElement(element) {
            var form = element.closest('form');
            var $placeholder = element.attr('data-placeholder');
            var $minimumInputLength = element.attr('data-minimum-input-length');
            var $dataSource = element.attr('data-data-source');
            var $method = element.attr('data-method');
            var $fieldAttribute = element.attr('data-field-attribute');
            var $connectedEntityKeyName = element.attr('data-connected-entity-key-name');
            var $includeAllFormFields = element.attr('data-include-all-form-fields')=='false' ? false : true;
            var $allowClear = element.attr('data-column-nullable') == 'true' ? true : false;
            var $dependencies = JSON.parse(element.attr('data-dependencies'));
            var $ajaxDelay = element.attr('data-ajax-delay');
            var $selectedOptions = JSON.parse(element.attr('data-selected-options') ?? null);

            var select2AjaxFetchSelectedEntry = function (element) {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        url: $dataSource,
                        data: {
                            'keys': $selectedOptions
                        },
                        type: $method,
                        success: function (result) {

                            resolve(result);
                        },
                        error: function (result) {
                            reject(result);
                        }
                    });
                });
            };

            // do not initialise select2s that have already been initialised
            if ($(element).hasClass("select2-hidden-accessible"))
            {
                return;
            }
            //init the element
            $(element).select2({
                theme: 'bootstrap',
                multiple: false,
                placeholder: $placeholder,
                minimumInputLength: $minimumInputLength,
                allowClear: $allowClear,
                ajax: {
                    url: $dataSource,
                    type: $method,
                    dataType: 'json',
                    delay: $ajaxDelay,
                    data: function (params) {
                        if ($includeAllFormFields) {
                            return {
                                q: params.term, // search term
                                page: params.page, // pagination
                                form: form.serializeArray() // all other form inputs
                            };
                        } else {
                            return {
                                q: params.term, // search term
                                page: params.page, // pagination
                            };
                        }
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        var result = {
                            results: $.map(data.data, function (item) {
                                textField = $fieldAttribute;
                                return {
                                    text: item[textField],
                                    id: item[$connectedEntityKeyName]
                                }
                            }),
                            pagination: {
                                    more: data.current_page < data.last_page
                            }
                        };

                        return result;
                    },
                    cache: true
                },
            });

            // if we have selected options here we are on a repeatable field, we need to fetch the options with the keys
            // we have stored from the field and append those options in the select.
            if (typeof $selectedOptions !== typeof undefined && 
                $selectedOptions !== false &&  
                $selectedOptions != '' && 
                $selectedOptions != null && 
                $selectedOptions != []) 
            {
                var optionsForSelect = [];
                select2AjaxFetchSelectedEntry(element).then(result => {
                    result.forEach(function(item) {
                        $itemText = item[$fieldAttribute];
                        $itemValue = item[$connectedEntityKeyName];
                        //add current key to be selected later.
                        optionsForSelect.push($itemValue);

                        //create the option in the select
                        $(element).append('<option value="'+$itemValue+'">'+$itemText+'</option>');
                    });

                    // set the option keys as selected.
                    $(element).val(optionsForSelect);
                    $(element).trigger('change');
                });
            }

            // if any dependencies have been declared
            // when one of those dependencies changes value
            // reset the select2 value
            for (var i=0; i < $dependencies.length; i++) {
                $dependency = $dependencies[i];
                $('input[name='+$dependency+'], select[name='+$dependency+'], checkbox[name='+$dependency+'], radio[name='+$dependency+'], textarea[name='+$dependency+']').change(function () {
                    element.val(null).trigger("change");
                });
            }

        }
    </script>
    <script>
        $(document).ready(function(){

            initializeFieldsWithJavascript('form');

            $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});


            $('[data-toggle="tooltip"]').tooltip()

            $('[data-toggle="popover"]').popover({
                trigger: 'focus'
            });

            const vistaRelacionados = `
                            <hr>
                            <h6 class="card-subtitle">Relacionados a la causa</h6>
                            <div class="row">
                            <listaRelacionados>
                            </div>`;

            const listaRelacionados = ({ perfil_privado_id, foto, nombres, apellidos}) => `
                                <div class="col-sm-6">
                                    <div class="media mt-4">
                                        <img src="${foto}" width="64" height="64" alt="${nombres} ${apellidos}" class="mr-3 rounded-circle" >
                                        <div class="media-body">
                                            <p class="card-text">
                                                ${nombres} ${apellidos}<br>
                                                <small><a href="${perfil_privado_id}/show" class="btn btn-link stretched-link">ver perfil</a></small>
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
            `;

            const listaExpedientes = ({ id, fecha, titulo, tipo}) => `
                                        <tr>
                                            <td>${fecha}</td>
                                            <td>${titulo}</td>
                                            <td>${tipo}</td>
                                            <td>
                                                <a href="#" data-act="${id}" class="btn btn-sm ver-actuacion" data-toggle="tooltip" data-placement="left" title="Ver actuación"><i class="la la-eye la-lg"></i></a>
                                                <a href="#" data-act="${id}" class="btn btn-sm editar-actuacion" data-toggle="tooltip" data-placement="left" title="Actualizar actuación"><i class="la la-edit la-lg"></i></a>
                                                <a href="#" data-act="${id}" class="btn btn-sm descargar-adjunto" data-toggle="tooltip" data-placement="left" title="Descargar adjunto"><i class="las la-paperclip la-lg"></i></a>
                                            </td>
                                        </tr>
            `;

            const causaRelacionada = ({ causa, anio }) => `
                                <em class="relacion">
                                    <i class="la la-folder la-lg"></i> 
                                    <small>relacionada a</small> 
                                    <i class="la la-folder la-lg"></i>
                                </em> 
                                <span>CAUSA ${causa}/${anio}</span>`;

            const Expediente = ({ numero, anio }) => `
                            <a href="#" data-causa="${numero}" class="float-right" data-toggle="tooltip" data-placement="bottom" title="Agregar actuación"><i class="la la-plus-square la-lg"></i></a>
                            <h6 class="card-subtitle">Expediente</h6>
                            <p class="titulo-causa">
                                <strong>CAUSA ${numero}/${anio}</strong> 
                                <causaRelacionada>
                            </p>
                            <div class="table-responsive">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                    <listadoExpedientes>
                                    </tbody>
                                </table>
                            </div>
                            <listadoRelacionados>
            `;

            $('#perfil-persona').on('click', '.abrir-expediente', function(){
                $('#expediente').removeClass('d-none');
                $('.cerrar-expediente').children('.la').removeClass('la-folder-minus').addClass('la-folder-plus');
                $('.cerrar-expediente').addClass('abrir-expediente').removeClass('cerrar-expediente');
                $(this).children('.la').removeClass('la-folder-plus').addClass('la-folder-minus');
                $(this).addClass('cerrar-expediente').removeClass('abrir-expediente');

                var $form = $(this).closest('form');
                var $data = $(this).attr('data-exp');
                var $dataSource = '/api/abrir-expediente/' + $data;

                $.ajax({
                    url: $dataSource,
                    type: 'GET',
                    success: function (result) {

                        if (result.expedientes.length)
                            var listaExp = result.expedientes.map(listaExpedientes).join('');

                        var causaRel = '';
                        if (result.causa_relacionada.length)
                            causaRel = result.causa_relacionada.map(causaRelacionada).join('');


                        var relacionado = '';
                        if (result.relacionados.length){
                            relacionado = result.relacionados.map(listaRelacionados).join('');
                            relacionado = vistaRelacionados.replace('<listaRelacionados>', relacionado);
                        }

                        var exp = result.causa.map(Expediente).join('');
                        exp = exp.replace('<listadoExpedientes>', listaExp).replace('<causaRelacionada>', causaRel).replace('<listadoRelacionados>', relacionado);
                        
                        $('#expediente').html(exp);

                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            });

            $('#perfil-persona').on('click', '.cerrar-expediente', function(){
                $('#expediente').addClass('d-none');
                $(this).children('.la').removeClass('la-folder-minus').addClass('la-folder-plus');
                $(this).addClass('abrir-expediente').removeClass('cerrar-expediente');
            });
        });

        $('#perfil-persona').on('click', '.btn-add', function(){
            var modal = $(this).data('target');
            var action = $(this).data('route');
            $(modal + ' .modal-title span').text('Agregar');
            $(modal + ' form').trigger("reset").attr({action: action, method: 'POST'});
            $(modal + ' select').trigger('change');
        });

        $('#perfil-persona').on('click', '.btn-edit', function(){
            var modal = $(this).data('target');
            var action = $(this).data('route');
            var id = $(this).data('id');
            $(modal + ' .modal-title span').text('Editar');
            $(modal + ' form').trigger("reset").attr({action: action, method: 'PUT'});
            $.ajax({
                url:  action,
                type: 'GET',
                success: function (result) {
                    var results = result[0];
                    $.each(results, function(index, value){
                        if ($(modal + ' #'+index).is("select") && value && !$(modal + ' #'+index).val())
                        {
                            var text = "";
                            if (index == 'departamento_id') text = results.departamento.nombre;
                            if (index == 'ciudad_id') text = results.laciudad.nombre;
                            if (index == 'grado_academico_id') text = results.grado_academico.nombre;
                            var option = '<option value="' + value + '">' + text + '</option>';
                            $(modal + ' #'+index).html(option);
                         } else {
                            $(modal + ' #'+index).val(value).trigger('change');
                        }
                    });
                    bpFieldInitDatePickerElement($(modal + ' .date input'));
                }
            });

        });

        function initializeFieldsWithJavascript(container) {
            var selector;
            if (container instanceof jQuery) {
                selector = container;
            } else {
                selector = $(container);
            }
            selector.find("[data-init-function]").not("[data-initialized=true]").each(function () {
                var element = $(this);
                var functionName = element.data('init-function');

                if (typeof window[functionName] === "function") {
                window[functionName](element);

                // mark the element as initialized, so that its function is never called again
                element.attr('data-initialized', 'true');
                }
            });
        }

</script>
<script>

if (typeof deleteEntry != 'function') {
  $("[data-button-type=delete]").unbind('click');

  function deleteEntry(button) {
    // ask for confirmation before deleting an item
    // e.preventDefault();
    var button = $(button);
    var route = button.attr('data-route');
    var id = button.attr('data-id');
    var row = $(".perfilTable a[data-route='"+route+"']").closest('tr');

    swal({
      title: "{!! trans('backpack::base.warning') !!}",
      text: "{!! trans('backpack::crud.delete_confirm') !!}",
      icon: "warning",
      buttons: {
          cancel: {
          text: "{!! trans('backpack::crud.cancel') !!}",
          value: null,
          visible: true,
          className: "bg-secondary",
          closeModal: true,
        },
          delete: {
          text: "{!! trans('backpack::crud.delete') !!}",
          value: true,
          visible: true,
          className: "bg-danger",
        }
      },
    }).then((value) => {
        if (value) {
            $.ajax({
              url: route,
              type: 'DELETE',
              data: {id: id},
              success: function(result) {
                  if (result == 1) {
                        // Show a success notification bubble
                      new Noty({
                        type: "success",
                        text: "{!! '<strong>'.trans('backpack::crud.delete_confirmation_title').'</strong><br>'.trans('backpack::crud.delete_confirmation_message') !!}"
                      }).show();

                      // Hide the modal, if any
                      $('.modal').modal('hide');

                    //   // Remove the details row, if it is open
                    //   if (row.hasClass("shown")) {
                    //       row.next().remove();
                    //   }

                      // Remove the row from the datatable
                      row.remove();
                  } else {
                      // if the result is an array, it means 
                      // we have notification bubbles to show
                        if (result instanceof Object) {
                            // trigger one or more bubble notifications 
                            Object.entries(result).forEach(function(entry, index) {
                              var type = entry[0];
                              entry[1].forEach(function(message, i) {
                                new Noty({
                                type: type,
                                text: message
                              }).show();
                              });
                            });
                        } else {// Show an error alert
                          swal({
                              title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                            text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
                              icon: "error",
                              timer: 4000,
                              buttons: false,
                          });
                        }			          	  
                  }
              },
              error: function(result) {
                  // Show an alert with the result
                  swal({
                      title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                    text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
                      icon: "error",
                      timer: 4000,
                      buttons: false,
                  });
              }
          });
        }
    });

  }
}

</script>
@endsection
