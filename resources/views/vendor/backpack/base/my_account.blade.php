@extends(backpack_view('blank'))

@section('after_styles')
    <link href="{{ asset('packages/croppie/croppie.css') }}" rel="stylesheet" type="text/css" />
    <style media="screen">
        .backpack-profile-form .required::after {
            content: ' *';
            color: red;
        }
    </style>
@endsection

@php
  $breadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      trans('backpack::base.my_account') => false,
  ];
@endphp

@section('header')
    <section class="content-header">
        <div class="container-fluid mb-3">
            <h1>{{ trans('backpack::base.my_account') }}</h1>
        </div>
    </section>
@endsection

@section('content')
    <div class="row">

        @if (session('success'))
        <div class="col-lg-8">
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        </div>
        @endif

        @if ($errors->count())
        <div class="col-lg-8">
            <div class="alert alert-danger">
                <ul class="mb-1">
                    @foreach ($errors->all() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif




    </div>



    <div class="row">
          		<div class="col-sm-4 col-md-3">
                    <ul class="list-group mb-4">
                        <li class="list-group-item text-center">
                            @php
                                $field = 'avatar';
                            @endphp
                            <div id="profile-container">
                            @if (isset($user->$field) && $user->$field)
                                <img src="{{ asset('storage/users/' . $user->$field) }}" alt="Avatar" class="img-fluid rounded mx-auto d-block" id="profileImage">
                            @else
                                <img src="{{ asset('storage/users/avatar.png') }}" alt="Avatar" class="img-fluid rounded mx-auto d-block" id="profileImage">
                            @endif
                            </div>
                            <input id="upload_image" type="file" accept="image/*" name="image" placeholder="Avatar" required="" capture class="d-none">
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Mensajes
                            <span class="badge badge-primary badge-pill">10930</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Tareas
                            <span class="badge badge-primary badge-pill">2</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            Notificaciones
                            <span class="badge badge-primary badge-pill">0</span>
                        </li>
                    </ul> 
                </div><!--/col-3-->
            	<div class="col-sm-8 col-md-9">
                    <div class="tab-container mb-2">
                        <div class="nav-tabs-custom " id="form_tabs">
                            <ul class="nav nav-tabs " role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a href="#tab_profile" 
                                        aria-controls="tab_profile" 
                                        role="tab" 
                                        tab_name="profile" 
                                        data-toggle="tab" 
                                        class="nav-link active">{{ trans('backpack::base.update_account_info') }}</a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#tab_password" 
                                        aria-controls="tab_password" 
                                        role="tab" 
                                        tab_name="password" 
                                        data-toggle="tab" 
                                        class="nav-link ">{{ trans('backpack::base.change_password') }}</a>
                                </li>
                            </ul>
                            <div class="tab-content p-0 ">
                                {{-- UPDATE INFO FORM --}}
                                <div role="tabpanel" class="tab-pane active backpack-profile-form bold-labels" id="tab_profile">
                                    <form class="form" action="{{ route('backpack.account.info.store') }}" method="post">
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            <div class="col-lg-6 form-group">
                                                @php
                                                    $label = trans('backpack::base.name');
                                                    $field = 'name';
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input required class="form-control" type="text" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                @php
                                                    $label = trans('backpack::base.surname');
                                                    $field = 'surname';
                                                @endphp
                                                <label>{{ $label }}</label>
                                                <input class="form-control" type="text" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                @php
                                                    $label = trans('backpack::base.email_address');
                                                    $field = 'email';
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input required class="form-control" type="text" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                @php
                                                    $label = config('backpack.base.authentication_column_name');
                                                    $field = backpack_authentication_column();
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input required class="form-control" type="{{ backpack_authentication_column()=='email'?'email':'text' }}" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                                            </div>
                                            <div class="card-footer col-12 pb-0">
                                                <button type="submit" class="btn btn-success"><i class="la la-save"></i> {{ trans('backpack::base.save') }}</button>
                                                <a href="{{ backpack_url() }}" class="btn">{{ trans('backpack::base.cancel') }}</a>
                                                <input type="hidden" name="avatar" value="{{ old('avatar') ? old('avatar') : $user->avatar }}" id="avatar">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{-- CHANGE PASSWORD FORM --}}
                                <div role="tabpanel" class="tab-pane backpack-profile-form bold-labels" id="tab_password">
                                    <form class="form" action="{{ route('backpack.account.password') }}" method="post">
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            <div class="col-lg-4 form-group">
                                                @php
                                                    $label = trans('backpack::base.old_password');
                                                    $field = 'old_password';
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                            </div>

                                            <div class="col-lg-4 form-group">
                                                @php
                                                    $label = trans('backpack::base.new_password');
                                                    $field = 'new_password';
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                            </div>

                                            <div class="col-lg-4 form-group">
                                                @php
                                                    $label = trans('backpack::base.confirm_password');
                                                    $field = 'confirm_password';
                                                @endphp
                                                <label class="required">{{ $label }}</label>
                                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                            </div>
                                            <div class="card-footer col-12 pb-0">
                                                <button type="submit" class="btn btn-success"><i class="la la-save"></i> {{ trans('backpack::base.change_password') }}</button>
                                                <a href="{{ backpack_url() }}" class="btn">{{ trans('backpack::base.cancel') }}</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/col-9-->
            </div>


@endsection


@section('after_scripts')
    <div id="uploadimageModal" tabindex="-1" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload & Crop Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div id="image_crop" style="width:450px; margin-top:30px"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary crop_image">Crop & Upload Image</button>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('packages/croppie/croppie.js') }}"></script>
<script>  
    $(document).ready(function(){

        $image_crop = $('#image_crop').croppie({
                enableExif: true,
                viewport: {
                width:240,
                height:240,
                type:'square' //circle
            },
            boundary:{
                width:350,
                height:350
            }
        });
        $("#profileImage").click(function(e) {
            $("#upload_image").click();
        });
        $('#upload_image').on('change', function(){
            var reader = new FileReader();
            reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event){
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            })
            .then(function(response){
                $.ajax({
                    url: "upload-crop",
                    type: "POST",
                    data: {image: response, destination: 'users' },
                    success: function(data)
                    {
                        if (data.status) {
                            $('#uploadimageModal').modal('hide');
                            $('#avatar').val(data.image);
                            $('#profile-container').html('<img src="{{ asset('storage/users') }}/' + data.image + '"  alt="Avatar" class="img-fluid rounded mx-auto d-block" id="profileImage" />');
                        } else {
                            new Noty({
                                type: "error",
                                text: data.message
                            }).show();
                        }
                    }
                });
            })
        });

    });  
</script>

@endsection