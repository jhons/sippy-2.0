<!-- This file is used to store topbar (right) items -->
@php 
$user = backpack_auth()->user(); 
$count = $user->unreadNotifications->count();
@endphp


    



<li class="nav-item dropdown d-md-down-none mr-3">
    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-bell"></i><span class="badge badge-pill badge-danger">{{ $count }}</span></a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
        <div class="dropdown-header bg-light">
            <strong>{{ trans_choice('notifications.notification', $count) }}</strong>
        </div>
        @foreach ($user->unreadNotifications as $notification)
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> {{ $notification->data['title'] }}
        </a>
        @endforeach
        <a href="#ver-notificaciones" class="dropdown-item">
            <i class="la la-eye"></i> ver todas
        </a>
    </div>
</li>

{{--
<li class="nav-item dropdown d-md-down-none mr-3">
    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-bell"></i><span class="badge badge-pill badge-danger">5</span></a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
        <div class="dropdown-header bg-light">
            <strong>Tienes 5 notificaciones</strong>
        </div>
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> Nuevo usuario registrado
        </a>
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> Usuario eliminado
        </a>
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> Informe de ventas listo.
        </a>
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> Nuevo cliente
        </a>
        <a class="dropdown-item" href="#">
            <i class="la la-bell"></i> Servidor sobrecargado
        </a>
        <div class="dropdown-header bg-light"><strong>Servidor</strong></div>
        <a class="dropdown-item d-block" href="#">
            <div class="text-uppercase mb-1"><small><b>Uso de CPU</b></small></div>
            <span class="progress progress-xs">
                <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
            <small class="text-muted">348 Procesos. 1/4 Núcleos.</small>
        </a>
        <a class="dropdown-item d-block" href="#">
            <div class="text-uppercase mb-1"><small><b>Uso de memoria</b></small></div>
            <span class="progress progress-xs">
                <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
            <small class="text-muted">11444GB/16384MB</small>
        </a>
        <a class="dropdown-item d-block" href="#">
            <div class="text-uppercase mb-1"><small><b>Uso de 1 SSD</b></small></div>
            <span class="progress progress-xs">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </span>
            <small class="text-muted">243GB/256GB</small>
        </a>
    </div>
</li>

<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-list"></i></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-map"></i></a></li>
--}}
