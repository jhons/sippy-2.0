<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('perfil-privado') }}'><i class='nav-icon la la-landmark'></i> Listado General</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('perfil-privado/create') }}'><i class='nav-icon la la-id-badge'></i> Fichar</a></li>
<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('centro-privado') }}'><i class='nav-icon la la-question'></i> Privados</a></li> -->

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('perfil-persona') }}'><i class='nav-icon la la-id-card'></i> Perfiles Personas</a></li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-user-friends"></i> Visitas</a>
	<ul class="nav-dropdown-items">
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('agenda-visita') }}'><i class='nav-icon la la-laptop'></i> Agendas Visitas</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('solicitud-registro') }}'><i class='nav-icon la la-book'></i> Solicitudes</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('area-visita') }}'><i class='nav-icon la la-person-booth'></i> Areas Visitas</a></li>
	</ul>
</li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-city"></i> Centros</a>
	<ul class="nav-dropdown-items">
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('centro') }}'><i class='nav-icon la la-building'></i> Centros</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('pabellon') }}'><i class='nav-icon la la-warehouse'></i> Pabellones</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('celda') }}'><i class='nav-icon la la-landmark'></i> Celdas</a></li>
	</ul>
</li>

<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Autenticación</a>
	<ul class="nav-dropdown-items">
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Usuarios</span></a></li>
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-group"></i> <span>Roles</span></a></li>
		<li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permisos</span></a></li>
	</ul>
</li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-list-alt"></i> Parametros</a>
	<ul class="nav-dropdown-items">
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('funcionario-cargo') }}'><i class='nav-icon la la-list-alt'></i> Cargos Funcionario </a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('causa-tipo') }}'><i class='nav-icon la la-list-alt'></i> Causas Tipos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ciudad') }}'><i class='nav-icon la la-list-alt'></i> Ciudades</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('departamento') }}'><i class='nav-icon la la-list-alt'></i> Departamentos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('documentacion-tipo') }}'><i class='nav-icon la la-list-alt'></i> Documentaciones</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('institucion') }}'><i class='nav-icon la la-list-alt'></i> Institución Ed.</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('estado-civil') }}'><i class='nav-icon la la-list-alt'></i> Estados Civiles</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('centro-tipo') }}'><i class='nav-icon la la-list-alt'></i> Centros Tipos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('estado-solicitud-registro') }}'><i class='nav-icon la la-list-alt'></i> Estados Solicitudes</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('etnia') }}'><i class='nav-icon la la-list-alt'></i> Etnias</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('foto-tipo') }}'><i class='nav-icon la la-list-alt'></i> Fotos Tipos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('genero') }}'><i class='nav-icon la la-list-alt'></i> Géneros</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('grupo-criminal') }}'><i class='nav-icon la la-list-alt'></i> Grupos Criminales</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('grupo-vulnerable') }}'><i class='nav-icon la la-list-alt'></i> Grupos Vulnerables</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('hecho-punible') }}'><i class='nav-icon la la-list-alt'></i> Hechos Punibles</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('huella-tipo') }}'><i class='nav-icon la la-list-alt'></i> Huellas</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('identidad-tipo') }}'><i class='nav-icon la la-list-alt'></i> Identidad Tipo</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('incidente-grado') }}'><i class='nav-icon la la-list-alt'></i> Incidentes Grados</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('nivel-academico') }}'><i class='nav-icon la la-list-alt'></i> Niveles Académicos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('nivel-peligrosidad') }}'><i class='nav-icon la la-list-alt'></i> Niveles Peligrosidad</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('pais') }}'><i class='nav-icon la la-list-alt'></i> Paises</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('profesion') }}'><i class='nav-icon la la-list-alt'></i> Profesiones</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('profesion-grado') }}'><i class='nav-icon la la-list-alt'></i> Profesiones Grados</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('relacion-tipo') }}'><i class='nav-icon la la-list-alt'></i> Relaciones</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('telefono-tipo') }}'><i class='nav-icon la la-list-alt'></i> Teléfonos Tipos</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('verificacion-tipo') }}'><i class='nav-icon la la-list-alt'></i> Verificaciones</a></li>
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('visita-tipo') }}'><i class='nav-icon la la-list-alt'></i> Visitas Tipos</a></li>
	</ul>
</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('circunscripcion') }}'><i class='nav-icon la la-question'></i> Circunscripcions</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('fuero') }}'><i class='nav-icon la la-question'></i> Fueros</a></li>