<li class="nav-item dropdown pr-4">
  <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <strong>{{ backpack_auth()->user()->name }}</strong> <img class="img-avatar" src="{{ backpack_avatar_url(backpack_auth()->user()) }}" alt="{{ backpack_auth()->user()->name }}">
  </a>
  <div class="dropdown-menu {{ config('backpack.base.html_direction') == 'rtl' ? 'dropdown-menu-left' : 'dropdown-menu-right' }} mr-4 pb-1 pt-1">
    <div class="dropdown-header bg-light py-2"><strong>Cuenta</strong></div>
    <a class="dropdown-item" href="#">
      <i class="la la-envelope-o"></i> Mensajes
      <span class="badge badge-success">42</span>
    </a>
    <a class="dropdown-item" href="#">
      <i class="la la-tasks"></i> Tareas
      <span class="badge badge-danger">42</span>
    </a>
    <a class="dropdown-item" href="{{ route('backpack.account.info') }}">
      <i class="la la-user"></i> {{ trans('backpack::base.my_account') }}
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{ backpack_url('logout') }}">
      <i class="la la-lock"></i> {{ trans('backpack::base.logout') }}
    </a>
  </div>
</li>
