<!-- This file is used to store topbar (left) items -->

@if (backpack_auth()->user()->centro_id)
<li class="ml-2">{{ backpack_auth()->user()->centro->nombre }}</li>
@endif