@extends(backpack_view('blank'))

@php
    // $widgets['before_content'][] = [
    //    'type'        => 'jumbotron',
    //    'heading'     => trans('backpack::base.welcome'),
    //    'content'     => trans('backpack::base.use_sidebar'),
    //    'button_link' => backpack_url('logout'),
    //    'button_text' => trans('backpack::base.logout'),
    // ];

    $userCount = App\User::count();
    $centroCount = App\Models\Centro::count();
    $privadoCount = App\Models\PerfilPrivado::count();
    $capacidadCount = App\Models\Centro::sum('capacidad');
    $capacidadTotal = ($capacidadCount) ? $capacidadCount : 1000;

    // notice we use Widget::add() to add widgets to a certain group
    Widget::add()->to('before_content')->type('div')->class('row')->content([
        Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($userCount)
			->description('Usuarios Registrados.')
			->progress(100*(int)$userCount/1000)
			->hint(1000-$userCount.' más hasta el próximo hito.'),

		/*
		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-warning')
			->progressClass('progress-bar')
			->value($privadoCount)
			->description('Personas Privadas de Libertad')
			->progress(100*(int)$privadoCount/$capacidadCount)
			->hint($capacidadCount-$privadoCount.' más hasta la capacidad máxima.'),
		*/
		
        Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-success')
			->progressClass('progress-bar')
			->value($centroCount)
			->description('Centros de Reclusión.')
			->progress(100*(int)$centroCount/20)
			->hint(20-$centroCount.' más hasta el próximo hito.'),
    ]);


@endphp

@section('content')
@endsection