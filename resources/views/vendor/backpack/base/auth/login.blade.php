@extends(backpack_view('layouts.plain'))

@section('content')
    
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>{{ trans('backpack::base.login') }}</h1>
                <p class="text-muted">Iniciar sesión en su cuenta</p>
                <form role="form" method="POST" action="{{ route('backpack.auth.login') }}">
                        {!! csrf_field() !!}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                        <input type="text" class="form-control{{ $errors->has($username) ? ' is-invalid' : '' }}" name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}"  placeholder="Usuario">

                        @if ($errors->has($username))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first($username) }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="Contraseña">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <button class="btn btn-primary px-4" type="submit">{{ trans('backpack::base.login') }}</button>
                        </div>
                        @if (backpack_users_have_email())
                        <div class="col-6 text-right">
                            <button class="btn btn-link px-0" type="button">{{ trans('backpack::base.forgot_your_password') }}</button>
                        </div>
                        @endif
                        @if (config('backpack.base.registration_open'))
                        <div class="col-12 text-center">
                            <a href="{{ route('backpack.auth.register') }}">{{ trans('backpack::base.register') }}</a>
                        </div>
                        @endif
                    </div>
                </form>
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <img src="{{ asset('assets/img/logo.png') }}" alt="" class="img-fluid">
                  <p class="mt-4">{{ config('backpack.base.project_name') }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection



