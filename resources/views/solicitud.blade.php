@extends(backpack_view('layouts.plain'))

@section('content')
    
    <div class="row justify-content-center animated fadeIn mt-4 mb-2">
        <div class="col-md-10 bold-labels">
            
            <form method="post" action="/solicitud">
            @csrf
                <div class="card">
                    <div class="card-head">
                    <img src="{{ asset('assets/img/mj-logo.jpg') }}" alt="Ministerio de Justicia" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <h2 class="mb-4 text-center">Solicitud Registro para Visita</h2>
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissable fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="row">
                            <div class="form-group col-sm-12" element="div">	
                                <h4 class="mt-2">Sus datos</h4>
                                <hr class="mb-0">
                            </div>
                            <div class="form-group col-sm-6 required" element="div">
                                <label>Centro</label>
                                <select name="solicitante_centro_id" style="width: 100%"  data-init-function="bpFieldInitSelect2Element" class="form-control select2_field @error('solicitante_centro_id') is-invalid @enderror">
                                    <option value="">-</option>
                                    @foreach ($centros as $centro)
                                    <option value="{{ $centro->id }}"
                                        {{ (old('solicitante_centro_id') == $centro->id) ? 'selected': '' }}>
                                        {{ $centro->nombre }}
                                    </option>
                                    @endforeach
                                </select>
                                @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="form-group col-sm-6" element="div">    
                                <label>Pabellón</label>
                                <select
                                name="solicitante_pabellon_id"
                                style="width: 100%"
                                data-init-function="bpFieldInitSelect2FromAjaxElement"
                                data-column-nullable="true"
                                data-dependencies="[&quot;solicitante_centro_id&quot;]"
                                data-placeholder="Seleccione un pabellón"
                                data-minimum-input-length="0"
                                data-data-source="api/pabellon"
                                data-method="GET"
                                data-field-attribute="nombre"
                                data-connected-entity-key-name="id"
                                data-include-all-form-fields="true"
                                data-ajax-delay="500"
                                class="form-control"
                                >
                                </select>
                            </div>


                            <div class="form-group col-sm-6 required" element="div">
                                <label>Nombres</label>
                                <input type="text" name="solicitante_nombres" value="{{ old('solicitante_nombres') }}" class="form-control @error('solicitante_nombres') is-invalid @enderror">
                                @error('solicitante_nombres')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-sm-6 required" element="div">
                                <label>Apellidos</label>
                                <input type="text" name="solicitante_apellidos" value="{{ old('solicitante_apellidos') }}" class="form-control @error('solicitante_apellidos') is-invalid @enderror">
                                @error('solicitante_apellidos')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            
                            <div class="form-group col-md-6 required" element="div">
                                <label>Tipo de Documento</label>
                                <select name="solicitante_tipo_documento_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element"
                                class="form-control select2_field @error('solicitante_tipo_documento_id') is-invalid @enderror" >
                                    <option value="">-</option>
                                    @foreach($tipos_documentos as $tipo_documento)
                                    <option value="{{ $tipo_documento->id }}"
                                        @if (old('solicitante_tipo_documento_id') && old('solicitante_tipo_documento_id') == $tipo_documento->id )
                                            selected
                                        @elseif (!old('solicitante_tipo_documento_id') && $tipo_documento->id == '1') 
                                            selected 
                                        @endif    
                                    >
                                        {{ $tipo_documento->nombre }}
                                    </option>
                                    @endforeach
                                </select>
                                @error('solicitante_tipo_documento_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-sm-6 required" element="div" >
                                <label>País emisor</label>
                                <select name="solicitante_nacionalidad_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field @error('solicitante_nacionalidad_id') is-invalid @enderror">
                                    <option value="">-</option>
                                    @foreach ($nacionalidades as $nacionalidad)
                                        <option value="{{ $nacionalidad->id }}"
                                            @if (old('solicitante_nacionalidad_id') && old('solicitante_nacionalidad_id') == $nacionalidad->id )
                                                selected
                                            @elseif (!old('solicitante_nacionalidad_id') && $nacionalidad->id == '181') 
                                                selected 
                                            @endif 
                                            >
                                            {{ $nacionalidad->gentilicio }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('solicitante_nacionalidad_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="form-group col-md-12 required" element="div">
                                <label>N° de Documento</label>
                                <input type="text" name="solicitante_documento" value="{{ old('solicitante_documento') }}" class="form-control @error('solicitante_documento') is-invalid @enderror">
                                @error('solicitante_documento')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-sm-12" element="div">	
                                <h4 class="mt-2">Datos del Visitante</h4>
                                <hr class="mb-0">
                            </div>

                            <div class="form-group col-md-6 required" element="div">
                                <label>Nombres</label>
                                <input type="text" name="visitante_nombres" value="{{ old('visitante_nombres') }}" class="form-control @error('visitante_nombres') is-invalid @enderror">
                                @error('visitante_nombres')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 required" element="div">
                                <label>Apellidos</label>
                                <input type="text" name="visitante_apellidos" value="{{ old('visitante_apellidos') }}" class="form-control @error('visitante_apellidos') is-invalid @enderror">
                                @error('visitante_apellidos')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            <div class="form-group col-sm-12 required" element="div" >
                                <label>Pais</label>
                                <select name="pais_id" style="width: 100%" data-init-function="bpFieldInitSelect2Element" class="form-control select2_field @error('pais_id') is-invalid @enderror">
                                    <option value="">-</option>
                                    @foreach ($nacionalidades as $nacionalidad)
                                        <option value="{{ $nacionalidad->id }}"
                                            @if (old('pais_id') && old('pais_id') == $nacionalidad->id )
                                                selected
                                            @elseif (!old('pais_id') && $nacionalidad->id == '181') 
                                                selected 
                                            @endif 
                                            >
                                            +{{ $nacionalidad->phone_code }} - {{ $nacionalidad->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('pais_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-sm-12 required" element="div">
                                <label>Whatsapp <small>(9xxxxxxxx - sin espacios y sin el codigo de país)</small></label>
                                <div class="input-group">
                                    <input type="password" name="visitante_telefono" autocomplete="off" class="form-control @error('visitante_telefono') is-invalid @enderror" value="{{ old('visitante_telefono') }}">
                                    <div class="input-group-append"> 
                                        <button class="btn btn-primary show-content" type="button">      
                                            <i class="la la-eye-slash icon"></i> 
                                        </button>
                                    </div>
                                    @error('visitante_telefono')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>  


                            <div class="form-group col-sm-12" element="div">
                                <label>Confirmación Whatsapp</label>
                                <div class="input-group">
                                    <input type="password" name="visitante_telefono_confirmation" autocomplete="off" class="form-control" value="{{ old('visitante_telefono_confirmation') }}">
                                    <div class="input-group-append"> 
                                        <button class="btn btn-primary show-content" type="button">      
                                            <i class="la la-eye-slash icon"></i> 
                                        </button>
                                    </div>
                                </div>
                            </div>

           
                            <div class="form-group col-md-6 text-center text-md-left mt-3">
                                <button type="submit" class="btn btn-primary px-4">Solicitar</button>
                                <input type="hidden" name="enlace" value="{{ Ramsey\Uuid\Uuid::uuid4() }}">
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/select2/dist/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('packages/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
    <style>
        .app-footer {
            display: none !important;
        }
    </style>
@endsection

@section('after_scripts')
<script type="text/javascript">
    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });

    // Ajax calls should always have the CSRF token attached to them, otherwise they won't work
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>

    <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('packages/select2/dist/js/i18n/es.js') }}"></script>

<script>
    function bpFieldInitSelect2Element(element) {
        // element will be a jQuery wrapped DOM node
        if (!element.hasClass("select2-hidden-accessible")) {
            element.select2({
                theme: "bootstrap"
            });
        }
    }

    function bpFieldInitSelect2FromAjaxElement(element) {
        var form = element.closest('form');
        var $placeholder = element.attr('data-placeholder');
        var $minimumInputLength = element.attr('data-minimum-input-length');
        var $dataSource = element.attr('data-data-source');
        var $method = element.attr('data-method');
        var $fieldAttribute = element.attr('data-field-attribute');
        var $connectedEntityKeyName = element.attr('data-connected-entity-key-name');
        var $includeAllFormFields = element.attr('data-include-all-form-fields')=='false' ? false : true;
        var $allowClear = element.attr('data-column-nullable') == 'true' ? true : false;
        var $dependencies = JSON.parse(element.attr('data-dependencies'));
        var $ajaxDelay = element.attr('data-ajax-delay');
        var $selectedOptions = JSON.parse(element.attr('data-selected-options') ?? null);

        var select2AjaxFetchSelectedEntry = function (element) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: $dataSource,
                    data: {
                        'keys': $selectedOptions
                    },
                    type: $method,
                    success: function (result) {

                        resolve(result);
                    },
                    error: function (result) {
                        reject(result);
                    }
                });
            });
        };

        // do not initialise select2s that have already been initialised
        if ($(element).hasClass("select2-hidden-accessible"))
        {
            return;
        }
        //init the element
        $(element).select2({
            theme: 'bootstrap',
            multiple: false,
            placeholder: $placeholder,
            minimumInputLength: $minimumInputLength,
            allowClear: $allowClear,
            ajax: {
                url: $dataSource,
                type: $method,
                dataType: 'json',
                delay: $ajaxDelay,
                data: function (params) {
                    if ($includeAllFormFields) {
                        return {
                            q: params.term, // search term
                            page: params.page, // pagination
                            form: form.serializeArray() // all other form inputs
                        };
                    } else {
                        return {
                            q: params.term, // search term
                            page: params.page, // pagination
                        };
                    }
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = {
                        results: $.map(data.data, function (item) {
                            textField = $fieldAttribute;
                            return {
                                text: item[textField],
                                id: item[$connectedEntityKeyName]
                            }
                        }),
                        pagination: {
                                more: data.current_page < data.last_page
                        }
                    };

                    return result;
                },
                cache: true
            },
        });

        // if we have selected options here we are on a repeatable field, we need to fetch the options with the keys
        // we have stored from the field and append those options in the select.
        if (typeof $selectedOptions !== typeof undefined && 
            $selectedOptions !== false &&  
            $selectedOptions != '' && 
            $selectedOptions != null && 
            $selectedOptions != []) 
        {
            var optionsForSelect = [];
            select2AjaxFetchSelectedEntry(element).then(result => {
                result.forEach(function(item) {
                    $itemText = item[$fieldAttribute];
                    $itemValue = item[$connectedEntityKeyName];
                    //add current key to be selected later.
                    optionsForSelect.push($itemValue);

                    //create the option in the select
                    $(element).append('<option value="'+$itemValue+'">'+$itemText+'</option>');
                });

                // set the option keys as selected.
                $(element).val(optionsForSelect);
                $(element).trigger('change');
            });
        }

        // if any dependencies have been declared
        // when one of those dependencies changes value
        // reset the select2 value
        for (var i=0; i < $dependencies.length; i++) {
            $dependency = $dependencies[i];
            $('input[name='+$dependency+'], select[name='+$dependency+'], checkbox[name='+$dependency+'], radio[name='+$dependency+'], textarea[name='+$dependency+']').change(function () {
                element.val(null).trigger("change");
            });
        }

    }

    function bpFieldInitSelect2FromAjaxElement(element) {
        var form = element.closest('form');
        var $placeholder = element.attr('data-placeholder');
        var $minimumInputLength = element.attr('data-minimum-input-length');
        var $dataSource = element.attr('data-data-source');
        var $method = element.attr('data-method');
        var $fieldAttribute = element.attr('data-field-attribute');
        var $connectedEntityKeyName = element.attr('data-connected-entity-key-name');
        var $includeAllFormFields = element.attr('data-include-all-form-fields')=='false' ? false : true;
        var $allowClear = element.attr('data-column-nullable') == 'true' ? true : false;
        var $dependencies = JSON.parse(element.attr('data-dependencies'));
        var $ajaxDelay = element.attr('data-ajax-delay');
        var $selectedOptions = JSON.parse(element.attr('data-selected-options') ?? null);

        var select2AjaxFetchSelectedEntry = function (element) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: $dataSource,
                    data: {
                        'keys': $selectedOptions
                    },
                    type: $method,
                    success: function (result) {

                        resolve(result);
                    },
                    error: function (result) {
                        reject(result);
                    }
                });
            });
        };

        // do not initialise select2s that have already been initialised
        if ($(element).hasClass("select2-hidden-accessible"))
        {
            return;
        }

        //init the element
        $(element).select2({
            theme: 'bootstrap',
            multiple: false,
            placeholder: $placeholder,
            minimumInputLength: $minimumInputLength,
            allowClear: $allowClear,
            ajax: {
                url: $dataSource,
                type: $method,
                dataType: 'json',
                delay: $ajaxDelay,
                data: function (params) {
                    if ($includeAllFormFields) {
                        return {
                            q: params.term, // search term
                            page: params.page, // pagination
                            form: form.serializeArray() // all other form inputs
                        };
                    } else {
                        return {
                            q: params.term, // search term
                            page: params.page, // pagination
                        };
                    }
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var result = {
                        results: $.map(data.data, function (item) {
                            textField = $fieldAttribute;
                            return {
                                text: item[textField],
                                id: item[$connectedEntityKeyName]
                            }
                        }),
                        pagination: {
                                more: data.current_page < data.last_page
                        }
                    };

                    return result;
                },
                cache: true
            },
        });

        // if we have selected options here we are on a repeatable field, we need to fetch the options with the keys
        // we have stored from the field and append those options in the select.
        if (typeof $selectedOptions !== typeof undefined && 
            $selectedOptions !== false &&  
            $selectedOptions != '' && 
            $selectedOptions != null && 
            $selectedOptions != []) 
        {
            var optionsForSelect = [];
            select2AjaxFetchSelectedEntry(element).then(result => {
                result.forEach(function(item) {
                    $itemText = item[$fieldAttribute];
                    $itemValue = item[$connectedEntityKeyName];
                    //add current key to be selected later.
                    optionsForSelect.push($itemValue);

                    //create the option in the select
                    $(element).append('<option value="'+$itemValue+'">'+$itemText+'</option>');
                });

                // set the option keys as selected.
                $(element).val(optionsForSelect);
                $(element).trigger('change');
            });
        }

        // if any dependencies have been declared
        // when one of those dependencies changes value
        // reset the select2 value
        for (var i=0; i < $dependencies.length; i++) {
            $dependency = $dependencies[i];
            $('input[name='+$dependency+'], select[name='+$dependency+'], checkbox[name='+$dependency+'], radio[name='+$dependency+'], textarea[name='+$dependency+']').change(function () {
                element.val(null).trigger("change");
            });
        }

    }

    function initializeFieldsWithJavascript(container) {
      var selector;
      if (container instanceof jQuery) {
        selector = container;
      } else {
        selector = $(container);
      }
      selector.find("[data-init-function]").not("[data-initialized=true]").each(function () {
        var element = $(this);
        var functionName = element.data('init-function');

        if (typeof window[functionName] === "function") {
          window[functionName](element);

          // mark the element as initialized, so that its function is never called again
          element.attr('data-initialized', 'true');
        }
      });
    }

    jQuery('document').ready(function($){

      // trigger the javascript for all fields that have their js defined in a separate method
      initializeFieldsWithJavascript('form');


      // Save button has multiple actions: save and exit, save and edit, save and new
      var saveActions = $('#saveActions'),
      crudForm        = saveActions.parents('form'),
      saveActionField = $('[name="save_action"]');

      saveActions.on('click', '.dropdown-menu a', function(){
          var saveAction = $(this).data('value');
          saveActionField.val( saveAction );
          crudForm.submit();
      });

      // Ctrl+S and Cmd+S trigger Save button click
      $(document).keydown(function(e) {
          if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey))
          {
              e.preventDefault();
              $("button[type=submit]").trigger('click');
              return false;
          }
          return true;
      });

      // prevent duplicate entries on double-clicking the submit form
      crudForm.submit(function (event) {
        $("button[type=submit]").prop('disabled', true);
      });

      // Place the focus on the first element in the form
              
        var focusField = $('form').find('input, textarea, select').not('[type="hidden"]').eq(0),
        
        fieldOffset = focusField.offset().top,
        scrollTolerance = $(window).height() / 2;

        focusField.trigger('focus');

        if( fieldOffset > scrollTolerance ){
            $('html, body').animate({scrollTop: (fieldOffset - 30)});
        }
      
      // Add inline errors to the DOM
      
      $("a[data-toggle='tab']").click(function(){
          currentTabName = $(this).attr('tab_name');
          $("input[name='current_tab']").val(currentTabName);
      });

      if (window.location.hash) {
          $("input[name='current_tab']").val(window.location.hash.substr(1));
      }

      });

      $('.show-content').on('click', function(){
        var input = $(this).parent().parent().find('input');
        var icono = $(this).children();

        input.attr("type", (input.attr("type") == "password") ? "text" : "password");
        (icono.hasClass("la-eye-slash")) ? icono.removeClass('la la-eye-slash').addClass('la la-eye') : icono.removeClass('la la-eye').addClass('la la-eye-slash');

      });

        

  </script>
@endsection