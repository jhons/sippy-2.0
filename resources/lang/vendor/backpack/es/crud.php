<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new'         => 'Guardar y crear nuevo',
    'save_action_save_and_edit'        => 'Guardar y continuar editando',
    'save_action_save_and_back'        => 'Guardar y regresar',
    'save_action_save_and_preview'     => 'Guardar y previsualizar',
    'save_action_changed_notification' => 'La acción por defecto del botón guardar ha sido modificada.',

    // Create form
    'add'                 => 'Añadir',
    'back_to_all'         => 'Volver al listado de',
    'cancel'              => 'Cancelar',
    'add_a_new'           => 'Añadir ',

    // Edit form
    'edit'                 => 'Editar',
    'save'                 => 'Guardar',

    // Translatable models
    'edit_translations' => 'EDITAR TRADUCCIONES',
    'language'          => 'Idioma',

    // CRUD table view
    'all'                       => 'Todos ',
    'in_the_database'           => 'en la base de datos',
    'list'                      => 'Listar',
    'reset'                     => 'Resetear',
    'actions'                   => 'Acciones',
    'preview'                   => 'Vista previa',
    'delete'                    => 'Eliminar',
    'confirm'                   => 'Confirmar',
    'moderate_block'            => 'Bloquear',
    'moderate_unblock'          => 'Habilitar',
    'admin'                     => 'Admin',
    'details_row'               => 'Esta es la fila de detalles. Modificar a su gusto.',
    'details_row_loading_error' => 'Se ha producido un error al cargar los datos. Por favor, intente de nuevo.',
    'clone' => 'Clonar',
    'clone_success' => '<strong>Entrada clonada</strong><br>Se ha agregado una nueva entrada, con la misma información que esta.',
    'clone_failure' => '<strong>La clonación falló</strong><br>La nueva entrada no se pudo crear. Inténtalo de nuevo.',

    // Confirmation messages and bubbles
    'delete_confirm'                              => '¿Está seguro que desea eliminar este elemento?',
    'delete_confirmation_title'                   => 'Elemento eliminado',
    'delete_confirmation_message'                 => 'El elemento ha sido eliminado de manera correcta.',
    'delete_confirmation_not_title'               => 'No se pudo eliminar',
    'delete_confirmation_not_message'             => 'Ha ocurrido un error. Puede que el elemento no haya sido eliminado.',
    'delete_confirmation_not_deleted_title'       => 'No se pudo eliminar',
    'delete_confirmation_not_deleted_message'     => 'No ha ocurrido nada. Su elemento está seguro.',

    // Bulk actions
    'bulk_no_entries_selected_title' => 'No hay entradas seleccionadas',
    'bulk_no_entries_selected_message' => 'Seleccione uno o más elementos para realizar una acción masiva en ellos.',

    // Bulk confirmation
    'bulk_delete_are_you_sure' => '¿Estás seguro de que deseas eliminar estas entradas de números?',
    'bulk_delete_sucess_title' => 'Entradas eliminadas',
    'bulk_delete_sucess_message' => ' elementos han sido eliminados',
    'bulk_delete_error_title' => 'Eliminación fallida',
    'bulk_delete_error_message' => 'Uno o más elementos no se pudieron eliminar',
    
    // Ajax errors
    'ajax_error_title' => 'Error',
    'ajax_error_text'  => 'Error al cargar la página. Por favor actualiza la página.',

    // Confirmation messages and bubbles
    'moderate_confirm'                              => '¿Está seguro que desea cambiar el estado de este elemento?',
    'moderate_confirmation_title'                   => 'Elemento moderado',
    'moderate_confirmation_message'                 => 'El elemento ha sido moderado de manera correcta.',
    'moderate_confirmation_not_title'               => 'No se pudo moderar',
    'moderate_confirmation_not_message'             => 'Ha ocurrido un error. Puede que el elemento no haya sido moderado.',
    'moderate_confirmation_not_moderated_title'       => 'No se pudo moderar',
    'moderate_confirmation_not_moderated_message'     => 'No ha ocurrido nada. Su elemento está seguro.',
    
    // DataTables translation
    'emptyTable'     => 'No hay datos disponibles en la tabla',
    'info'           => 'Mostrando registros _START_ a _END_ de un total de _TOTAL_ registros',
    'infoEmpty'      => 'Mostrando 0 registros',
    'infoFiltered'   => '(filtrando de _MAX_ registros totales)',
    'infoPostFix'    => '',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ elementos por página',
    'loadingRecords' => 'Cargando...',
    'processing'     => 'Procesando...',
    'search'         => 'Buscar: ',
    'zeroRecords'    => 'No se encontraron elementos',
    'paginate'       => [
        'first'    => 'Primero',
        'last'     => 'Último',
        'next'     => 'Siguiente',
        'previous' => 'Anterior',
    ],
    'aria' => [
        'sortAscending'  => ': activar para ordenar ascendentemente',
        'sortDescending' => ': activar para ordenar descendentemente',
    ],

    'export' => [
        'export'            => 'Exportar',
        'copy'              => 'Copiar',
        'excel'             => 'Excel',
        'csv'               => 'CSV',
        'pdf'               => 'PDF',
        'print'             => 'Imprimir',
        'column_visibility' => 'Visibilidad de columnas',
    ],

    // global crud - errors
    'unauthorized_access' => 'Acceso denegado - usted no tiene los permisos necesarios para ver esta página.',
    'please_fix'          => 'Por favor corrija los siguientes errores:',

    // global crud - success / error notification bubbles
    'insert_success' => 'El elemento ha sido añadido de manera correcta.',
    'update_success' => 'El elemento ha sido modificado de manera correcta.',

    // CRUD reorder view
    'reorder'                      => 'Reordenar',
    'reorder_text'                 => 'Arrastrar y soltar para reordenar.',
    'reorder_success_title'        => 'Hecho',
    'reorder_success_message'      => 'El orden ha sido guardado.',
    'reorder_error_title'          => 'Error',
    'reorder_error_message'        => 'El orden no se ha guardado.',

    // CRUD yes/no
    'yes' => 'Sí',
    'no'  => 'No',

    // CRUD filters navbar view
    'filters'        => 'Filtros',
    'toggle_filters' => 'Alternar filtros',
    'remove_filters' => 'Eliminar filtros',

    // Fields
    'browse_uploads'            => 'Subir archivos',
    'select_all'                => 'Seleccionar todo',
    'select_files'              => 'Seleccione archivos',
    'select_file'               => 'Seleccione Archivo',
    'clear'                     => 'Limpiar',
    'page_link'                 => 'Enlace de página',
    'page_link_placeholder'     => 'http://example.com/su-pagina',
    'internal_link'             => 'Enlace interno',
    'internal_link_placeholder' => 'Slug interno. Ej: \'admin/pagina\' (sin comillas) para \':url\'',
    'external_link'             => 'Enlace externo',
    'choose_file'               => 'Elija el archivo',

    //Table field
    'table_cant_add'    => 'No se puede agregar nuevo :entity',
    'table_max_reached' => 'Número máximo de :max alcanzado',

    // File manager
    'file_manager' => 'Administrar archivos',

      // InlineCreateOperation
      'related_entry_created_success' => 'Se ha creado y seleccionado una entrada relacionada.',
      'related_entry_created_error' => 'No se pudo crear una entrada relacionada.',
];
