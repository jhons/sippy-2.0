<?php
    return [
        'notification' => '{0} No tienes notificaciones|{1} Tienes 1 notificación|[2,*] Tienes :count notificaciones',
    ];