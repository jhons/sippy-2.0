<?php

return [    
    
    'whatsapp'     => [
        'solicitud_registro' => "Sr(a). :visitante. \nLa persona *:solicitante* recluida en *:centro* desea que usted se inscriba al sistema solicitud de visitas penitenciarias. \nPara registrarse puede ingresar a :urlregistro y completar todos los datos o presionar /agendar. \nUna vez que se haya registrado podrá agendar una visita a _:solicitante_. \n\nMuchas gracias, \n*Ministerio de Justicia*"
    ],

    'notify' => 'Notificar',
    'warning' => 'Atención',
    'notify_confirm' => 'Seleccione una opción una vez enviada la notificación correctamente!',
    'not_notified' => 'Cancelar',
    'notified' => 'Por favor notificar!',
    'notification_confirmation_title' => 'Notificado',
    'notification_confirmation_message' => 'Usted ha indicado que se ha notificado de manera correcta.',
    'notification_confirmation_not_title' => 'No se pudo notificar',
    'notification_confirmation_not_message' => 'Ha ocurrido un error. Puede que no se haya guardado bien.'

];
