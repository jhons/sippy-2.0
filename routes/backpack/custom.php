<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.


// Api interno

Route::group([
    'prefix'     => 'api',
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Api',
], function () { 
    Route::get('departamento', 'DepartamentoController@index');
    Route::get('departamento/{id}', 'DepartamentoController@show');
    Route::get('ciudad', 'CiudadController@index');
    Route::get('ciudad/{id}', 'CiudadController@show');
    Route::get('grado_academico', 'GradoAcademicoController@index');
    Route::get('grado_academico/{id}', 'GradoAcademicoController@show');
    Route::get('pabellon', 'CentroPabellonController@index');
    Route::get('pabellon/{id}', 'CentroPabellonController@show');
    Route::get('perfil-privado', 'PerfilPrivadoController@index');
    Route::get('perfil-privado/{id}', 'PerfilPrivadoController@show');
    Route::get('persona', 'PerfilPersonaController@index');
    Route::post('persona', 'PerfilPersonaController@update');

    Route::resource('perfil-documento', 'IdentidadController')->except(['index']);
    Route::resource('perfil-direccion', 'DireccionController')->except(['index']);
    Route::resource('perfil-telefono', 'TelefonoController')->except(['index']);
    Route::resource('perfil-profesion', 'ProfesionController')->except(['index']);
    Route::resource('perfil-academico', 'AcademicoController')->except(['index']);
    Route::resource('perfil-criminal', 'CriminalController')->except(['index']);

    Route::get('persona/{id}', 'PerfilPersonaController@show');
    Route::post('buscar-personas', 'IngresarPersonaController@index');
    Route::post('ingresar-persona', 'IngresarPersonaController@ingresar');
    Route::post('fichar', 'PerfilPrivadoController@store');
    Route::post('perfil-foto', 'PerfilFotoController@store');
    Route::post('perfil-huella', 'PerfilHuellaController@store');
    Route::get('abrir-expediente/{id}', 'ExpedienteController@show');
    Route::post('enviar-mensaje', 'ChatBotController@index');
}); 

// Rutas administrador

Route::group([
    'prefix'     => config('backpack.base.route_prefix', ''),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::post('upload-crop', 'UploadCropController@index');
    Route::any('filepicker', 'FilepickerController@handle');
    Route::any('filepicker-avatar', 'FilepickerAvatarController@handle');
    Route::crud('relacion-tipo', 'RelacionTipoCrudController');
    Route::crud('visita-tipo', 'VisitaTipoCrudController');
    Route::crud('identidad-tipo', 'IdentidadTipoCrudController');
    Route::crud('causa-tipo', 'CausaTipoCrudController');
    Route::crud('documentacion-tipo', 'DocumentacionTipoCrudController');
    Route::crud('huella-tipo', 'HuellaTipoCrudController');
    Route::crud('funcionario-cargo', 'FuncionarioCargoCrudController');
    Route::crud('hecho-punible', 'HechoPunibleCrudController');
    Route::crud('profesion-grado', 'ProfesionGradoCrudController');
    Route::crud('nivel-academico', 'NivelAcademicoCrudController');
    Route::crud('institucion', 'InstitucionCrudController');
    Route::crud('grupo-criminal', 'GrupoCriminalCrudController');
    Route::crud('nivel-peligrosidad', 'NivelPeligrosidadCrudController');
    Route::crud('etnia', 'EtniaCrudController');
    Route::crud('grupo-vulnerable', 'GrupoVulnerableCrudController');
    Route::crud('verificacion-tipo', 'VerificacionTipoCrudController');
    Route::crud('telefono-tipo', 'TelefonoTipoCrudController');
    Route::crud('foto-tipo', 'FotoTipoCrudController');
    Route::crud('centro-tipo', 'CentroTipoCrudController');
    Route::crud('centro', 'CentroCrudController');
    Route::crud('pais', 'PaisCrudController');
    Route::crud('departamento', 'DepartamentoCrudController');
    Route::crud('ciudad', 'CiudadCrudController');
    Route::crud('pabellon', 'CentroPabellonCrudController');
    Route::crud('celda', 'PabellonCeldaCrudController');
    Route::crud('perfil-persona', 'PerfilPersonaCrudController');
    Route::crud('genero', 'GeneroCrudController');
    Route::crud('estado-solicitud-registro', 'EstadoSolicitudRegistroCrudController');

    Route::post('solicitud-registro/{id}/notify', 'SolicitudRegistroCrudController@notify');
    Route::crud('solicitud-registro', 'SolicitudRegistroCrudController');
    Route::crud('area-visita', 'AreaVisitaCrudController');
    Route::crud('agenda-visita', 'AgendaVisitaCrudController');
    Route::crud('perfil-privado', 'PerfilPrivadoCrudController');
    Route::crud('centro-privado', 'CentroPrivadoCrudController');
    Route::crud('estado-civil', 'EstadoCivilCrudController');
    
    Route::get('test/{id}', function($id){

        $id = $id ?? 6;
        $persona = App\Models\PerfilPersona::find($id);

        if ($persona)
            return $persona->privado ?? 'Sin antecedentes';

        else 
            return 'No existe';
    });

    Route::get('test2/{id}', function($id){

        $id = $id ?? 2;
        $privado = App\Models\PerfilPrivado::find($id);
        
        return $privado->centro_privado_actual->pabellon->nombre;
        // if ($privado)
        //     return $privado->privado ?? 'Sin antecedentes';

        // else 
        //     return 'No existe';
    });

    Route::get('ri/{id}', function($id){

        $id = $id ?? 2;
        $relacion = App\Models\RelacionTipo::find($id);
        
        return $relacion->nombre . ' <=> ' . $relacion->getRelacionInversaNombre();
        // if ($privado)
        //     return $privado->privado ?? 'Sin antecedentes';

        // else 
        //     return 'No existe';
    });

    
    Route::crud('profesion', 'ProfesionCrudController');
    Route::crud('incidente-grado', 'IncidenteGradoCrudController');
    Route::crud('incidente', 'IncidenteCrudController');
    Route::crud('circunscripcion', 'CircunscripcionCrudController');
    Route::crud('fuero', 'FueroCrudController');
}); // this should be the absolute last line of this file