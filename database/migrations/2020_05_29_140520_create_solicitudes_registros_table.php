<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes_registros', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_privado_id')->nullable();
            $table->string('solicitante_nombres');
            $table->string('solicitante_apellidos');
            $table->unsignedBigInteger('solicitante_nacionalidad_id')->nullable();
            $table->unsignedBigInteger('solicitante_tipo_identidad_id')->nullable();
            $table->string('solicitante_documento');
            $table->unsignedBigInteger('solicitante_centro_id');
            $table->unsignedBigInteger('solicitante_pabellon_id')->nullable();
            $table->unsignedBigInteger('visitante_id')->nullable();
            $table->string('visitante_nombres');
            $table->string('visitante_apellidos');
            $table->unsignedBigInteger('pais_id')->nullable();
            $table->string('visitante_telefono');
            $table->unsignedBigInteger('estado_solicitud_registro_id')->default(1)->nullable();
            $table->uuid('enlace')->nullable();
            $table->timestamps();

            $table->foreign('perfil_privado_id')
                    ->references('id')
                    ->on('perfiles_privados')
                    ->onDelete('restrict');
            $table->foreign('solicitante_nacionalidad_id')
                    ->references('id')
                    ->on('paises')
                    ->onDelete('restrict');
            $table->foreign('solicitante_tipo_identidad_id')
                    ->references('id')
                    ->on('identidades_tipos')
                    ->onDelete('restrict');
            $table->foreign('solicitante_centro_id')
                    ->references('id')
                    ->on('centros')
                    ->onDelete('restrict');
            $table->foreign('solicitante_pabellon_id')
                    ->references('id')
                    ->on('centros_pabellones')
                    ->onDelete('restrict');
            $table->foreign('visitante_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
            $table->foreign('estado_solicitud_registro_id')
                    ->references('id')
                    ->on('estados_solicitudes_registros')
                    ->onDelete('restrict');
            $table->foreign('pais_id')
                    ->references('id')
                    ->on('paises')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_registros');
    }
}
