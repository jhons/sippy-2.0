<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_fotos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_persona_id');
            $table->text('foto');
            $table->boolean('principal')->nullable();
            $table->timestamps();

            $table->foreign('perfil_persona_id')
                ->references('id')
                ->on('perfiles_personas')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_fotos');
    }
}
