<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendasVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas_visitas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visitante_id');
            $table->unsignedBigInteger('visitado_id')->nullable();
            $table->string('visitado_nombres')->nullable();
            $table->string('visitado_apellidos')->nullable();
            $table->unsignedBigInteger('visita_tipo_id');
            $table->text('motivo')->nullable();
            $table->unsignedBigInteger('centro_id');
            $table->unsignedBigInteger('area_visita_id')->nullable();
            $table->timestamp('fecha_visita')->nullable();
            $table->timestamp('fecha_asistencia')->nullable();
            $table->timestamps();

            $table->foreign('visitante_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
            $table->foreign('visitado_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
            $table->foreign('visita_tipo_id')
                    ->references('id')
                    ->on('visitas_tipos')
                    ->onDelete('restrict');
            $table->foreign('centro_id')
                    ->references('id')
                    ->on('centros')
                    ->onDelete('restrict');
            $table->foreign('area_visita_id')
                    ->references('id')
                    ->on('areas_visitas')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas_visitas');
    }
}
