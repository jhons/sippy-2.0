<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePabellonesCeldasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pabellones_celdas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('centro_pabellon_id');
            $table->string('numero');
            $table->unsignedInteger('capacidad')->nullable();
            $table->timestamps();

            $table->foreign('centro_pabellon_id')
                    ->references('id')
                    ->on('centros_pabellones')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pabellones_celdas');
    }
}
