<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesTelefonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_telefonos', function (Blueprint $table) {
            $table->id();
            $table->string('telefono');
            $table->unsignedBigInteger('telefono_tipo_id')->nullable();
            $table->unsignedBigInteger('perfil_persona_id');

            $table->foreign('telefono_tipo_id')
                    ->references('id')
                    ->on('telefonos_tipos')
                    ->onDelete('restrict');
            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_telefonos');
    }
}
