<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesCriminalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_criminales', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('grupo_criminal_id')->nullable();
            $table->unsignedBigInteger('nivel_peligrosidad_id');
            $table->unsignedBigInteger('perfil_persona_id');
            $table->text('observaciones')->nullable();
            $table->timestamps();

            $table->foreign('grupo_criminal_id')
                    ->references('id')
                    ->on('grupos_criminales')
                    ->onDelete('restrict');

            $table->foreign('nivel_peligrosidad_id')
                    ->references('id')
                    ->on('niveles_peligrosidades')
                    ->onDelete('restrict');

            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_criminales');
    }
}
