<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesIdentidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_identidades', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_persona_id');
            $table->unsignedBigInteger('identidad_tipo_id');
            $table->string('documento');
            $table->unsignedBigInteger('pais_emisor_id');
            $table->date('validez')->nullable();
            $table->unsignedBigInteger('verificacion_tipo_id')->nullable();
            $table->text('motivo_rechazo')->nullable();
            $table->timestamps();

            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');

            $table->foreign('identidad_tipo_id')
                    ->references('id')
                    ->on('identidades_tipos')
                    ->onDelete('restrict');

            $table->foreign('pais_emisor_id')
                    ->references('id')
                    ->on('paises')
                    ->onDelete('restrict');

            $table->foreign('verificacion_tipo_id')
                    ->references('id')
                    ->on('verificaciones_tipos')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_identidades');
    }
}
