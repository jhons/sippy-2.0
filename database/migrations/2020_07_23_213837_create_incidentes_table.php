<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidentes', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha');
            $table->text('detalles');
            $table->unsignedBigInteger('incidente_grado_id');
            $table->text('implicados')->nullable();
            $table->unsignedBigInteger('perfil_privado_id');

            $table->foreign('incidente_grado_id')
                    ->references('id')
                    ->on('incidentes_grados')
                    ->onDelete('restrict');
                    
            $table->foreign('perfil_privado_id')
                    ->references('id')
                    ->on('perfiles_privados')
                    ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentes');
    }
}
