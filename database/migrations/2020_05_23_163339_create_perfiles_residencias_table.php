<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesResidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_residencias', function (Blueprint $table) {
            $table->id();
            $table->string('direccion');
            $table->string('barrio')->nullable();
            $table->string('ciudad')->nullable();
            $table->unsignedBigInteger('pais_id')->nullable();
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->unsignedBigInteger('ciudad_id')->nullable();
            $table->boolean('principal')->nullable();
            $table->unsignedBigInteger('perfil_persona_id');
            $table->timestamps();

            $table->foreign('pais_id')
                    ->references('id')
                    ->on('paises')
                    ->onDelete('restrict');
           
            $table->foreign('departamento_id')
                    ->references('id')
                    ->on('departamentos')
                    ->onDelete('restrict');
            
            $table->foreign('ciudad_id')
                    ->references('id')
                    ->on('ciudades')
                    ->onDelete('restrict');
            
            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_residencias');
    }
}
