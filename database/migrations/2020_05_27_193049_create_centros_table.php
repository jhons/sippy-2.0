<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('centro_tipo_id');
            $table->string('nombre');
            $table->string('direccion')->nullable();
            $table->unsignedBigInteger('pais_id')->nullable();
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->unsignedBigInteger('ciudad_id')->nullable();
            $table->string('telefono')->nullable();
            $table->unsignedInteger('capacidad')->nullable();
            $table->decimal('lng', 11, 8)->nullable();
            $table->decimal('lat', 10, 8)->nullable();
            $table->timestamps();

            $table->foreign('centro_tipo_id')
                    ->references('id')
                    ->on('centros_tipos')
                    ->onDelete('restrict');
            $table->foreign('pais_id')
                    ->references('id')
                    ->on('paises')
                    ->onDelete('restrict');
            $table->foreign('departamento_id')
                    ->references('id')
                    ->on('departamentos')
                    ->onDelete('restrict');
            $table->foreign('ciudad_id')
                    ->references('id')
                    ->on('ciudades')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros');
    }
}
