<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentidadesFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identidades_fotos', function (Blueprint $table) {
            $table->id();
            $table->string('foto');
            $table->unsignedBigInteger('foto_tipo_id');
            $table->unsignedBigInteger('perfil_identidad_id');
            $table->timestamps();

            $table->foreign('foto_tipo_id')
                    ->references('id')
                    ->on('fotos_tipos')
                    ->onDelete('restrict');

            $table->foreign('perfil_identidad_id')
                    ->references('id')
                    ->on('perfiles_identidades')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identidades_fotos');
    }
}
