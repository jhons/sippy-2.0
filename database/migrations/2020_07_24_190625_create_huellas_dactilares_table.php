<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHuellasDactilaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('huellas_dactilares', function (Blueprint $table) {
            $table->id();
            $table->binary('captura');
            $table->timestamp('fecha');
            $table->unsignedBigInteger('huella_tipo_id');
            $table->unsignedBigInteger('perfil_persona_id');
            $table->timestamps();

            $table->foreign('huella_tipo_id')
                    ->references('id')
                    ->on('huellas_tipos')
                    ->onDelete('restrict');
            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('huellas_dactilares');
    }
}
