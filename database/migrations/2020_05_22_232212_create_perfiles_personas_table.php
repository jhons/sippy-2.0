<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_personas', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('apodo')->nullable();
            $table->string('alias')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->unsignedBigInteger('genero_id')->nullable();
            $table->unsignedBigInteger('estado_civil_id')->nullable();
            $table->unsignedBigInteger('etnia_id')->nullable();
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->unsignedBigInteger('grupo_vulnerable_id')->nullable();
            $table->timestamps();

            $table->foreign('genero_id')
                    ->references('id')
                    ->on('generos')
                    ->onDelete('restrict');

            $table->foreign('estado_civil_id')
                    ->references('id')
                    ->on('estados_civiles')
                    ->onDelete('restrict');

            $table->foreign('etnia_id')
                    ->references('id')
                    ->on('etnias')
                    ->onDelete('restrict');

            $table->foreign('usuario_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('restrict');

            $table->foreign('grupo_vulnerable_id')
                    ->references('id')
                    ->on('grupos_vulnerables')
                    ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_personas');
    }
}
