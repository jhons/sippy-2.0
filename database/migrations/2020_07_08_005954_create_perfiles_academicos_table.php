<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesAcademicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_academicos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('institucion_id')->nullable();
            $table->string('otro')->nullable();
            $table->unsignedBigInteger('nivel_academico_id')->nullable();
            $table->unsignedBigInteger('grado_academico_id')->nullable();
            $table->unsignedInteger('anio')->nullable();
            $table->date('fecha_promocion')->nullable();
            $table->unsignedBigInteger('perfil_persona_id');
            $table->timestamps();

            $table->foreign('institucion_id')
                    ->references('id')
                    ->on('instituciones')
                    ->onDelete('restrict');
            $table->foreign('nivel_academico_id')
                    ->references('id')
                    ->on('niveles_academicos')
                    ->onDelete('restrict');
            $table->foreign('grado_academico_id')
                    ->references('id')
                    ->on('grados_academicos')
                    ->onDelete('restrict');
            $table->foreign('perfil_persona_id')
                    ->references('id')
                    ->on('perfiles_personas')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_academicos');
    }
}
