<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_visitas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('centro_id');
            $table->string('nombre');
            $table->unsignedInteger('capacidad');
            $table->json('dias_visitas');
            $table->json('parametros')->nullable();
            $table->timestamps();

            $table->foreign('centro_id')
                    ->references('id')
                    ->on('centros')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_visitas');
    }
}
