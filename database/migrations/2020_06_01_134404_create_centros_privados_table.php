<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosPrivadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_privados', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_ingreso');
            $table->date('fecha_egreso')->nullable();
            $table->unsignedBigInteger('centro_id');
            $table->unsignedBigInteger('pabellon_id')->nullable();
            $table->unsignedBigInteger('celda_id')->nullable();
            $table->unsignedBigInteger('perfil_privado_id');
            $table->text('observacion')->nullable();
            $table->timestamps();

            $table->foreign('centro_id')
                ->references('id')
                ->on('centros')
                ->onDelete('restrict');
            $table->foreign('pabellon_id')
                ->references('id')
                ->on('centros_pabellones')
                ->onDelete('restrict');
            $table->foreign('celda_id')
                ->references('id') 
                ->on('pabellones_celdas')
                ->onDelete('restrict');
            $table->foreign('perfil_privado_id')
                ->references('id')
                ->on('perfiles_privados')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros_privados');
    }
}
