<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelacionTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Abogado/a'],
            ['nombre' => 'Abuelo/a'],
            ['nombre' => 'Amigo/a'],
            ['nombre' => 'Cliente/a'],
            ['nombre' => 'Concubino/a'],
            ['nombre' => 'Defendido/a'],
            ['nombre' => 'Defensor/a'],
            ['nombre' => 'Esposo/a'],
            ['nombre' => 'Hermano/a'],
            ['nombre' => 'Hijo/a'],
            ['nombre' => 'Madre'],
            ['nombre' => 'Nieto/a'],
            ['nombre' => 'Novio/a'],
            ['nombre' => 'Padre'],
            ['nombre' => 'Primo/a'],
            ['nombre' => 'Sobrino/a'],
            ['nombre' => 'Tio/a']              
        ];
        
        foreach ($items as $item) {
            DB::table('relaciones_tipos')->insert($item);
        }

    }
}
