<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CircunscripcionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'nombre' => '1° Circunscripción',
                'departamento_id' => 18
            ],
            [
                'nombre' => '2° Circunscripción',
                'departamento_id' => 4
            ],
            [
                'nombre' => '3° Circunscripción',
                'departamento_id' => 7
            ],
            [
                'nombre' => '4° Circunscripción',
                'departamento_id' => 1
            ],
            [
                'nombre' => '5° Circunscripción',
                'departamento_id' => 13
            ],
            [
                'nombre' => '6° Circunscripción',
                'departamento_id' => 10
            ],
            [
                'nombre' => '7° Circunscripción',
                'departamento_id' => 5
            ],
            [
                'nombre' => '8° Circunscripción',
                'departamento_id' => 12
            ],
            [
                'nombre' => '9° Circunscripción',
                'departamento_id' => 8
            ],
            [
                'nombre' => '10° Circunscripción',
                'departamento_id' => 9
            ],
            [
                'nombre' => '11° Circunscripción',
                'departamento_id' => 6
            ],
            [
                'nombre' => '12° Circunscripción',
                'departamento_id' => 2
            ],
            [
                'nombre' => '13° Circunscripción',
                'departamento_id' => 3
            ],
            [
                'nombre' => '14° Circunscripción',
                'departamento_id' => 15
            ],
            [
                'nombre' => '15° Circunscripción',
                'departamento_id' => 14
            ],
            [
                'nombre' => '16° Circunscripción',
                'departamento_id' => 11
            ],
            [
                'nombre' => '17° Circunscripción',
                'departamento_id' => 16
            ],
            [
                'nombre' => '18° Circunscripción',
                'departamento_id' => 17
            ]
        ];
        
        foreach ($items as $item) {
            DB::table('circunscripciones')->insert($item);
        }
    }
}
