<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EtniaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['nombre' => 'Ache (aché)'],
            ['nombre' => 'Ava guaraní (avá-chiripá)'],
            ['nombre' => 'Guaraní ñandéva (tapieté)'],
            ['nombre' => 'Mbya (mbya-guaraní)'],
            ['nombre' => 'Guaraní occidental (guarayo)'],
            ['nombre' => 'Paĩ tavyterã (pa\'ï-tavyterä)'],
            ['nombre' => 'Nivaclé'],
            ['nombre' => 'Maká (maka)'],
            ['nombre' => 'Manjui'],
            ['nombre' => 'Mataco'],
            ['nombre' => 'Ayoreo'],
            ['nombre' => 'Ybytoso (chamacoco)'],
            ['nombre' => 'Tomárãho'],
            ['nombre' => 'Enlhet norte (lengua)'],
            ['nombre' => 'Enxet sur'],
            ['nombre' => 'Angaité'],
            ['nombre' => 'Sanapaná'],
            ['nombre' => 'Guaná'],
            ['nombre' => 'Toba maskoy'],
            ['nombre' => 'Qom (toba qom)']
        ];
        
        foreach ($items as $item) {
            DB::table('etnias')->insert($item);
        }

    }
}
