<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoCriminalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Clan Rotela'],
            ['nombre' => 'PCC (Primer Comando Capital)'],
            ['nombre' => 'ACA (Asociacion Campesina Armada)'],
            ['nombre' => 'EPP (Ejercito del Pueblo Paraguayo)'],
            ['nombre' => 'La Firma'],
            ['nombre' => 'Comando Vermelho'],
            ['nombre' => 'Minotauro'],
            ['nombre' => 'PCCATARINENSE (Primer Comando Catarinense)']
        ];
        
        foreach ($items as $item) {
            DB::table('grupos_criminales')->insert($item);
        }
    }
}
