<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['nombre' => 'Casado/a'],
            ['nombre' => 'Soltero/a'],
            ['nombre' => 'Viudo/a'],
            ['nombre' => 'Divorciado/a'],
            ['nombre' => 'Concubinado/a']
        ];
        
        foreach ($items as $item) {
            DB::table('estados_civiles')->insert($item);
        }

    }
}
