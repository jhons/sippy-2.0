<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CentroTipoSeeder::class);
        $this->call(EstadoCivilSeeder::class);
        $this->call(EstadoSolicitudRegistroSeeder::class);
        $this->call(EtniaSeeder::class);
        $this->call(FotoTipoSeeder::class);
        $this->call(GeneroSeeder::class);
        $this->call(GrupoVulnerableSeeder::class);
        $this->call(HuellaTipoSeeder::class);
        $this->call(NivelAcademicoSeeder::class);
        $this->call(GradoAcademicoSeeder::class);
        $this->call(NivelPeligrosidadSeeder::class);
        $this->call(ProfesionSeeder::class);
        $this->call(RelacionTipoSeeder::class);
        $this->call(TelefonoTipoSeeder::class);
        $this->call(VerificacionTipoSeeder::class);
        $this->call(VisitaTipoSeeder::class);
        $this->call(HechoPunibleTipoSeeder::class);
        $this->call(HechoPunibleSeeder::class);
        $this->call(GrupoCriminalSeeder::class);
        $this->call(PaisSeeder::class);
        $this->call(DepartamentoSeeder::class);
        $this->call(CiudadSeeder::class);
        $this->call(CentroSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(IdentidadTipoSeeder::class);
        $this->call(IncidenteGradoSeeder::class);
    }
}
