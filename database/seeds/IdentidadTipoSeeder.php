<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IdentidadTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Cédula de Identidad'],
            ['nombre' => 'Pasaporte'],
            ['nombre' => 'Número temporal AFIS'],
            ['nombre' => 'Número Temporal Policía Nacional']
        ];
        
        foreach ($items as $item) {
            DB::table('identidades_tipos')->insert($item);
        }
    }
}
