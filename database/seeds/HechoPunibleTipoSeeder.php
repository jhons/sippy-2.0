<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HechoPunibleTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Delito'],
            ['nombre' => 'Crimen']
        ];
        
        foreach ($items as $item) {
            DB::table('hechos_punibles_tipos')->insert($item);
        }

    }
}
