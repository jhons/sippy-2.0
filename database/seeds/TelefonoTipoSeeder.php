<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TelefonoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Principal'],
            ['nombre' => 'Móvil'],
            ['nombre' => 'Casa'],
            ['nombre' => 'Trabajo'],
            ['nombre' => 'Otro']
        ];
        
        foreach ($items as $item) {
            DB::table('telefonos_tipos')->insert($item);
        }

    }
}
