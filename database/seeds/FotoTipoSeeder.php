<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FotoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['nombre' => 'Frontal'],
            ['nombre' => 'Reverso'],
            ['nombre' => 'Selfie']
        ];
        
        foreach ($items as $item) {
            DB::table('fotos_tipos')->insert($item);
        }

    }
}
