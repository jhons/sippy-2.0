<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HechoPunibleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'HURTO SEGUIDO DE VIOLENCIA', 'tipo_id' =>2],
            ['nombre' => 'LIBERACION DE PRESOS', 'tipo_id' =>2],
            ['nombre' => 'SOBORNO', 'tipo_id' =>2],
            ['nombre' => 'ENRIQUECIMIENTO ILICITO', 'tipo_id' =>2],
            ['nombre' => 'ASOCIACION TERRORISTA', 'tipo_id' =>2],
            ['nombre' => 'DESACATO', 'tipo_id' =>2],
            ['nombre' => 'ABIGEATO', 'tipo_id' =>2],
            ['nombre' => 'INVASION DE INMUEBLE', 'tipo_id' =>2],
            ['nombre' => 'FRUSTRACION DE LA PERSECUCION PENAL', 'tipo_id' =>2],
            ['nombre' => 'PRODUCCION DE RIESGOS COMUNES', 'tipo_id' =>2],
            ['nombre' => 'LESION DE CONFIANZA', 'tipo_id' =>2],
            ['nombre' => 'PORNOGRAFIA RELATIVA A NIÑOS Y ADOLESCENTES', 'tipo_id' =>2],
            ['nombre' => 'ABUSO DE DOCUMENTOS DE IDENTIDAD', 'tipo_id' =>2],
            ['nombre' => 'ABUSO POR MEDIOS TECNOLOGICOS', 'tipo_id' =>2],
            ['nombre' => 'LEY 1340 - TENENCIA, COMERCIALIZACION, POSESION Y TRAFICO DE DROGAS', 'tipo_id' =>2],
            ['nombre' => 'CONTRABANDO', 'tipo_id' =>2],
            ['nombre' => 'HOMICIDIO CULPOSO', 'tipo_id' =>1],
            ['nombre' => 'MALTRATO FISICO', 'tipo_id' =>1],
            ['nombre' => 'LESION', 'tipo_id' =>1],
            ['nombre' => 'LESION CULPOSA', 'tipo_id' =>1],
            ['nombre' => 'COACCION', 'tipo_id' =>1],
            ['nombre' => 'COACCION GRAVE', 'tipo_id' =>1],
            ['nombre' => 'AMENAZA', 'tipo_id' =>1],
            ['nombre' => 'HURTO', 'tipo_id' =>1],
            ['nombre' => 'ROBO', 'tipo_id' =>1],
            ['nombre' => 'ESTAFA', 'tipo_id' =>1],
            ['nombre' => 'REDUCCION', 'tipo_id' =>1],
            ['nombre' => 'ASOCIACION CRIMINAL', 'tipo_id' =>1],
            ['nombre' => 'PERTURBACION A LA PAZ PUBLICA', 'tipo_id' =>1],
            ['nombre' => 'USURA', 'tipo_id' =>2],
            ['nombre' => 'ABORTO', 'tipo_id' =>2],
            ['nombre' => 'USURPACION DE FUNCIONES PUBLICAS', 'tipo_id' =>2],
            ['nombre' => 'USO DE DOCUMENTOS PUBLICOS DE CONTENIDO FALSO', 'tipo_id' =>2],
            ['nombre' => 'EXHORTO', 'tipo_id' =>2],
            ['nombre' => 'INCESTO', 'tipo_id' =>2],
            ['nombre' => 'HOMICIDIO DOLOSO', 'tipo_id' =>2],
            ['nombre' => 'COACCION SEXUAL Y VIOLACION', 'tipo_id' =>2],
            ['nombre' => 'ABUSO SEXUAL EN PERSONAS INDEFENSAS', 'tipo_id' =>2],
            ['nombre' => 'FEMINICIDIO', 'tipo_id' =>2],
            ['nombre' => 'H.P. C/ EL MEDIO AMBIENTE', 'tipo_id' =>2],
            ['nombre' => 'CIRCULACIÓN DE MONEDA NO AUTENTICA', 'tipo_id' =>2],
            ['nombre' => 'TRASGRESION A LA LEY DE ARMAS DE FUEGO Y OTROS', 'tipo_id' =>2],
            ['nombre' => 'VIOLACIÓN DE LA LEY 4036/10 DE ARMAS', 'tipo_id' =>2]
        ];
        
        foreach ($items as $item) {
            DB::table('hechos_punibles')->insert($item);
        }

    }
}
