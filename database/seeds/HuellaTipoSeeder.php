<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HuellaTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Pulgar Derecho'],
            ['nombre' => 'Indice Derecho'],
            ['nombre' => 'Medio Derecho'],
            ['nombre' => 'Anular Derecho'],
            ['nombre' => 'Meñique Derecho'],
            ['nombre' => 'Palma Derecha'],
            ['nombre' => 'Pulgar Izquierdo'],
            ['nombre' => 'Indice Izquierdo'],
            ['nombre' => 'Medio Izquierdo'],
            ['nombre' => 'Anular Izquierdo'],
            ['nombre' => 'Meñique Izquierdo'],
            ['nombre' => 'Palma Izquierda']
        ];
        
        foreach ($items as $item) {
            DB::table('huellas_tipos')->insert($item);
        }

    }
}
