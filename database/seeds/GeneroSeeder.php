<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['nombre' => 'Masculino'],
            ['nombre' => 'Femenino']
        ];
        
        foreach ($items as $item) {
            DB::table('generos')->insert($item);
        }

    }
}
