<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Casa del Buen Pastor', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  18, 'ciudad_id' =>  1, 'lng' => -25.2902540, 'lat' => -57.5914252],
            ['nombre' => 'Ciudad del Este', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  10, 'ciudad_id' =>  165, 'lng' => -25.5220116, 'lat' => -54.6185027],
            ['nombre' => 'Concepción', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  1, 'ciudad_id' =>  2, 'lng' => -23.4076249, 'lat' => -57.3875262],
            ['nombre' => 'Coronel Oviedo', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  5, 'ciudad_id' =>  74, 'lng' => -25.5374880, 'lat' => -56.4546640],
            ['nombre' => 'Emboscada', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  3, 'ciudad_id' =>  41, 'lng' => -25.0970730, 'lat' => -57.3167750],
            ['nombre' => 'Encarnación', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  7, 'ciudad_id' =>  107, 'lng' => -27.3127048, 'lat' => -55.8030058],
            ['nombre' => 'Granja Itá Porã', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  3, 'ciudad_id' =>  41, 'lng' => -25.0975355, 'lat' => -57.3139626],
            ['nombre' => 'Granja Ko\'e Pyahu', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  18, 'ciudad_id' =>  1, 'lng' => null, 'lat' => null],
            ['nombre' => 'Juana Ma. de Lara', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  10, 'ciudad_id' =>  165, 'lng' => null, 'lat' => null],
            ['nombre' => 'Misiones', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  8, 'ciudad_id' =>  137, 'lng' => -26.693061, 'lat' => -57.0748426],
            ['nombre' => 'Nacional (Tacumbú)', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  18, 'ciudad_id' =>  1, 'lng' => -25.308607, 'lat' => -57.6488633],
            ['nombre' => 'Nueva Oportunidad', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  19, 'ciudad_id' =>  2, 'lng' => null, 'lat' => null],
            ['nombre' => 'Padre Juan A. de la Vega', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  3, 'ciudad_id' =>  41, 'lng' => -25.097138, 'lat' => -57.3137400],
            ['nombre' => 'Pedro J. Caballero', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  13, 'ciudad_id' =>  222, 'lng' => -22.568931, 'lat' => -55.7581877],
            ['nombre' => 'San Pedro', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  2, 'ciudad_id' =>  15, 'lng' => -24.089158, 'lat' => -57.0926656],
            ['nombre' => 'Serafína Dávalos', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  5, 'ciudad_id' =>  74, 'lng' => -25.537927, 'lat' => -56.4566860],
            ['nombre' => 'UPIE (Esperanza)', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  18, 'ciudad_id' =>  1, 'lng' => -25.309784, 'lat' => -57.6483536],
            ['nombre' => 'Villarrica', 'centro_tipo_id' =>  1, 'pais_id' =>  181, 'departamento_id' =>  4, 'ciudad_id' =>  56, 'lng' => -25.784891, 'lat' => -56.4326417]
        ];
        
        foreach ($items as $item) {
            DB::table('centros')->insert($item);
        }
    }
}
