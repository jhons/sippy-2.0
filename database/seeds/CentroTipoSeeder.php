<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CentroTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Adultos'],
            ['nombre' => 'Menores']
        ];
        
        foreach ($items as $item) {
            DB::table('centros_tipos')->insert($item);
        }
    }
}
