<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoVulnerableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Adultos mayores'],
            ['nombre' => 'Extranjeros'],
            ['nombre' => 'Indígenas'],
            ['nombre' => 'LGBT (Lesbiana, Gay, Bisexual y Transgénero)'],
            ['nombre' => 'Mujeres con hijos'],
            ['nombre' => 'Mujeres en período de gestación'],
            ['nombre' => 'Personas con discapacidad']            
        ];
        
        foreach ($items as $item) {
            DB::table('grupos_vulnerables')->insert($item);
        }

    }
}
