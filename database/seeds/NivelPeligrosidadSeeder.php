<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NivelPeligrosidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Bajo', 'color' => '#32CD32'],     // verde
            ['nombre' => 'Medio', 'color' => '#FFFF00'],    // amarillo
            ['nombre' => 'Alto', 'color' => '#FF0000'],     // rojo
            ['nombre' => 'Extremo', 'color' => '#FF00FF']   // fucsia
        ];
        
        foreach ($items as $item) {
            DB::table('niveles_peligrosidades')->insert($item);
        }

    }
}
