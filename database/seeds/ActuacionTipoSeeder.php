<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActuacionTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Acta'],
            ['nombre' => 'Auto Interlocutorio'],
            ['nombre' => 'Cédula de Notificación'],
            ['nombre' => 'Informe del Actuario'],
            ['nombre' => 'Oficio'],
            ['nombre' => 'Presentación de las Partes'],
            ['nombre' => 'Presentación Electrónica'],
            ['nombre' => 'Providencia'],
            ['nombre' => 'Providencia de Mero Trámite'],
            ['nombre' => 'Sentencia Definitiva']

        ];
        
        foreach ($items as $item) {
            DB::table('actuaciones_tipos')->insert($item);
        }
    }
}
