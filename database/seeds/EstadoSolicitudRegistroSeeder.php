<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoSolicitudRegistroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['nombre' => 'Pendiente'],
            ['nombre' => 'Enviado'],
            ['nombre' => 'Abierto'],
            ['nombre' => 'Registrado']
        ];
        
        foreach ($items as $item) {
            DB::table('estados_solicitudes_registros')->insert($item);
        }

    }
}
