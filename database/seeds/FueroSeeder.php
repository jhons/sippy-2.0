<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FueroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['id' => 1, 'nombre' => 'Civil y Comercial'],
            ['id' => 2, 'nombre' => 'Laboral'],
            ['id' => 3, 'nombre' => 'Tutelar Menor'],
            ['id' => 4, 'nombre' => 'Correccional Menor'],
            ['id' => 5, 'nombre' => 'Penal'],
            ['id' => 6, 'nombre' => 'Penal Liquidación'],
            ['id' => 7, 'nombre' => 'Constitucional'],
            ['id' => 8, 'nombre' => 'Contencioso Administrativo'],
            ['id' => 9, 'nombre' => 'Niñez y Adolescencia'],
            ['id' => 10, 'nombre' => 'Ejecución Penal'],
            ['id' => 11, 'nombre' => 'Sindicatura General de Quiebras'],
            ['id' => 12, 'nombre' => 'Justicia Letrada'],
            ['id' => 13, 'nombre' => 'Justicia de Paz'],
        ];
        
        foreach ($items as $item) {
            DB::table('fueros')->insert($item);
        }
    }
}
