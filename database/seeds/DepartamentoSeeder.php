<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'nombre' => 'Concepción', 'pais_id' => 181],
            ['id' => 2, 'nombre' => 'San Pedro', 'pais_id' => 181],
            ['id' => 3, 'nombre' => 'Cordillera', 'pais_id' => 181],
            ['id' => 4, 'nombre' => 'Guairá', 'pais_id' => 181],
            ['id' => 5, 'nombre' => 'Caaguazú', 'pais_id' => 181],
            ['id' => 6, 'nombre' => 'Caazapá', 'pais_id' => 181],
            ['id' => 7, 'nombre' => 'Itapúa', 'pais_id' => 181],
            ['id' => 8, 'nombre' => 'Misiones', 'pais_id' => 181],
            ['id' => 9, 'nombre' => 'Paraguarí', 'pais_id' => 181],
            ['id' => 10, 'nombre' => 'Alto Paraná', 'pais_id' => 181],
            ['id' => 11, 'nombre' => 'Central', 'pais_id' => 181],
            ['id' => 12, 'nombre' => 'Ñeembucú', 'pais_id' => 181],
            ['id' => 13, 'nombre' => 'Amambay', 'pais_id' => 181],
            ['id' => 14, 'nombre' => 'Canindeyú', 'pais_id' => 181],
            ['id' => 15, 'nombre' => 'Presidente Hayes', 'pais_id' => 181],
            ['id' => 16, 'nombre' => 'Boquerón', 'pais_id' => 181],
            ['id' => 17, 'nombre' => 'Alto Paraguay', 'pais_id' => 181],
            ['id' => 18, 'nombre' => 'Capital', 'pais_id' => 181],
            ['id' => 19, 'nombre' => 'No Disponible', 'pais_id' => 181]
        ];
        
        foreach ($items as $item) {
            DB::table('departamentos')->insert($item);
        }
    }
}
