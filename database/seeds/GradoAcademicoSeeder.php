<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GradoAcademicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Maternal', 'nivel_academico_id' => 2],
            ['nombre' => 'Pre-Jardín', 'nivel_academico_id' => 2],
            ['nombre' => 'Jardín', 'nivel_academico_id' => 2],
            ['nombre' => 'Pre-escolar', 'nivel_academico_id' => 3],
            ['nombre' => 'Primer grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Segundo Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Tercer Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Cuarto Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Quinto Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Sexto Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Séptimo Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Octavo Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Noveno Grado', 'nivel_academico_id' => 4],
            ['nombre' => 'Primer Año de la Media', 'nivel_academico_id' => 5],
            ['nombre' => 'Segundo Año de la Media', 'nivel_academico_id' => 5],
            ['nombre' => 'Tercer Año de la Media', 'nivel_academico_id' => 5],
            ['nombre' => 'Título de grado', 'nivel_academico_id' => 6],
            ['nombre' => 'Posgrado', 'nivel_academico_id' => 7],
            ['nombre' => 'Maestria', 'nivel_academico_id' => 8],
            ['nombre' => 'Doctorado', 'nivel_academico_id' => 9]
        ];
        
        foreach ($items as $item) {
            DB::table('grados_academicos')->insert($item);
        }
    }
}
