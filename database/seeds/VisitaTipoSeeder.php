<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisitaTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Conyugal'],
            ['nombre' => 'Regular']
        ];
        
        foreach ($items as $item) {
            DB::table('visitas_tipos')->insert($item);
        }

    }
}
