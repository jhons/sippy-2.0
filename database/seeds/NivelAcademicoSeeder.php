<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NivelAcademicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Sin Educación Formal'],
            ['nombre' => 'Nivel Inicial'],
            ['nombre' => 'Preescolar'],
            ['nombre' => 'Educación Escolar Básica'],
            ['nombre' => 'Educación Media'],
            ['nombre' => 'Educación Terciaria-Universidad'],
            ['nombre' => 'Posgrado'],
            ['nombre' => 'Maestría'],
            ['nombre' => 'Doctorado']
        ];
        
        foreach ($items as $item) {
            DB::table('niveles_academicos')->insert($item);
        }

    }
}
