<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Admin',
                'email' => 'admin@sippy.dev',
                'password' => bcrypt('password'),
                'surname' => 'MJ',
                'username' => 'admin',
                'avatar' => 'fcf78a4a2b.png'
            ]
        ];
        
        foreach ($items as $item) {
            DB::table('users')->insert($item);
        }
    }
}
