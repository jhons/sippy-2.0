<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VerificacionTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['nombre' => 'Pendiente'],
            ['nombre' => 'En proceso'],
            ['nombre' => 'Verificado'],
            ['nombre' => 'Rechazado']
        ];
        
        foreach ($items as $item) {
            DB::table('verificaciones_tipos')->insert($item);
        }

    }
}
