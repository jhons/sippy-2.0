<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CentroPrivadoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CentroPrivadoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CentroPrivadoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\CentroPrivado');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/centro-privado');
        $this->crud->setEntityNameStrings('centro privado', 'centros privados');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CentroPrivadoRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();
        $this->crud->addField([
            'name'          => 'centro_id',
            'type'          => 'select2_from_ajax_multiple',
            'label'         => "centro",
            'entity'        => 'centro',
            'attribute'     => 'nombre',
            'data_source'   => url('api/centro'),
            'placeholder'   => 'Seleccione un centro',
            'minimum_input_length' => 2,
            'include_all_form_fields' => true
        ]);

        // $this->crud->addField([
        //     'name'          => 'centro_id',
        //     'type'          => 'select2',
        //     'label'         => "Centro",
        //     'entity'        => 'centro',
        //     'attribute'     => 'nombre',
        //     'model'         => "App\Models\Centro",
        // ]);

        
        $this->crud->addField([
            'name'          => 'fecha_ingreso',
            'type'          => 'date_picker',
            'label'         => "Fecha Ingreso",
            'default'       => date('Y-m-d'),
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd/mm/yyyy',
                'language' => 'es',
                'endDate'  => 'new date()',
                // 'setDate'  => 'new Date()',
             ],
        ]);
        $this->crud->addField([
            'name'          => 'centro_id',
            'type'          => 'textarea',
            'label'         => "Fecha Ingreso"
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
