<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Hazzard\Filepicker\Handler;
use Hazzard\Filepicker\Uploader;
use Intervention\Image\ImageManager;
use Hazzard\Config\Repository as Config;

class FilepickerAvatarController extends Controller
{
    /**
     * @var \Hazzard\Filepicker\Handler
     */
    protected $handler;

    /**
     * Create a new controller instance.
     */
    public function __construct(Request $request)
    {
        $this->handler = new Handler(
            new Uploader($config = new Config, new ImageManager)
        );

        $config['debug'] = config('app.debug');
        $config['upload_dir'] = storage_path('app/public/' . $request->origin);
        $config['upload_url'] = Storage::disk($request->disk)->url($request->origin);
        $config['accept_file_types'] = 'jpg|jpeg|png|gif';
        $config['keep_original_image'] = isset($request->keep_original) ? $request->keep_original : true;
        $config['image_versions.avatar'] = array(
            'width' => isset($request->width) ? $request->width : 300,
            'height' => isset($request->height) ? $request->width : 300,
        );

        // Here you may get the a pic id from session.
        $picId = substr(md5(uniqid(rand())),0,10);

        // Events

        /**
         * Fired before the file upload starts.
         *
         * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
         */
        $this->handler->on('upload.before', function ($file) use ($picId) {
            // Save the avatar to a temporary file.
            $file->save = '~' . $picId;
        });

        /**
         * Fired before cropping.
         *
         * @param \Symfony\Component\HttpFoundation\File\File $file
         * @param \Intervention\Image\Image $image
         */
        $this->handler->on('crop.before', function ($file, $image) use ($picId) {
            // Check if the pic is allowed the crop the image
            // throw new \Hazzard\Filepicker\Exception\AbortException('Not allowed');

            $file->save = $picId;
        });

        
        /**
         * Fired after cropping.
         *
         * @param \Symfony\Component\HttpFoundation\File\File $file
         * @param \Intervention\Image\Image $image
         */
        $this->handler->on('crop.after', function ($file, $image) {
            // Here you may save/update the image into database.
            // $_SESSION['avatar'] = $file->getFilename();
        });

        /**
         * Fired on file deletion.
         *
         * @param \Symfony\Component\HttpFoundation\File\File $file
         */
        $this->handler->on('file.delete', function ($file) {
            // Here you may delete the image from database.
            // unset($_SESSION['avatar']);
        });

        // Handle the request.
        // $this->handler->handle()->send();

    }

    /**
     * Handle an incoming HTTP request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request)
    {
        return $this->handler->handle($request);
    }
}