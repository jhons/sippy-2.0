<?php

namespace App\Http\Controllers\Admin;

use App\Models\IdentidadTipo;
use App\Models\Centro;
use App\Models\Pais;
use Illuminate\Http\Request;
use App\Http\Requests\SolicitudRequest;
use App\Models\SolicitudRegistro;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class SolicitudController extends CrudController
{
    public function index()
    {
        $centro = Centro::where('establecimiento_tipo_id', 1)->orderBy('nombre')->get();
        $nacionalidades = Pais::all();
        $tipos_documentos = IdentidadTipo::all();

        return view('solicitud', compact('centro', 'nacionalidades', 'tipos_documentos'));
    }

    public function store(SolicitudRequest $request)
    { 
        SolicitudRegistro::create($request->all());
        return redirect('solicitud')->with('status', 'Solicitud enviada exitosamente!');
    }
}
