<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CiudadRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CiudadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CiudadCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Ciudad');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/ciudad');
        $this->crud->setEntityNameStrings('ciudad', 'ciudades');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        CRUD::column('nombre')
            ->type('text')
            ->label('Nombre');
        
        CRUD::column('departamento_id')
            ->type('select')
            ->label('Departamento')
            ->entity('departamento')
            ->attribute('nombre')
            ->model("App\Models\Departamento");

        CRUD::column('pais_id')
            ->type('select')
            ->label('País')
            ->entity('pais')
            ->attribute('nombre')
            ->model("App\Models\Pais");


        // $this->crud->setColumnDetails('pais_id', ['label' => 'País', 'type' => 'select', 'entity' => 'pais', 'attribute' => 'nombre', 'model' => "App\Models\Pais"]);
        // $this->crud->setColumnDetails('departamento_id', ['label' => 'Departamento', 'type' => 'select', 'entity' => 'departamento', 'attribute' => 'nombre', 'model' => "App\Models\Departamento"]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CiudadRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();
        CRUD::field('nombre')
            ->type('text')
            ->label('Nombre');
        
        CRUD::field('pais_id')
            ->type('select2')
            ->label('País')
            ->entity('pais')
            ->attribute('nombre')
            ->model("App\Models\Pais")
            ->default('181');
        
        CRUD::field('departamento_id')
            ->type('select2_from_ajax')
            ->label('Departamento')
            ->entity('departamento')
            ->attribute('nombre')
            ->data_source(url('api/departamento'))
            ->placeholder('Seleccione un departamento')
            ->minimum_input_length(0)
            ->dependencies('pais_id')
            ->include_all_form_fields(true);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
