<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AreaVisitaRequest;
use App\Models\AreaVisita;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AreaVisitaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AreaVisitaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\AreaVisita');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/area-visita');
        $this->crud->setEntityNameStrings('area visita', 'areas visitas');

        $this->crud->enableDetailsRow();
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        
        CRUD::column('centro_id')
            ->type('select')
            ->label('Centro')
            ->entity('centro')
            ->atrtribute('nombre')
            ->model("App\Models\Centro");

        CRUD::column('nombre')
            ->type('text')
            ->label('Nombre Área');

        CRUD::column('capacidad')
            ->type('number')
            ->label('Capacidad')
            ->suffix(' pers.');

        // $this->crud->addColumn([
        //     'name'  => 'dias_visitas', 
        //     'label' => 'Días y horarios', 
        //     'type'  => 'table', 
        //     'columns' => [
        //         'dia'         => 'Día',
        //         'hora_inicio' => 'Inicio',
        //         'hora_fin'    => 'Fin'
        //     ]
        // ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AreaVisitaRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        $this->crud->addField([
            'label'     => "Centro",
            'type'      => 'select2',
            'name'      => 'centro_id',
            'entity'    => 'centro',
            'attribute' => 'nombre',
            'model'     => "App\Models\Centro",
        ]);

        $this->crud->addField([
            'label'     => "Nombre del área",
            'type'      => 'text',
            'name'      => 'nombre',
        ]);

        $this->crud->addField([
            'label'     => "Capacidad",
            'type'      => 'number',
            'name'      => 'capacidad',
            'attributes' => ["step" => '5'],
            'suffix'    => 'personas',
            'attributes' => [
                'min' => 5
            ]
        ]);

        $this->crud->addField([
            'name'   => 'dias_visitas',
            'label'  => 'Días y horarios de visita',
            'type'   => 'repeatable',
            'fields' => [
                [
                    'name'          => 'dia',
                    'type'          => 'select_from_array',
                    'label'         => 'Día',
                    'options'       => [0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado'],
                    'allows_null'   => false,
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                // [
                //     'name'    => 'hora_inicio',
                //     'type'    => 'time',
                //     'label'   => 'Hora Inicio',
                //     'wrapper' => ['class' => 'form-group col-md-4'],
                // ],
                // [
                //     'name'    => 'hora_fin',
                //     'type'    => 'time',
                //     'label'   => 'Hora Fin',
                //     'wrapper' => ['class' => 'form-group col-md-4'],
                // ],
                [
                    'name'    => 'hora_inicio',
                    'type'    => 'datetime_picker',
                    'label'   => 'Hora Inicio',
                    'datetime_picker_options' => [
                        'format' => 'HH:mm',
                        'language' => 'es',
                        'stepping' => 30,
                        'enabledHours' => [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                    ],
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'hora_fin',
                    'type'    => 'datetime_picker',
                    'label'   => 'Hora Fin',
                    'datetime_picker_options' => [
                        'format' => 'HH:mm',
                        'language' => 'es',
                        'stepping' => 30,
                        'enabledHours' => [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                    ],
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ]
        ]);

        $this->crud->addField([
            'name'   => 'parametros',
            'label'  => 'Parámetros especiales',
            'type'   => 'table',
            'columns' => [
                'clave' => 'Clave',
                'valor' => 'Valor' 
            ]
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function showDetailsRow($id)
    {
        $area = AreaVisita::find($id);
        $horarios = $area->dias_visitas;
        $parametros = $area->parametros;
        $dias = [0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado'];
        return view('vendor.backpack.crud.details_row_horarios', compact('horarios', 'dias', 'parametros'));
    }
}
