<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PabellonCeldaRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PabellonCeldaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PabellonCeldaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PabellonCelda');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/celda');
        $this->crud->setEntityNameStrings('celda', 'celdas');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

                        
        CRUD::column('centro')
            ->type('model_function')
            ->label('Centro')
            ->function_name('getCentro');

        CRUD::column('centro_pabellon_id')
            ->type('select')
            ->label('Pabellón')
            ->entity('pabellon')
            ->attribute('nombre')
            ->model("App\Models\CentroPabellon");

        CRUD::column('numero')
            ->type('text')
            ->label('Celda');
        
        CRUD::column('capacidad')
            ->type('text')
            ->label('Capacidad');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PabellonCeldaRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        // CRUD::field('centro')
        //     ->type('select2')
        //     ->label('Centro')
        //     ->entity('pabellonCentro')
        //     ->attribute('nombre')
        //     ->model("App\Models\Centro");
        
        $this->crud->addField([
                'label'     => "Centro",
                'type'      => 'select2',
                'name'      => 'centro',
                'entity'    => 'pabellonCentro',
                'attribute' => 'nombre',
                'model'     => "App\Models\Centro",
        ]);
        
        $this->crud->addField([
            'name'      => 'centro_pabellon_id',
            'type'      => 'select2_from_ajax',
            'label'     => 'Pabellón',
            'entity'    => 'pabellon',
            'attribute' => 'nombre',
            'model'     => "App\Models\CentroPabellon",
            'data_source' => url('api/pabellon'),
            'placeholder' => 'Seleccione un pabellon',
            'minimum_input_length' => 0,
            'dependencies' => 'centro',
            'include_all_form_fields' => true
        ]);
        
        CRUD::field('numero')
            ->type('text')
            ->label('Celda');

        CRUD::field('capacidad')
            ->type('number')
            ->label('Capacidad');

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
