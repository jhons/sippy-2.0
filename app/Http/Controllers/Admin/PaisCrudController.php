<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PaisRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PaisCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaisCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Pais');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pais');
        $this->crud->setEntityNameStrings('pais', 'paises');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->setColumnDetails('phone_code', ['label' => 'Código Telefónico']);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PaisRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        $this->crud->modifyField('phone_code', ['label' => 'Código Telefónico', 'type' => 'number']);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
