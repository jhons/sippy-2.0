<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CentroPabellonRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CentroPabellonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CentroPabellonCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\CentroPabellon');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pabellon');
        $this->crud->setEntityNameStrings('pabellón', 'pabellones');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        CRUD::column('nombre')
            ->type('text')
            ->label('Nombre');
        
        CRUD::column('centro_id')
            ->type('select')
            ->label('Centro')
            ->entity('centro')
            ->atrtribute('nombre')
            ->model("App\Models\Centro");

        CRUD::column('capacidad')
            ->type('text')
            ->label('Capacidad');
        
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CentroPabellonRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        CRUD::field('nombre')
            ->type('text')
            ->label('Nombre');

        CRUD::field('centro_id')
            ->type('select2')
            ->label('Centro')
            ->entity('centro')
            ->attribute('nombre')
            ->model("App\Models\Centro");

        CRUD::field('capacidad')
            ->type('number')
            ->label('Capacidad');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
