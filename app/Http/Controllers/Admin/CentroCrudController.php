<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CentroRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
// use Backpack\CRUD\app\Library\CrudPanel\CrudPanel as CRUD;

/**
 * Class CentroCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CentroCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Centro');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/centro');
        $this->crud->setEntityNameStrings('centro', 'centros');

        $this->crud->enableExportButtons();
        // $this->addCustomCrudFilters();
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        CRUD::column('nombre')
            ->type('text')
            ->label('Nombre');

        CRUD::column('capacidad')
            ->type('text')
            ->label('Capacidad');
        
        CRUD::column('telefono')
            ->type('text')
            ->label('Teléfono');
        
        CRUD::column('direccion')
            ->type('text')
            ->label('Dirección')
            ->visibleInTable(false);

        CRUD::column('centro_tipo_id')
            ->type('select')
            ->label('Tipo')
            ->entity('tipo_centro')
            ->atrtribute('nombre')
            ->model("App\Models\CentroTipo");

        CRUD::column('pais_id')
            ->type('select')
            ->label('País')
            ->entity('pais')
            ->atrtribute('nombre')
            ->model("App\Models\Pais")
            ->visibleInTable(false);

        CRUD::column('departamento_id')
            ->type('select')
            ->label('Departamento')
            ->entity('departamento')
            ->atrtribute('nombre')
            ->model("App\Models\Departamento")
            ->visibleInTable(false);

        CRUD::column('ciudad_id')
            ->type('select')
            ->label('Ciudad')
            ->entity('ciudad')
            ->atrtribute('nombre')
            ->model("App\Models\Ciudad")
            ->visibleInTable(false);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CentroRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        $this->crud->addField([
                'label'     => "Tipo",
                'type'      => 'select2',
                'name'      => 'centro_tipo_id',
                'entity'    => 'tipo_centro',
                'attribute' => 'nombre',
                'model'     => "App\Models\CentroTipo",
        ]);

        CRUD::field('nombre')
            ->type('text')
            ->label('Nombre');

        CRUD::field('capacidad')
            ->type('number')
            ->label('Capacidad');
        
        CRUD::field('telefono')
            ->type('text')
            ->label('Teléfono');

        CRUD::field('direccion')
            ->type('text')
            ->label('Dirección');
        
        CRUD::field('pais_id')
            ->type('select2')
            ->label('País')
            ->entity('pais')
            ->attribute('nombre')
            ->model("App\Models\Pais")
            ->default('181');
        
        CRUD::field('departamento_id')
            ->type('select2_from_ajax')
            ->label('Departamento')
            ->entity('departamento')
            ->attribute('nombre')
            ->data_source(url('api/departamento'))
            ->placeholder('Seleccione un departamento')
            ->minimum_input_length(0)
            ->dependencies('pais_id')
            ->include_all_form_fields(true);

        CRUD::field('ciudad_id')
            ->type('select2_from_ajax')
            ->label('Ciudad')
            ->entity('ciudad')
            ->attribute('nombre')
            ->data_source(url('api/ciudad'))
            ->placeholder('Seleccione una ciudad')
            ->minimum_input_length(0)
            ->dependencies('departamento_id')
            ->include_all_form_fields(true);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
