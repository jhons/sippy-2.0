<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PerfilPersonaRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PerfilPersonaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PerfilPersonaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PerfilPersona');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/perfil-persona');
        $this->crud->setEntityNameStrings('perfil persona', 'perfiles personas');

        $this->crud->with('foto_principal');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        
            
        CRUD::column('avatar')
            ->type('closure')
            ->label('Foto')
            ->function(function($entry) {
                $foto_principal = $entry->foto_principal;
                if ($foto_principal)
                    return '<img src="'. url("storage/personas/fotos/" . $foto_principal->foto) . '" alt="" width="30" class="">';

                $usuario = $entry->usuario;
                if ($usuario)
                    return '<img src="'. url("storage/users/" . $usuario->avatar) . '" alt="" width="30" class="">';

                return '<img src="'. url("storage/users/avatar.png") . '" alt="" width="30" class="">';
            });

            
        CRUD::column('nombres')
            ->type('text')
            ->label('Nombres');

        CRUD::column('apellidos')
            ->type('text')
            ->label('Apellidos');
    
        CRUD::column('fecha_nacimiento')
            ->type('date')
            ->label('Fecha Nacimiento');
    
        CRUD::column('genero_id')
            ->type('select')
            ->label('Género')
            ->entity('genero')
            ->attribute('nombre')
            ->model("App\Models\Genero");
    
        CRUD::column('usuario_id')
            ->type('select')
            ->label('Usuario')
            ->entity('usuario')
            ->attribute('username')
            ->model("App\User");
        
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PerfilPersonaRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        $this->crud->addField([   // date_range
            'name'  => 'start_date', // db columns for start_date & end_date
            'label' => 'Event Date Range',
            'type'  => 'date_picker',
        
            // OPTIONALS
            // default values for start_date & end_date
            'default'            => date("Y-m-d"), 
            // options sent to daterangepicker.js
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy',
                'language' => 'es'
             ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
