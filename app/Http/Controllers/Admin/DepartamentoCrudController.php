<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DepartamentoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DepartamentoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DepartamentoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Departamento');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/departamento');
        $this->crud->setEntityNameStrings('departamento', 'departamentos');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->setColumnDetails('pais_id', ['label' => 'País', 'type' => 'select', 'entity' => 'pais', 'attribute' => 'nombre', 'model' => "App\Models\Pais"]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DepartamentoRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        $this->crud->modifyField('pais_id', ['label' => 'País', 'type' => 'select2', 'entity' => 'pais','attribute' => 'nombre', 'model' => "App\Models\Pais", 'default' => '181']);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
