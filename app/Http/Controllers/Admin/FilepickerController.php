<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Hazzard\Filepicker\Handler;
use Hazzard\Filepicker\Uploader;
use Intervention\Image\ImageManager;
use Hazzard\Config\Repository as Config;

class FilepickerController extends Controller
{
    /**
     * @var \Hazzard\Filepicker\Handler
     */
    protected $handler;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->handler = new Handler(
            new Uploader($config = new Config, new ImageManager)
        );

        $config['upload_dir'] = storage_path('app/public/files');
        $config['upload_url'] = Storage::disk('public')->url('files');
        $config['debug'] = config('app.debug');
    }

    /**
     * Handle an incoming HTTP request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request)
    {
        return $this->handler->handle($request);
    }
}