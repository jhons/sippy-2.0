<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadCropController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->image)
            return response()->json([
                'status' => false, 
                'message' => 'Debe seleccionar una imagen'
            ]);
            
        if (!$request->destination)
            return response()->json([
                'status' => false, 
                'message' => 'No se ha configurado un destino para almacenar la imagen'
            ]);

        $data = $request->image;
        $destinationFolder = $request->destination;

        $image_array_1 = explode(";", $data);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);

        $imageName = substr(md5(uniqid(rand())),0,10) . '.png';

        Storage::disk('public')->put($destinationFolder . '/' . $imageName, $data);
   
        return response()->json([
                                    'status' => true, 
                                    'image' => $imageName
                                ]);
    }

}
