<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\IncidenteGradoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class IncidenteGradoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class IncidenteGradoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\IncidenteGrado');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/incidente-grado');
        $this->crud->setEntityNameStrings('incidente grado', 'incidentes grados');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->modifyColumn('color', [
            'type'     => 'closure',
            'function' => function($entry) {
                return '<i class="la la-square la-lg" style="color: ' . $entry->color . ' "></i> ' . $entry->color;
            }
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(IncidenteGradoRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();

        $this->crud->modifyField('color', [  
            'type'                 => 'color_picker',
            'default'              => '#000000',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
