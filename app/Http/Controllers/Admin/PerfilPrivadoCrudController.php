<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PerfilPrivadoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PerfilPrivadoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PerfilPrivadoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PerfilPrivado');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/perfil-privado');
        $this->crud->setEntityNameStrings('PPL', 'perfiles privados');

        $this->crud->with('persona');
        $this->crud->with('centro_privado_actual');

        $this->crud->addClause('whereHas', 'centros_privados', function($query) {
            $query->whereNull('fecha_egreso');
        });

        if (isset(backpack_auth()->user()->centro_id)) {
            $this->crud->addClause('whereHas', 'centros_privados', function($query){
                $query->where('centro_id', backpack_auth()->user()->centro_id);
            });
        }
        
        $this->crud->setShowView('backpack::crud.privado_view');
        $this->crud->setCreateView('backpack::crud.privado_create');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

        CRUD::column('avatar')
            ->type('closure')
            ->label('Foto')
            ->function(function($entry) {
                $foto_principal = $entry->persona->foto_principal;
                if ($foto_principal)
                    return '<img src="'. url("storage/personas/fotos/" . $foto_principal->foto) . '" alt="" width="30" class="">';

                return '<img src="'. url("storage/users/avatar.png") . '" alt="" width="30" class="">';
            });

        $this->crud->addColumn([
            'label'         => 'Nombres',
            'type'          => 'select',
            'name'          => 'perfil_persona_id',
            'entity'        => 'persona',
            'attribute'    => 'nombres',
            'model'         => "App\Models\PerfilPersona"
        ]);

        $this->crud->addColumn([
            'label'         => 'Apellidos',
            'type'          => 'select',
            'name'          => 'perfil_persona_id',
            'key'           => 'persona_apellidos',
            'entity'        => 'persona',
            'attribute'     => 'apellidos',
            'model'         => "App\Models\PerfilPersona"
        ]);

        if (!isset(backpack_auth()->user()->centro_id)) {
            $this->crud->addColumn([
                'label'         => 'Centro',
                'type'          => 'model_function',
                'name'          => 'centro',
                'function_name' => 'getCentroActual',
            ]);
        }

        $this->crud->addColumn([
            'label'         => 'Pabellón',
            'type'          => 'model_function',
            'name'          => 'pabellon',
            'function_name' => 'getPabellonActual',
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PerfilPrivadoRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        
        $this->crud->addField([
            'name'          => 'perfil_persona_id',
            'type'          => 'select2_from_ajax',
            'label'         => "Persona",
            'entity'        => 'persona',
            'attribute'     => 'nombre',
            'data_source'   => url('api/persona'),
            'placeholder'   => 'Seleccione una persona',
            'minimum_input_length' => 2,
            'include_all_form_fields' => true
        ]);



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
