<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SolicitudRegistroRequest;
use App\Models\Pais;
use App\Models\SolicitudRegistro;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Mike4ip\ChatApi;

// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Lang;

/**
 * Class SolicitudRegistroCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SolicitudRegistroCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\SolicitudRegistro');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/solicitud-registro');
        $this->crud->setEntityNameStrings('solicitud registro', 'solicitudes registros');

        if (!$this->crud->has('order')) {
            $this->crud->orderBy('estado_solicitud_registro_id', 'asc')->orderBy('created_at', 'desc'); 
        }

        if (isset(backpack_auth()->user()->centro_id)) {
            $this->crud->addClause('where', 'solicitante_centro_id', '=', backpack_auth()->user()->centro_id);
        }

        $this->crud->addButtonFromView('line', 'notify', 'notify', 'beginning');

    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

        if (!isset(backpack_auth()->user()->centro_id)) {
            CRUD::column('solicitante_centro_id')
                ->type('select')
                ->label('Centro')
                ->entity('centro')
                ->atrtribute('nombre')
                ->model("App\Models\Centro");
        }

        CRUD::column('visitante_nombres')
            ->type('text')
            ->label('Nombres');

        CRUD::column('visitante_apellidos')
            ->type('text')
            ->label('Apellidos');

        CRUD::column('visitante_telefono')
            ->type('text')
            ->label('Whatsapp');

        // $this->crud->addColumn([
        //     'name'          => 'visitante_telefono',
        //     'type'          => 'closure',
        //     'label'         => 'Whatsapp',
        //     'function'      => function($entry) {
        //         $whatsapp = substr($entry->visitante_telefono, 1);
        //         $mensaje = __('mensajes_notificaciones.whatsapp.solicitud_registro', [
        //                         'visitante' => $entry->visitante_nombres . ' ' . $entry->visitante_apellidos,
        //                         'solicitante' => $entry->solicitante_nombres . ' ' . $entry->solicitante_apellidos,
        //                         'centro' => $entry->centro->nombre,
        //                         'urlregistro' => 'https://agenda.ministeriodejusticia.gov.py/register'
        //                     ]);
        //         return '<a href="whatsapp://send?phone=595' . $whatsapp . '&text=' . urlencode($mensaje) . '">' . $entry->visitante_telefono . '</a>';
        //     }
        // ]);

        $this->crud->addColumn([
            'name'          => 'estado_solicitud_registro_id',
            'type'          => 'select',
            'label'         => 'Estado',
            'entity'        => 'estado_solicitud',
            'atrtribute'    => 'nombre',
            'model'         => "App\Models\EstadoSolicitudRegistro",
            'wrapper'       => [
                'element'   => 'span',
                'class'     => 'estado'
            ]
        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SolicitudRegistroRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

        if (isset(backpack_auth()->user()->centro_id)) {
            CRUD::field('solicitante_centro_id')
                ->type('hidden')
                ->value(backpack_auth()->user()->centro_id);
        } else {
            
            $this->crud->addField([
                'label'     => "Centro",
                'type'      => 'select2',
                'name'      => 'solicitante_centro_id',
                'entity'    => 'centro',
                'attribute' => 'nombre',
                'model'     => "App\Models\Centro",
            ]);
        }

                    
        $this->crud->addField([
            'label'         => "Pabellon",
            'type'          => 'select2_from_ajax',
            'name'          => 'solicitante_pabellon_id',
            'entity'        => 'pabellon',
            'attribute'     => 'nombre',
            'data_source'   => url('api/pabellon'),
            'placeholder'   => 'Seleccione un pabellón',
            'minimum_input_length' => 0,
            'dependencies' => 'solicitante_centro_id',
            'include_all_form_fields' => true
        ]);

        $this->crud->addField([
            'label'         => "Solicitante",
            'type'          => 'select2_from_ajax',
            'name'          => 'perfil_privado_id',
            'entity'        => 'perfil_privado',
            'attribute'     => 'nombre',
            'data_source'   => url('api/perfil-privado'),
            'placeholder'   => 'Seleccione una persona',
            'minimum_input_length' => 2,
            'dependencies' => 'solicitante_centro_id',
            'include_all_form_fields' => true
        ]);

        CRUD::field('solicitante_nombres')
            ->type('text')
            ->label('Nombres')
            ->wrapper([ 
                'class' => 'form-group col-md-6'
             ]);
            
            
        CRUD::field('solicitante_apellidos')
            ->type('text')
            ->label('Apellidos')
            ->wrapper([ 
                'class' => 'form-group col-md-6'
             ]);

        $this->crud->addField([
            'label'     => "Nacionalidad",
            'type'      => 'select2',
            'name'      => 'solicitante_nacionalidad_id',
            'entity'    => 'nacionalidad',
            'default'   => '181',
            'attribute' => 'gentilicio',
            'model'     => "App\Models\Pais",
        ]);

        $this->crud->addField([
            'label'     => "Tipo de Identidad",
            'type'      => 'select2',
            'name'      => 'solicitante_tipo_identidad_id',
            'entity'    => 'tipo_identidad',
            'attribute' => 'nombre',
            'model'     => "App\Models\IdentidadTipo",
            'wrapper'   => [ 
                'class'      => 'form-group col-md-6'
             ],
        ]);

        CRUD::field('solicitante_documento')
                ->type('text')
                ->label('Documento de Identidad')
                ->wrapper([ 
                    'class' => 'form-group col-md-6'
                 ]);

        CRUD::field('separator')
            ->type('custom_html')
            ->value('<h4 class="mt-2">Datos del Visitante</h4><hr class="mb-0">');

        CRUD::field('visitante_nombres')
            ->type('text')
            ->label('Nombres')
            ->wrapper([ 
                'class' => 'form-group col-md-6'
             ]);

        CRUD::field('visitante_apellidos')
            ->type('text')
            ->label('Apellidos')
            ->wrapper([ 
                'class' => 'form-group col-md-6'
             ]);

                
        $this->crud->addField([
            'label'     => "Pais",
            'type'      => 'select2',
            'name'      => 'pais_id',
            'entity'    => 'pais',
            'attribute' => 'nombre',
            'default'   => '181',
            'model'     => "App\Models\Pais",
            'wrapper'   => [ 
                'class'      => 'form-group col-md-12'
             ],
        ]);

        $this->crud->addField([
            'name'          => 'visitante_telefono',
            'type'          => 'password',
            'label'         => 'Whatsapp <small>(09xxxxxxxx - sin espacios)</small>',
        ]);

        
        $this->crud->addField([
            'name'          => 'visitante_telefono_confirmation',
            'type'          => 'password',
            'label'         => 'Confirmación Whatsapp',
        ]);

        CRUD::field('enlace')
            ->type('hidden')
            ->value(Uuid::uuid4());
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function notify($id)
    {
        $solicitud = SolicitudRegistro::find($id);

        if ($solicitud) {

            $codigo = Pais::where('id', $solicitud->pais_id)->first();
            $whatsapp = $codigo->phone_code . $solicitud->visitante_telefono;

            $mensaje = __('mensajes_notificaciones.whatsapp.solicitud_registro', [
                            'visitante' => $solicitud->visitante_nombres . ' ' . $solicitud->visitante_apellidos,
                            'solicitante' => $solicitud->solicitante_nombres . ' ' . $solicitud->solicitante_apellidos,
                            'centro' => $solicitud->centro->nombre,
                            'urlregistro' => 'http://agenda.web.py/register?res=' . $solicitud->enlace 
                        ]);
            
        

            $api = new ChatApi(env('CHAT_API_TOKEN'), env('CHAT_API_URL'));
            if ($api->sendPhoneMessage('+' . $whatsapp, $mensaje) == true) {
                $solicitud->estado_solicitud_registro_id = 2;
                $solicitud->save();
                return true;
            } else {
                return false;
            }
        }

        return $solicitud;

    }
}
