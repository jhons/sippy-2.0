<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CentroPabellon;
use Illuminate\Http\Request;

class CentroPabellonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = CentroPabellon::query();

        if (isset($form['solicitante_centro_id'])){
            $form['centro'] = $form['solicitante_centro_id'];
        }
        
        // if no category has been selected, show no options
        if (! $form['centro']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['centro']) {
            $options = $options->where('centro_id', $form['centro']);
        }

        if ($search_term) {
            $results = $options->where('nombre', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return CentroPabellon::find($id);
    }

}
