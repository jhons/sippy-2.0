<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GradoAcademico;
use Illuminate\Http\Request;

class GradoAcademicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = GradoAcademico::query();

        // if no category has been selected, show no options
        if (! $form['nivel_academico_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['nivel_academico_id']) {
            $options = $options->where('nivel_academico_id', $form['nivel_academico_id']);
        }

        if ($search_term) {
            $results = $options->where('nombre', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return GradoAcademico::find($id);
    }

}
