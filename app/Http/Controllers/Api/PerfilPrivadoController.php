<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FicharRequest;
use App\Models\CentroPrivado;
use App\Models\PerfilIdentidad;
use App\Models\PerfilPersona;
use App\Models\PerfilPrivado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PerfilPrivadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = DB::table('perfiles_personas')
                    ->select(DB::raw('perfiles_privados.id as id, CONCAT(perfiles_personas.nombres, \' \', perfiles_personas.apellidos) as nombre, perfiles_personas.nombres'))
                    ->leftJoin('perfiles_privados', 'perfiles_privados.perfil_persona_id', '=', 'perfiles_personas.id')
                    ->leftJoin('centros_privados', 'centros_privados.perfil_privado_id', '=', 'perfiles_privados.id');

        // $options = EstablecimientoPabellon::query();

        if (isset($form['solicitante_centro_id'])){
            $form['centro'] = $form['solicitante_centro_id'];
        }
        
        // if no category has been selected, show no options
        if (! $form['centro']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['centro']) {
            $options = $options->where('centros_privados.centro_id', $form['centro']);
        }

        if ($search_term) {
            $results = $options->where('centros_privados.fecha_ingreso', '<=', 'now()')
                                ->where(DB::raw('UPPER(perfiles_personas.nombres::text)'), 'LIKE', DB::raw("UPPER('%" . $search_term . "%')"))
                                ->orWhere(DB::raw('UPPER(perfiles_personas.apellidos::text)'), 'LIKE', DB::raw("UPPER('%" . $search_term . "%')"))
                                // ->toSql();
                                ->paginate(10);
                                
            // dd($results);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = DB::table('perfiles_privados')
                    ->select(DB::raw('perfiles_privados.id, CONCAT(perfiles_personas.nombres, \' \', perfiles_personas.apellidos) as nombre'))
                    ->leftJoin('perfiles_personas', 'perfiles_personas.id', '=', 'perfiles_privados.perfil_persona_id')
                    ->where('perfiles_privados.id', $id)->get();

        return $result;

    }

    public function store(FicharRequest $request)
    {
        $persona = PerfilPersona::create([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'genero_id' => $request->genero_id
        ]);

        $documento = PerfilIdentidad::create([
            'perfil_persona_id' => $persona->id,
            'identidad_tipo_id' => $request->identidad_tipo_id,
            'documento' => $request->documento,
            'pais_emisor_id' => $request->nacionalidad_id
        ]);

        $privado = PerfilPrivado::create([
            'perfil_persona_id' => $persona->id,
            'fecha' => $request->fecha_ingreso
        ]);

        $centro = CentroPrivado::create([
            'perfil_privado_id' => $privado->id,
            'fecha_ingreso' => $request->fecha_ingreso,
            'centro_id' => (backpack_auth()->user()->centro_id) ?? $request->centro_id,
            'pabellon_id' => $request->pabellon_id
        ]);

        $privado->response_type = 'create';

        return $privado;
    }
}
