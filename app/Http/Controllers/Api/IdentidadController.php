<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilIdentidadRequest;
use App\Models\PerfilIdentidad;

class IdentidadController extends Controller
{

    public function store(PerfilIdentidadRequest $request)
    {
        $identidad = PerfilIdentidad::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'identidad_tipo_id' => $request->identidad_tipo_id,
            'documento' => $request->documento,
            'pais_emisor_id' => $request->pais_emisor_id,
            'validez' => $request->validez
        ]);

        $identidad->identidad_tipo_id = $identidad->identidad_tipo->nombre;
        $identidad->pais_emisor_id = $identidad->pais_emisor->nombre;
        $identidad->response_type = 'create';

        return $identidad;
    }

    public function show($id)
    {
        $identidad = PerfilIdentidad::where('id', $id)->get();
        return $identidad;
    }

    public function update(PerfilIdentidadRequest $request, $id)
    {
        $identidad = PerfilIdentidad::find($id);
        $identidad->identidad_tipo_id = $request->identidad_tipo_id;
        $identidad->documento = $request->documento;
        $identidad->pais_emisor_id = $request->pais_emisor_id;
        $identidad->validez = $request->validez;
        $identidad->save();

        $identidad->identidad_tipo_id = $identidad->identidad_tipo->nombre;
        $identidad->pais_emisor_id = $identidad->pais_emisor->nombre;
        $identidad->response_type = 'edit';
        return $identidad;
    }

    public function destroy(Request $request)
    {
        $identidad = PerfilIdentidad::find($request->id);
        $identidad->delete();
        return true;
    }
}
