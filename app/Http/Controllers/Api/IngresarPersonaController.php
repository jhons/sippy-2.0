<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\IngresarPersonaRequest;
use App\Models\Centro;
use App\Models\CentroPrivado;
use App\Models\PerfilPersona;
use App\Models\PerfilPrivado;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class IngresarPersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $message = ['success' => false, 'message' => '<p class="alert alert-danger"><i class="la la-times-circle la-lg"></i> Debe ingresar algún parámetro para la búsqueda</p>'];

        if ($request['nombres'] != '' || $request['apellidos'] != '' || $request['documento'] != '') {

            $personas = PerfilPersona::whereNotNull('nombres');

            if ($request['nombres'] != ''){
                $personas->whereRaw('UPPER(nombres) like UPPER(?)',["%{$request['nombres']}%"]);
            }

            if ($request['apellidos'] != '') {
                $personas->whereRaw('UPPER(apellidos) like UPPER(?)',["%{$request['apellidos']}%"]);
            }

            if ($request['documento'] != ''){
                $documento = $request['documento'];
                $personas->whereHas('documentos', function (Builder $query) use ($documento) {
                    $query->whereRaw('UPPER(documento) like UPPER(?)',["%{$documento}%"]);
                });
            }

            $personas->with('identidad_principal')->with('privado');
            $resultados = $personas->get();
   
            $info = [];
            if ($resultados->count() > 0) {
                foreach ($resultados as $persona)
                {
                    $id = $persona->id;
                    $accion = 'fichar';
                    $tipo = 'perfil_persona_id';
                    $nombres = $persona->nombres;
                    $apellidos = $persona->apellidos;
    
                    $foto_principal = $persona->foto_principal;
                    $foto = ($foto_principal) ? url("storage/personas/fotos/" . $foto_principal->foto) : url("storage/users/avatar.png");
                    $documento = ($persona->identidad_principal) ? $persona->identidad_principal->documento : '<em>Sin documento en el sistema</em>';
                    $estado = 'Sin ficha en el sistema';
                    $centro = false;
                    $color = 'success';
    
                    if ($persona->privado) {
                        $id = $persona->privado->id;
                        $accion = 'ingresar';
                        $tipo = 'perfil_privado_id';
                        $estado = 'Posee ficha!';
                        $color = 'warning';
                        if ($persona->privado->centro_privado_actual){
                            $centro_id = $persona->privado->centro_privado_actual->centro_id;
                            if ($centro_id == backpack_auth()->user()->centro_id){
                                $accion = 'ver';
                                $estado = 'Recluido/a en este centro';
                            } else {
                                $estado =  'Recluido/a en ' . $persona->privado->centro_privado_actual->centro->nombre;
                                $accion = 'transferir';
                                $centro = $persona->privado->centro_privado_actual->centro_id;
                            }
                            $color = 'danger';
                        }
                    }
                    
                    $info[] = ['id' => $id, 'accion' => $accion, 'tipo' => $tipo, 'nombres' => $nombres, 'apellidos' => $apellidos, 'foto' => $foto, 'documento' => $documento, 'estado' => $estado, 'centro' => $centro, 'color' => $color];
                }
                $message = ['success' => true, 'message' => $info];
            } else {
                $message = ['success' => false, 'message' => '<p class="alert alert-warning"><i class="la la-exclamation-circle la-lg"></i> No se han obtenido resultados para la búsqueda</p>'];
            }

        }

        return $message;
    }

    public function ingresar(IngresarPersonaRequest $request)
    {
        // dd($request);
        switch ($request->accion) {
            case "fichar":

                $request->centro_id = (backpack_auth()->user()->centro_id) ?? $request->centro_id;

                $privado = PerfilPrivado::create([
                    'perfil_persona_id' => $request->perfil_persona_id,
                    'fecha' => $request->fecha_ingreso,
                    'observacion' => $request->mensaje
                ]);

                $centro = CentroPrivado::create([
                    'perfil_privado_id' => $privado->id,
                    'fecha_ingreso' => $request->fecha_ingreso,
                    'centro_id' => $request->centro_id,
                    'pabellon_id' => $request->pabellon_id,
                    'observacion' => $request->mensaje
                ]);

                $response = $centro;
                break;

            case "ingresar":

                $centro = CentroPrivado::create([
                    'perfil_privado_id' => $request->perfil_privado_id,
                    'fecha_ingreso' => $request->fecha_ingreso,
                    'centro_id' => (backpack_auth()->user()->centro_id) ?? $request->centro_id,
                    'pabellon_id' => $request->pabellon_id,
                    'observacion' => $request->mensaje
                ]);

                $response = $centro;
                break;

            case "transferir":
                // TODO: Enviar notificacion a la penitenciaria donde aparece como recluido el ppl

                $aQuien = PerfilPrivado::find($request->perfil_privado_id)->persona;

                
                $dondeQuiere = Centro::find(backpack_auth()->user()->centro_id);
                $dondeEsta = $request->centro;
                $mensaje = $request->mensaje;

                $notificacion = [
                    "title" => "Traslado requerido",
                    "description"  => "El centro de " .$dondeQuiere->nombre . " solicita que se genere el traslado de " . $aQuien->nombres . " " . $aQuien->apellidos . ", para efectuar su ingreso en dicha institución",
                    "type" => "traslado_requerido",
                    "message" => $mensaje,
                    "id" => $request->perfil_privado_id
                ];

                dd($notificacion);
                $response = '';
                break;
        }

        return $response;
        
    }
}
