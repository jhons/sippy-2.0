<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExpedienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expedientes = [
                        1234 => [   'causa' => [
                                        [
                                            'numero' => 1234,
                                            'anio' => 2019
                                        ]
                                    ],  
                                    'expedientes' =>   [
                                                        [
                                                            'id'    => 3,
                                                            'fecha' => '12/03/2020',
                                                            'titulo' => 'Titulo de la actuación 3',
                                                            'tipo' => 'Tipo Actuación'
                                                        ],
                                                        [
                                                            'id'    => 2,
                                                            'fecha' => '23/02/2020',
                                                            'titulo' => 'Titulo de la actuación 2',
                                                            'tipo' => 'Tipo Actuación'
                                                        ],
                                                        [
                                                            'id'    => 1,
                                                            'fecha' => '19/01/2020',
                                                            'titulo' => 'Titulo de la actuación 1',
                                                            'tipo' => 'Tipo Actuación'
                                                        ]
                                                    ],
                                    'causa_relacionada' => [
                                        [
                                            'causa' => 1041,
                                            'anio' => 2019
                                        ]
                                    ],
                                    'relacionados' => [
                                        [
                                            'perfil_privado_id' => 2,
                                            'nombres' => 'Juan Alberto',
                                            'apellidos' => 'Perez Estevez',
                                            'foto' => asset('storage/personas/fotos') . '/' . 'foto.png'

                                        ],
                                        [
                                            'perfil_privado_id' => 4,
                                            'nombres' => 'Luis Humberto',
                                            'apellidos' => 'Brítez Vazquez',
                                            'foto' => asset('storage/personas/fotos') . '/' . 'foto.png'

                                        ]
                                    ]
                                ],
                                
                        1041 => [   'causa' => [
                                        [
                                            'numero' => 1041,
                                            'anio' => 2019
                                        ]
                                    ],  
                                    'expedientes' =>   [
                                                        [
                                                            'id'    => 3,
                                                            'fecha' => '05/01/2020',
                                                            'titulo' => 'Titulo de la actuación 3',
                                                            'tipo' => 'Tipo Actuación'
                                                        ],
                                                        [
                                                            'id'    => 2,
                                                            'fecha' => '18/12/2019',
                                                            'titulo' => 'Titulo de la actuación 2',
                                                            'tipo' => 'Tipo Actuación'
                                                        ],
                                                        [
                                                            'id'    => 1,
                                                            'fecha' => '23/11/2019',
                                                            'titulo' => 'Titulo de la actuación 1',
                                                            'tipo' => 'Tipo Actuación'
                                                        ]
                                    ],
                                    'causa_relacionada' => [],
                                    'relacionados' => []
                                ],
                                948 => [   'causa' => [
                                    [
                                        'numero' => 948,
                                        'anio' => 2019
                                    ]
                                ],  
                                'expedientes' =>   [
                                                    [
                                                        'id'    => 12,
                                                        'fecha' => '12/02/2019',
                                                        'titulo' => 'Titulo de la actuación 12',
                                                        'tipo' => 'Tipo Actuación'
                                                    ],

                                ],
                                'causa_relacionada' => [],
                                'relacionados' => []
                    ]
                ];

                return $expedientes[$id];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
