<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilProfesionRequest;
use App\Models\PerfilProfesion;

class ProfesionController extends Controller
{

    public function store(PerfilProfesionRequest $request)
    {
        $profesion = PerfilProfesion::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'fecha' => $request->fecha,
            'otro' => $request->otro,
            'profesion_id' => $request->profesion_id
        ]);

        $profesion->profesion_id = $profesion->profesion->nombre;
        $profesion->response_type = 'create';

        return $profesion;
    }

    public function show($id)
    {
        $profesion = PerfilProfesion::where('id', $id)->get();
        return $profesion;
    }

    public function update(PerfilProfesionRequest $request, $id)
    {
        $profesion = PerfilProfesion::find($id);
        $profesion->fecha = $request->fecha;
        $profesion->otro = $request->otro;
        $profesion->profesion_id = $request->profesion_id;
        $profesion->save();

        $profesion->profesion_id = $profesion->profesion->nombre;
        $profesion->response_type = 'edit';
        return $profesion;
    }

    public function destroy(Request $request)
    {
        $profesion = PerfilProfesion::find($request->id);
        $profesion->delete();
        return true;
    }
}
