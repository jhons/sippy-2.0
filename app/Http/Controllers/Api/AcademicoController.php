<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilAcademicoRequest;
use App\Models\PerfilAcademico;

class AcademicoController extends Controller
{

    public function store(PerfilAcademicoRequest $request)
    {
        $academico = PerfilAcademico::create([
            'institucion_id' => $request->institucion_id,
            'otro' => $request->otro,
            'nivel_academico_id' => $request->nivel_academico_id,
            'grado_academico_id' => $request->grado_academico_id,
            'anio' => $request->anio,
            'fecha_promocion' => $request->fecha_promocion,
            'perfil_persona_id' => $request->perfil_persona_id,
        ]);

        $academico->nivel_academico_nombre = ($academico->nivel_academico_id) ? $academico->nivel_academico->nombre : '';
        $academico->grado_academico_nombre = ($academico->grado_academico_id) ? $academico->grado_academico->nombre : '';
        $academico->response_type = 'create';

        return $academico;
    }

    public function show($id)
    {
        $academico = PerfilAcademico::where('id', $id)->with(['institucion', 'nivel_academico', 'grado_academico'])->get();
        return $academico;
    }

    public function update(PerfilAcademicoRequest $request, $id)
    {
        $academico = PerfilAcademico::find($id);
        $academico->institucion_id = $request->institucion_id;
        $academico->otro = $request->otro;
        $academico->nivel_academico_id = $request->nivel_academico_id;
        $academico->grado_academico_id = $request->grado_academico_id;
        $academico->anio = $request->anio;
        $academico->fecha_promocion = $request->fecha_promocion;
        $academico->save();

        $academico->nivel_academico_nombre = ($academico->nivel_academico_id) ? $academico->nivel_academico->nombre : '';
        $academico->grado_academico_nombre = ($academico->grado_academico_id) ? $academico->grado_academico->nombre : '';
        $academico->response_type = 'edit';
        return $academico;
    }

    public function destroy(Request $request)
    {
        $academico = PerfilAcademico::find($request->id);
        $academico->delete();
        return true;
    }
}
