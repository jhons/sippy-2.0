<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pais;
use Illuminate\Http\Request;
use Mike4ip\ChatApi;

class ChatBotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $pais = Pais::find($request->pais_id);
        $whatsapp = $pais->phone_code . $request->destinatario_telefono;

        $mensaje = '
        Sr(a). ' . $request->destinatario_nombres . ' ' . $request->destinatario_apellidos . '.\n
        La persona ' . $request->solicitante_nombres . ' ' . $request->solicitante_apellidos . ' recluida en ' . $request->centro . ' desea que usted se inscriba al sistema solicitud de visitas penitenciarias.\n
        Para registrarse debe ingresar a http://agenda.web.py/register?res=' . $request->url . ' y completar todos los datos.\n
        Una vez que se haya registrado podrá agendar una visita a ' . $request->solicitante_nombres . ' ' . $request->solicitante_apellidos . '.\n
        \n
        Muchas gracias,\n
        Ministerio de Justicia
        ';

        $api = new ChatApi(env('CHAT_API_TOKEN'), env('CHAT_API_URL'));
        return ($api->sendPhoneMessage('+' . $whatsapp, $mensaje) == true) ? 'Mensaje enviado' : 'Fallo';

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
