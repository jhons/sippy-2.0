<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HuellaDactilar;
use Illuminate\Http\Request;

class PerfilHuellaController extends Controller
{
    public function store(Request $request)
    {
        $huella = HuellaDactilar::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'captura' => $request->captura,
            'fecha' => 'now()',
            'huella_tipo_id' => $request->huella_tipo_id
        ]);

        $huella->response_type = 'create';

        return $huella;
    }
}