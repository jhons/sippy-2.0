<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilResidenciaRequest;
use App\Models\PerfilResidencia;

class DireccionController extends Controller
{

    public function store(PerfilResidenciaRequest $request)
    {
        $direccion = PerfilResidencia::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'direccion' => $request->direccion,
            'barrio' => $request->barrio,
            'ciudad' => $request->ciudad,
            'pais_id' => $request->pais_id,
            'departamento_id' => $request->departamento_id,
            'ciudad_id' => $request->ciudad_id,
            'principal' => $request->principal
        ]);

        $direccion->pais_id = $direccion->pais->nombre;
        $direccion->departamento_id = ($direccion->departamento_id) ? $direccion->departamento->nombre : '-';
        $direccion->ciudad_id = ($direccion->ciudad_id) ? $direccion->laciudad->nombre : '-';
        $direccion->response_type = 'create';

        return $direccion;
    }

    public function show($id)
    {
        $direccion = PerfilResidencia::where('id', $id)->with(['departamento', 'laciudad'])->get();

        return $direccion;
    }

    public function update(PerfilResidenciaRequest $request, $id)
    {
        $direccion = PerfilResidencia::find($id);
        $direccion->direccion = $request->direccion;
        $direccion->barrio = $request->barrio;
        $direccion->ciudad = $request->ciudad;
        $direccion->pais_id = $request->pais_id;
        $direccion->departamento_id = $request->departamento_id;
        $direccion->ciudad_id = $request->ciudad_id;
        $direccion->principal = $request->principal;
        $direccion->save();

        $direccion->pais_id = $direccion->pais->nombre;
        $direccion->departamento_id = ($direccion->departamento_id) ? $direccion->departamento->nombre : '-';
        $direccion->ciudad_id = ($direccion->ciudad_id) ? $direccion->laciudad->nombre : '-';
        $direccion->response_type = 'edit';
        return $direccion;
    }

    public function destroy(Request $request)
    {
        $direccion = PerfilResidencia::find($request->id);
        $direccion->delete();
        return true;
    }
}
