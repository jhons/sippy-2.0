<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Ciudad::query();

        // if no category has been selected, show no options
        if (! $form['departamento_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['departamento_id']) {
            $options = $options->where('departamento_id', $form['departamento_id']);
        }

        if ($search_term) {
            $results = $options->where('nombre', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Ciudad::find($id);
    }

}
