<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PerfilPersonaRequest;
use App\Models\PerfilPersona;
use Carbon\Carbon;

class PerfilPersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = DB::table('perfiles_personas')
                    ->select(
                        DB::raw('perfiles_personas.id,
                                CONCAT(perfiles_personas.nombres, \' \', perfiles_personas.apellidos, \' (\',
                                (SELECT perfiles_identidades.documento FROM perfiles_identidades
                                WHERE perfiles_identidades.perfil_persona_id = perfiles_personas.id
                                ORDER BY perfiles_identidades.documento_tipo_id, created_at DESC LIMIT 1), \')\') as nombre
                        ')
                    )
                    ->leftJoin('perfiles_identidades', 'perfiles_identidades.perfil_persona_id', '=', 'perfiles_personas.id');


        if ($search_term) {
            $results = $options->where(DB::raw('UPPER(perfiles_personas.nombres::text)'), 'LIKE', DB::raw("UPPER('%" . $search_term . "%')"))
                                ->orWhere(DB::raw('UPPER(perfiles_personas.apellidos::text)'), 'LIKE', DB::raw("UPPER('%" . $search_term . "%')"))
                                ->orWhere(DB::raw('perfiles_identidades.documento::text'), 'LIKE', DB::raw("'%" . $search_term . "%'"))
                                ->groupBy('nombre')
                                ->groupBy('perfiles_personas.id')
                                // ->toSql();
                                ->paginate(10);
                                
            // dd($results);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = DB::table('perfiles_privados')
                    ->select(DB::raw('perfiles_privados.id, CONCAT(perfiles_personas.nombres, \' \', perfiles_personas.apellidos) as nombre'))
                    ->leftJoin('perfiles_personas', 'perfiles_personas.id', '=', 'perfiles_privados.perfil_persona_id')
                    ->where('perfiles_privados.id', $id)->get();

        return $result;

    }

    public function update(PerfilPersonaRequest $request)
    {
        $persona = PerfilPersona::findOrFail($request->perfil_id);
        $persona->nombres = $request->nombres;
        $persona->apellidos = $request->apellidos;
        $persona->alias = $request->alias;
        $persona->apodo = $request->apodo;
        $persona->fecha_nacimiento = $request->fecha_nacimiento;
        $persona->genero_id = $request->genero_id;
        $persona->estado_civil_id = $request->estado_civil_id;
        $persona->etnia_id = $request->etnia_id;
        $persona->save();

        $persona->genero_id = ($persona->genero_id) ? $persona->genero->nombre : '-';
        $persona->estado_civil_id = ($persona->estado_civil_id) ? $persona->estado_civil->nombre : '-';
        $persona->etnia_id = ($persona->etnia_id) ? $persona->etnia->nombre : '-';

        $persona->fecha_nacimiento_texto = $persona->fecha_nacimiento ? Carbon::parse($persona->fecha_nacimiento)
                    ->locale(App()->getLocale())
                    ->isoFormat(config('backpack.base.default_date_format')) : '-';
        return $persona;
        
    }


}
