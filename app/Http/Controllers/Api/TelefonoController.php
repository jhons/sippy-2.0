<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilTelefonoRequest;
use App\Models\PerfilTelefono;

class TelefonoController extends Controller
{

    public function store(PerfilTelefonoRequest $request)
    {
        $telefono = PerfilTelefono::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'telefono' => $request->telefono,
            'telefono_tipo_id' => $request->telefono_tipo_id
        ]);

        $telefono->telefono_tipo_id = $telefono->telefono_tipo->nombre;
        $telefono->response_type = 'create';

        return $telefono;
    }

    public function show($id)
    {
        $telefono = PerfilTelefono::where('id', $id)->get();

        return $telefono;
    }

    public function update(PerfilTelefonoRequest $request, $id)
    {
        $telefono = PerfilTelefono::find($id);
        $telefono->telefono = $request->telefono;
        $telefono->telefono_tipo_id = $request->telefono_tipo_id;
        $telefono->save();

        $telefono->telefono_tipo_id = $telefono->telefono_tipo->nombre;
        $telefono->response_type = 'edit';
        return $telefono;
    }

    public function destroy(Request $request)
    {
        $telefono = PerfilTelefono::find($request->id);
        $telefono->delete();
        return true;
    }
}
