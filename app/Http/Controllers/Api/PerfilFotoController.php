<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PerfilFoto;

class PerfilFotoController extends Controller
{
    public function store(Request $request)
    {
        $foto = PerfilFoto::create([
            'perfil_persona_id' => $request->perfil_persona_id,
            'foto' => $request->foto,
            'principal' => $request->principal
        ]);

        $foto->response_type = 'create';

        return $foto;
    }
}