<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PerfilCriminalRequest;
use App\Models\PerfilCriminal;
use App\Models\PerfilPersona;

class CriminalController extends Controller
{

    public function store(PerfilCriminalRequest $request)
    {
        $criminal = PerfilCriminal::create([
            'grupo_criminal_id' => $request->grupo_criminal_id,
            'nivel_peligrosidad_id' => $request->nivel_peligrosidad_id,
            'perfil_persona_id' => $request->perfil_persona_id,
            'observaciones' => $request->observaciones
        ]);

        $perfil = PerfilPersona::find($request->perfil_persona_id);
        $perfil->grupo_vulnerable_id = $request->grupo_vulnerable_id;
        $perfil->save();
        

        $criminal->grupo_vulnerable = ($perfil->grupo_vulnerable_id) ? $perfil->grupo_vulnerable->nombre : '';
        $criminal->grupo_vulnerable_id = $perfil->grupo_vulnerable_id;
        $criminal->grupo_criminal_nombre = ($criminal->grupo_criminal_id) ? $criminal->grupo_criminal->nombre : '';
        $criminal->nivel_peligrosidad_nombre = ($criminal->nivel_peligrosidad_id) ? $criminal->nivel_peligrosidad->nombre : '';
        
        $criminal->response_type = 'create';

        return $criminal;
    }

    public function show($id)
    {
        $criminal = PerfilCriminal::where('id', $id)->with(['nivel_peligrosidad', 'grupo_criminal'])->get();
        return $criminal;
    }

    public function update(PerfilCriminalRequest $request, $id)
    {
        $criminal = PerfilCriminal::find($id);
        $criminal->grupo_criminal_id = $request->grupo_criminal_id;
        $criminal->nivel_peligrosidad_id = $request->nivel_peligrosidad_id;
        $criminal->observaciones = $request->observaciones;
        $criminal->save();

        $criminal->grupo_criminal_nombre = ($criminal->grupo_criminal_id) ? $criminal->grupo_criminal->nombre : '';
        $criminal->nivel_peligrosidad_nombre = ($criminal->nivel_peligrosidad_id) ? $criminal->nivel_peligrosidad->nombre : '';
        $criminal->response_type = 'edit';
        return $criminal;
    }

    public function destroy(Request $request)
    {
        $criminal = PerfilCriminal::find($request->id);
        $criminal->delete();
        return true;
    }
}
