<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SolicitudRegistroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'solicitante_centro_id' => 'required',
            'solicitante_nombres' => 'required|min:3|max:255',
            'solicitante_apellidos' => 'required|min:3|max:255',
            'solicitante_documento' => 'required',
            'visitante_nombres' => 'required|min:3|max:255',
            'visitante_apellidos' => 'required|min:3|max:255',
            'visitante_telefono' => 'required|regex:/^09[0-9]{8}$/i|confirmed',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'solicitante_centro_id' => 'Centro',
            'solicitante_nombres' => 'Nombres del solicitinte',
            'solicitante_apellidos' => 'Apellidos del solicitinte',
            'solicitante_documento' => 'Documento de Identidad',
            'visitante_nombres' => 'Nombres del visitante',
            'visitante_apellidos' => 'Apellidos del visitante',
            'visitante_telefono' => 'Whatsapp',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'visitante_telefono.regex' => 'El formato del campo :attribute es inválido. Debe ser en el siguiente formato 0961234567 sin espacios'
        ];
    }
}
