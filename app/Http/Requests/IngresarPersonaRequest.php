<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class IngresarPersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'perfil_persona_id' => 'sometimes|required|unique:perfiles_privados',
            'fecha_ingreso' => 'sometimes|required|date',
            'centro_id' => (!backpack_auth()->user()->centro_id) ? 'required' : ''
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'perfil_persona_id' => 'persona',
            'fecha_ingreso' => 'fecha de ingreso',
            'centro_id' => 'centro',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
