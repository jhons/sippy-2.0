<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SolicitudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        // return backpack_auth()->check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = [
            181 => 'regex:/^9[0-9]{8}$/i|',
            33 => 'regex:/^[1-9]{2}9[0-9]{7}$/i|',
            999 => 'numeric|'
        ];
        
        $pais = $_REQUEST['pais_id'];
        if (!array_key_exists($pais, $regex))
            $pais = "999";

        return [
            'solicitante_centro_id' => 'required',
            'solicitante_nombres' => 'required|min:3|max:255',
            'solicitante_apellidos' => 'required|min:3|max:255',
            'solicitante_documento' => 'required',
            'visitante_nombres' => 'required|min:3|max:255',
            'visitante_apellidos' => 'required|min:3|max:255',
            'visitante_telefono' => 'required|' . $regex[$pais] . 'confirmed',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'solicitante_centro_id' => 'Centro',
            'solicitante_nombres' => 'Nombres del solicitinte',
            'solicitante_apellidos' => 'Apellidos del solicitinte',
            'solicitante_documento' => 'Documento de Identidad',
            'visitante_nombres' => 'Nombres del visitante',
            'visitante_apellidos' => 'Apellidos del visitante',
            'visitante_telefono' => 'Whatsapp',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $regex = [
            181 => 'Debe ser en el siguiente formato 961234567 sin espacios.',
            33 => 'Debe ser en el siguiente formato 55961234567 sin espacios.',
            999 => ''
        ];
        
        $pais = $_REQUEST['pais_id'];
        if (!array_key_exists($pais, $regex))
            $pais = "999";

        return [
            'visitante_telefono.regex' => 'El formato del campo :attribute es inválido. ' . $regex[$pais],
            'visitante_telefono.numeric' => 'El formato del campo :attribute es inválido. Debe ser números sin espacios.' 
        ];
    }
}
