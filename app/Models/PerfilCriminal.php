<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class PerfilCriminal extends Model
{
    use CrudTrait;
   /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'perfiles_criminales';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['grupo_criminal_id', 'nivel_peligrosidad_id', 'perfil_persona_id', 'observaciones'];
    protected $dates = ['created_at'];
    protected $casts = [
        'created_at'  => 'date',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function persona()
    {
        return $this->hasOne(PerfilPersona::class);
    }

    public function grupo_criminal()
    {
        return $this->belongsTo(GrupoCriminal::class);
    }

    public function nivel_peligrosidad()
    {
        return $this->belongsTo(NivelPeligrosidad::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
