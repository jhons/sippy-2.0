<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerfilIdentidad extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'perfiles_identidades';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['perfil_persona_id', 'identidad_tipo_id', 'documento', 'pais_emisor_id', 'validez', 'verificacion_tipo_id', 'motivo_rechazo'];
    protected $dates = ['validez'];
    protected $casts = [
        'validez'  => 'date:Y-m-d',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function persona()
    {
        return $this->hasOne(PerfilPersona::class);
    }

    public function identidad_tipo()
    {
        return $this->belongsTo(IdentidadTipo::class);
    }

    public function pais_emisor()
    {
        return $this->belongsTo(Pais::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
