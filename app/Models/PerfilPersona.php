<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class PerfilPersona extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'perfiles_personas';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['nombres', 'apellidos', 'apodo', 'alias', 'fecha_nacimiento', 'genero_id', 'estado_civil_id', 'etnia_id', 'grupo_vulnerable_id', 'usuario_id'];
    // protected $hidden = [];
    protected $dates = ['fecha_nacimiento'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function genero()
    {
        return $this->belongsTo(Genero::class);
    }

    public function estado_civil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }

    public function etnia()
    {
        return $this->belongsTo(Etnia::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function privado()
    {
        return $this->hasOne(PerfilPrivado::class, 'perfil_persona_id')->with('centros_privados');
    }

    public function fotos()
    {
        return $this->hasMany(PerfilFoto::class);
    }

    public function huellas_tipos()
    {
        return $this->hasMany(HuellaDactilar::class)->select('huella_tipo_id');
    }

    public function derecha_pulgar()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 1)->orderBy('created_at', 'desc')->limit(1);
    }

    public function derecha_indice()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 2)->orderBy('created_at', 'desc')->limit(1);
    }

    public function derecha_medio()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 3)->orderBy('created_at', 'desc')->limit(1);
    }

    public function derecha_anular()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 4)->orderBy('created_at', 'desc')->limit(1);
    }

    public function derecha_menique()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 5)->orderBy('created_at', 'desc')->limit(1);
    }

    public function derecha_palma()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 6)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_pulgar()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 7)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_indice()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 8)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_medio()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 9)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_anular()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 10)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_menique()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 11)->orderBy('created_at', 'desc')->limit(1);
    }

    public function izquierda_palma()
    {
        return $this->hasOne(HuellaDactilar::class)->where('huella_tipo_id', 12)->orderBy('created_at', 'desc')->limit(1);
    }

    public function foto_principal()
    {
        return $this->hasOne(PerfilFoto::class)->where('principal', 1)->orderBy('created_at', 'desc')->limit(1);
    }

    public function identidades()
    {
        return $this->hasMany(PerfilIdentidad::class)->orderBy('updated_at', 'desc');
    }

    public function direcciones()
    {
        return $this->hasMany(PerfilResidencia::class)->orderBy('updated_at', 'desc');
    }

    public function telefonos()
    {
        return $this->hasMany(PerfilTelefono::class)->orderBy('updated_at', 'desc');
    }

    public function identidad_principal()
    {
        return $this->hasOne(PerfilIdentidad::class)->orderBy('identidad_tipo_id')->orderBy('created_at', 'desc');
    }

    public function profesiones()
    {
        return $this->hasMany(PerfilProfesion::class)->orderBy('fecha', 'desc');
    }

    public function academicos()
    {
        return $this->hasMany(PerfilAcademico::class)->orderBy('anio', 'desc');
    }

    public function criminal()
    {
        return $this->hasMany(PerfilCriminal::class)->orderBy('created_at', 'desc');
    }

    public function grupo_vulnerable()
    {
        return $this->belongsTo(GrupoVulnerable::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getNombreCompletoAttribute()
    {
        return "{$this->nombres}  {$this->apellidos}";
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
