<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class PerfilPrivado extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'perfiles_privados';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['fecha', 'perfil_persona_id', 'observacion'];
    // protected $hidden = [];
    protected $dates = ['fecha'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    public function persona()
    {
        return $this->belongsTo(PerfilPersona::class, 'perfil_persona_id');
    }

    public function centros_privados()
    {
        return $this->hasMany(CentroPrivado::class, 'perfil_privado_id');
    }

    public function centro_privado_actual()
    {
        return $this->hasOne(CentroPrivado::class, 'perfil_privado_id')->whereNull('fecha_egreso')->orderBy('fecha_ingreso', 'desc')->with('centro');
    }

    public function incidentes()
    {
        return $this->belongsToMany(Incidente::class);
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getCentroActual()
    {
        $centro = $this->centro_privado_actual->centro;
        return $centro ? $centro->nombre : '-';
    }

    public function getPabellonActual()
    {
        $pabellon = $this->centro_privado_actual->pabellon;
        return $pabellon ? $pabellon->nombre : '-';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
