<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class AgendaVisita extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'agendas_visitas';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['visitante_id', 'visitado_id', 'visitado_nombres', 'visitado_apellidos', 'visita_tipo_id', 'motivo', 'centro_id', 'area_visita_id', 'fecha_visita', 'fecha_asistencia'];

    // protected $hidden = [];
    protected $dates = ['fecha_visita', 'fecha_asistencia'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function perfil_visitante()
    {
        return $this->hasOne(PerfilPersona::class, 'visitante_id');
    }

    public function perfil_visitado()
    {
        return $this->hasOne(PerfilPersona::class, 'visitado_id');
    }

    public function tipo_visita()
    {
        return $this->hasOne(VisitaTipo::class, 'visita_tipo_id');
    }

    public function centro()
    {
        return $this->hasOne(Centro::class, 'centro_id');
    }

    public function area_visita()
    {
        return $this->hasOne(AreaVisita::class, 'area_visita_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
