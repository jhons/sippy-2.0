<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class PerfilAcademico extends Model
{
    use CrudTrait;
   /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'perfiles_academicos';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['institucion_id', 'otro', 'nivel_academico_id', 'grado_academico_id', 'anio', 'fecha_promocion', 'perfil_persona_id'];
    protected $dates = ['fecha_promocion'];
    protected $casts = [
        'fecha_promocion'  => 'date:Y-m-d',
    ];
    // protected $hidden = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function persona()
    {
        return $this->hasOne(PerfilPersona::class);
    }

    public function institucion()
    {
        return $this->belongsTo(Institucion::class);
    }

    public function nivel_academico()
    {
        return $this->belongsTo(NivelAcademico::class);
    }

    public function grado_academico()
    {
        return $this->belongsTo(GradoAcademico::class);
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
