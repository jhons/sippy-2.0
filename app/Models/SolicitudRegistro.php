<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class SolicitudRegistro extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'solicitudes_registros';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['perfil_privado_id', 'solicitante_nombres', 'solicitante_apellidos', 'solicitante_nacionalidad_id', 'solicitante_tipo_documento_id', 'solicitante_documento', 'solicitante_centro_id', 'solicitante_pabellon_id', 'visitante_nombres', 'visitante_apellidos', 'pais_id', 'visitante_telefono', 'estado_solicitud_registro_id', 'enlace'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function estado_solicitud()
    {
        return $this->belongsTo(EstadoSolicitudRegistro::class, 'estado_solicitud_registro_id');
    }

    public function nacionalidad()
    {
        return $this->belongsTo(Pais::class, 'solicitante_nacionalidad_id');
    }

    // public function perfil_privado()
    // {
    //     return $this->belongsTo(PerfilPrivado::class, 'perfil_privado_id');
    // }

    public function tipo_identidad()
    {
        return $this->belongsTo(IdentidadTipo::class, 'solicitante_tipo_identidad_id');
    }

    public function centro()
    {
        return $this->belongsTo(Centro::class, 'solicitante_centro_id');
    }

    public function pabellon()
    {
        return $this->belongsTo(CentroPabellon::class, 'solicitante_pabellon_id');
    }

    public function perfil_privado()
    {
        return $this->belongsTo(PerfilPrivado::class, 'perfil_privado_id');
    }

    public function pais()
    {
        return $this->belongsTo(Pais::class, 'pais_id');
    }

    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
