<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CentroPrivado extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'centros_privados';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['fecha_ingreso', 'fecha_egreso', 'centro_id', 'pabellon_id', 'celda_id', 'perfil_privado_id', 'observacion'];
    // protected $hidden = [];
    protected $dates = ['fecha_ingreso', 'fecha_egreso'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function privado()
    {
        return $this->belongsToMany(PerfilPrivado::class)->with('persona');
    }

    public function centro()
    {
        return $this->belongsTo(Centro::class);
    }

    public function pabellon()
    {
        return $this->belongsTo(CentroPabellon::class);
    }

    public function celda()
    {
        return $this->hasOne(PabellonCelda::class, 'celda_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
