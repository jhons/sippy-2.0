<?php

namespace App\Providers;

use App\Models\SolicitudRegistro;
use App\Observers\SolicitudRegistroObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        SolicitudRegistro::observe(SolicitudRegistroObserver::class);
    }
}
