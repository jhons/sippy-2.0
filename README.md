## Sippy 2.0

Requisitos:

- PHP >= 7.2.5
- Extenciones PHP:
     - BCMath
     - Ctype
     - Fileinfo
     - JSON
     - Mbstring
     - OpenSSL
     - PDO
     - Tokenizer
     - XML
     - PGSQL
- Apache 2
- PosgreSql

## ‎Instalación del Proyecto

Primero debe descargar el proyecto a su maquina mediante una de las siguientes opciones:


*Opción 1*: **Clonar el proyecto mediante git**

Instalar **[Git](https://git-scm.com/downloads)** en el dispositivo y clonar el proyecto en la carpeta deseada mediante el siguiente comando:

*Mediante SSH*

```shell
git clone git@bitbucket.org:jhons/sippy-2.0.git
```

*Mediante HTTPS*

```shell
git clone https://bitbucket.org/jhons/sippy-2.0.git
```

*Opción 2*: **Descargar el proyecto comprimido**

```shell
https://bitbucket.org/jhons/sippy-2.0/downloads/
```

El proyexcto esta desarrollado en ‎Laravel que utiliza **[Composer](https://getcomposer.org/)** para gestionar sus dependencias. Por lo tanto, antes de usar Laravel, asegúrese de tener Composer instalado en su máquina.‎

‎**Instalando el proyecto**

Dentro de la carpeta del proyecto ejecute el siguiente comando para installar laravel y todos los paquetes del proyecto:‎

```shell
cd sippy-2.0
composer install
```

Crear una copia del archivo ```.env.example``` con el nombre ```.env```

Ejecunte el siguiente comando para generar la llave del proyecto:

```shell
php artisan key:generate
```

Luego cree la base de datos vacia en postgresql y configure el archivo .env con las credenciales de la misma.

```
DB_CONNECTION=pgsql
DB_HOST=localhost
DB_PORT=5432
DB_DATABASE=sippy2
DB_USERNAME=postgres
DB_PASSWORD=
```

Seguidamente ejecute el siguiente comando para crear las tablas y crear los datos iniciales:

```shell
php artisan migrate --seed
```

Ejecutar el siguiente comando para crear los enlaces sibolicos a imagenes

```shell
php artisan storage:link
```

Si todo salio correctamente el proyecto deberia estar listo para acceder desde el navegador de su preferencia

```
Usuario: admin
Contraseña: password
```